/*
 * Copyright (c) 2006-2018, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2009-01-05     Bernard      the first version
 * 2013-07-12     aozima       update for auto initial.
 */

/**
 * @addtogroup STM32
 */
/*@{*/

#include <board.h>
#include <rtthread.h>
#include "system_config.h"

#ifdef RT_USING_DFS
/* dfs filesystem:ELM filesystem init */
#include <dfs_elm.h>
/* dfs Filesystem APIs */
#include <dfs_fs.h>
#endif

#ifdef PKG_USING_GUIENGINE
#include "rtgui_demo.h"
#include <rtgui/driver.h>
#endif

#ifdef RT_USING_W25QXX
#include "spi_flash_w25qxx.h"
#endif

#include "led.h"
#include "iwdg.h"

ALIGN(RT_ALIGN_SIZE)
static rt_uint8_t led_stack[ 512 ];
static struct rt_thread led_thread;
static void led_thread_entry(void* parameter)
{
    unsigned int count=0;

	/* 查找系统中的leds设备*/
    rt_device_t led_device = rt_device_find("leds");
    if (led_device != RT_NULL)
    {
        rt_device_open(led_device, RT_DEVICE_OFLAG_RDWR);
	}

    while (1)
    {
        /* led1 on */
#ifndef RT_USING_FINSH
        rt_kprintf("led on, count : %d\r\n",count);
#endif
        count++;
        rt_device_control(led_device, IOCTL_LED0_ON, NULL);
		rt_device_control(led_device, IOCTL_LED1_OFF, NULL);
        rt_thread_delay( RT_TICK_PER_SECOND/2 ); /* sleep 0.5 second and switch to other thread */

        /* led1 off */
#ifndef RT_USING_FINSH
        rt_kprintf("led off\r\n");
#endif
        rt_device_control(led_device, IOCTL_LED0_OFF, NULL);
		rt_device_control(led_device, IOCTL_LED1_ON, NULL);

        rt_thread_delay( RT_TICK_PER_SECOND/2 );
    }
}

void rt_init_thread_entry(void* parameter)
{
#ifdef RT_USING_COMPONENTS_INIT
    /* initialization RT-Thread Components */
    rt_components_init();
#endif

#if defined(RT_USING_W25QXX) && defined(RT_USING_SPI)
	if(w25qxx_init("flash0", "spi21") != RT_EOK)
	{
		rt_kprintf("w25qxx init error!\n");
	}
	 
    /* 查找系统中的flash0设备*/
    rt_device_t device = rt_device_find("flash0");
    if (device!= RT_NULL)
    {
        rt_err_t erro = rt_device_open(device, RT_DEVICE_FLAG_RDWR | RT_DEVICE_FLAG_STANDALONE);  
        if (erro == RT_EOK) {
            //dev_data_init();
            //pos_info_init();
        } else {
            rt_kprintf("param init error!\n");
        }
    }
#endif /* RT_USING_W25QXX */


    /* Filesystem Initialization */
#if defined(RT_USING_DFS) && defined(RT_USING_DFS_ELMFAT)
    /* mount sd card fat partition 1 as root directory */
    if (dfs_mount("sd0", "/", "elm", 0, 0) == 0)
    {
        rt_kprintf("File System initialized!\n");
    }
    else
        rt_kprintf("File System initialzation failed!\n");
#endif  /* RT_USING_DFS */

#ifdef PKG_USING_GUIENGINE
	{
		rt_device_t device;

		device = rt_device_find("lcd");
		/* re-set graphic device */
		rtgui_graphic_set_device(device);
		
		rt_gui_demo_init();
	}
#endif

	watch_dog_init(); // 看门狗
}

int rt_application_init(void)
{
    rt_thread_t init_thread;

    rt_err_t result;

    /* init led thread */
    result = rt_thread_init(&led_thread,
                            "led",
                            led_thread_entry,
                            RT_NULL,
                            (rt_uint8_t*)&led_stack[0],
                            sizeof(led_stack),
                            20,
                            5);
    if (result == RT_EOK)
    {
        //rt_thread_startup(&led_thread);
    }

#if (RT_THREAD_PRIORITY_MAX == 32)
    init_thread = rt_thread_create("init",
                                   rt_init_thread_entry, RT_NULL,
                                   2048, 8, 20);
#else
    init_thread = rt_thread_create("init",
                                   rt_init_thread_entry, RT_NULL,
                                   2048, 80, 20);
#endif

    if (init_thread != RT_NULL)
        rt_thread_startup(init_thread);

    return 0;
}

/*@}*/
