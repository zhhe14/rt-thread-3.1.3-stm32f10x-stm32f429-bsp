
#ifndef __DATA_DEF_H__
#define __DATA_DEF_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <rtthread.h>
//#include "network_global.h"

// 0~0x001000字节用于存储设备的参数
// 0x001000~0x085000字节用于存储刷卡记录，可存20000条记录
// 0x085000~0x0F1000字节用于存储黑名单，可存24000 个黑名单
// 0x0F1000~0x190000字节用于读到的身份证信息，可存500个
// 0x190000~0x1B0000字节用于读到的民生卡信息，可存1300个

/* dev info.*/

#define ADDR_DEV_DATA                           0x000000                        // 注意设备参数不能超于3082
#define POS_DEV_MAX                             1

/* pos info. */
#include "dev_data.h"

// 前面3082个字节用于存储设备的参数，注意设备参数不能超于3082
#define POS_INFO_START_ADDR			            ADDR_DEV_DATA + 3082

/* record info. */
//#include "swip_card_record.h"

// 前面4096个字节用于存储设备的参数，注意设备参数不能超于4096
#define REC_START_ADDR			0x001000//ADDR_DEV_DATA + sizeof(tDEV_CONF_PARAM) + 100
#define REC_PUSH_COUNT_ADDR        REC_START_ADDR + REC_FLAG_LEN
#define REC_POP_COUNT_ADDR          REC_PUSH_COUNT_ADDR + 2
#define REC_NUM_ADDR                       REC_POP_COUNT_ADDR + 2
#define REC_ADDR                                 REC_NUM_ADDR + 2

/* blacklist info. */
//#include "black_list.h"

// 0~0x001000字节用于存储设备的参数,0x001000~0x085000字节用于存储刷卡记录
#define BLACKLIST_START_ADDR			        0x085000//REC_START_ADDR + sizeof(tSWIP_CARD_REC_HEAD) + REC_INFO_NUM * sizeof(swipe_card_to_flash_records_t) + 100

//#define BLACKLIST_PUSH_COUNT_ADDR               BLACKLIST_START_ADDR + BLACKLIST_FLAG_LEN
//#define BLACKLIST_POP_COUNT_ADDR                BLACKLIST_PUSH_COUNT_ADDR + 2
//#define BLACKLIST_NUM_ADDR                      BLACKLIST_POP_COUNT_ADDR + 2
#define BLACKLIST_ADDR                          BLACKLIST_START_ADDR + BLACKLIST_FLAG_LEN + BLACKLIST_HEAD_RESERVE

#define IDENTITY_DATA_START_ADDR		0x0F1000
#define IDENTITY_PUSH_COUNT_ADDR       	IDENTITY_DATA_START_ADDR + ID_DATA_FLAG_LEN
#define IDENTITY_POP_COUNT_ADDR         	IDENTITY_PUSH_COUNT_ADDR + 2
#define IDENTITY_NUM_ADDR                       	IDENTITY_POP_COUNT_ADDR + 2
#define IDENTITY_DATA_ADDR                                 	IDENTITY_NUM_ADDR + 2
#define IDENTITY_DATA_END_ADDR			0X190000

#define MINSHENG_DATA_START_ADDR		0x190000
#define MINSHENG_PUSH_COUNT_ADDR		MINSHENG_DATA_START_ADDR + MINSHENG_DATA_FLAG_LEN
#define MINSHENG_POP_COUNT_ADDR		MINSHENG_PUSH_COUNT_ADDR + 2
#define MINSHENG_NUM_ADDR                       	MINSHENG_POP_COUNT_ADDR + 2
#define MINSHENG_DATA_ADDR                                 	MINSHENG_NUM_ADDR + 2
#define MINSHENG_DATA_END_ADDR			0x1B0000

#define OTHER_START_ADDR			        0x1B0000

#ifdef __cplusplus
    }
#endif      // __cplusplus

#endif      // __DATA_DEF_H__
/*********************************************************************************************************
END FILE
*********************************************************************************************************/
