/*******************************************************************************************
            Copyright (C) 2009-2014 China Elite Info Services Co.,Ltd.(Guangzhou),
                                all rights reserved.
**
**--------------File Info---------------------------------------------------------------------------------
** File Name:           storage_data_manager.c
** Last modified Date:  2014-12-29
** Last Version:        v1.00
** Description:         存储数据管理
**--------------------------------------------------------------------------------------------------------
** Created By:          Liguangtong
** Created date:        2014-12-29
** Version:             v1.00
** Descriptions:
**--------------------------------------------------------------------------------------------------------
** Modified by:
** Modified date:
** Version:
** Description:
*********************************************************************************************************/
//#include "pos_reader_rs485_pro.h"
#include "dev_data.h"
//#include "pos_reader_conf.h"
#include "spi_flash_w25qxx.h"
#include <stm32f10x.h>

//#include "http.h"

#define DEV_DATA_DEBUG

#ifdef DEV_DATA_DEBUG
#define DEV_DATA_TRACE         rt_kprintf
#else
#define DEV_DATA_TRACE(...)
#endif /* DEV_DATA_DEBUG */
/*********************************************************************************************************
  全局变量声明
*********************************************************************************************************/
static rt_uint8_t server_ip_def[] = "59.42.210.210";
static rt_uint8_t server_idn_def[] = "www.sunwardservice.com";
static rt_uint16_t server_port_def = 80;
struct rt_mutex net_send_mutex;

#if 0 // zhhe
// zhhe http://app.sunwardservice.com/hoffice/interface/index.php/Hardware
//zhhe http://app.sunwardservice.com/hoffice/interface/index.php/Hardware/index
rt_uint8_t  http_header[512] = {"POST /hoffice/interface/index.php/Hardware HTTP/1.1\r\n"
                                  "Host: 59.42.210.210:80\r\n"
                                  "Cache-Control: no-cache\r\n"
                                  "Content-Type: application/octet-stream\r\n"
                                  "Content-Length:      \r\n\r\n"};
#else // zhhe
// zhhe http://app.sunwardservice.com/hoffice/interface/index.php/Hardware
//zhhe http://app.sunwardservice.com/hoffice/interface/index.php/Hardware/index
//http://59.42.210.217/accesscon/interfaces/hardwareblack
//服务站正式后台: http://172.25.242.107/accesscon/interfaces/hardwareblack
rt_uint8_t  server_addr_port[22] = "172.25.242.107:80";
rt_uint8_t  http_header[256] = {"POST /accesscon/interfaces/hardwareblack HTTP/1.1\r\n"
                                  "Host:                      \r\n"
                                  "Cache-Control: no-cache\r\n"
                                  "Content-Type: application/octet-stream\r\n"
                                  //"Connection: keep-alive\r\n"
                                  "Content-Length:      \r\n\r\n"};
#endif

void add_server_addr_to_header(rt_uint8_t* header, const rt_uint8_t * server_addr_str)
{
    char *str1 = NULL;
    char *str2 = NULL;
	
    str1 = rt_strstr((const char *)header, "Host: ");
	str1 += 5;
	str2 = rt_strstr((const char *)str1, "\r\n");
	for (rt_uint8_t i = 0; i < str2 - str1; i++)
	{
		*(str1 + i) = ' ';
	}

	rt_strncpy(str1 + 1, (const char *)server_addr_str, rt_strlen((const char *)server_addr_str));
}

//#define RT_LWIP_DNSADDR 	"192.168.1.1"
//static rt_uint8_t dns_addr[] = "192.168.1.1";

static rt_uint8_t wifi_name_def[] = "sunward-new";
static rt_uint8_t wifi_psswd_def[] = "sunward123";

tDEV_CONF_PARAM __tdev_config_param;
tALL_POS_INFO __tall_pos_info;

tDEV_CONF_PARAM dummy_dev_conf_param;
tALL_POS_INFO dummy_all_pos_info;

//rt_uint8_t dev_data_4096str[4096] = {0};

/* ip address of target*/
#define RT_LWIP_IPADDR0	192
#define RT_LWIP_IPADDR1	168
#define RT_LWIP_IPADDR2	1
#define RT_LWIP_IPADDR3	129

/* gateway address of target*/
#define RT_LWIP_GWADDR0	192
#define RT_LWIP_GWADDR1	168
#define RT_LWIP_GWADDR2	1
#define RT_LWIP_GWADDR3	1

/* mask address of target*/
#define RT_LWIP_MSKADDR0	255
#define RT_LWIP_MSKADDR1	255
#define RT_LWIP_MSKADDR2	255
#define RT_LWIP_MSKADDR3	0

#define RT_LWIP_DNSADDR 	0x0101a8c0


rt_err_t dev_data_init(void)
{
    rt_err_t erro = RT_ERROR;
    rt_device_t device;

    /* 查找系统中的flash0设备*/
    device = rt_device_find("flash0");
    if (device!= RT_NULL)
    {
        //erro = rt_device_open(device, RT_DEVICE_FLAG_RDWR | RT_DEVICE_FLAG_STANDALONE);                                 // 打开设备
        erro = RT_EOK;
        if (erro == RT_EOK) {
            rt_memset(&dummy_dev_conf_param, 0, sizeof(tDEV_CONF_PARAM));
            rt_device_read(device, ADDR_DEV_DATA, &dummy_dev_conf_param, sizeof(tDEV_CONF_PARAM));
            if(rt_memcmp(dummy_dev_conf_param.flag, FLAG_DEV_DATA, sizeof(dummy_dev_conf_param.flag)) == 0 )
            {
                rt_memcpy(&__tdev_config_param, (void *)&dummy_dev_conf_param,sizeof(tDEV_CONF_PARAM));
					__tdev_config_param.get_sys_param_time = 0x05; // 5 分钟

					
#if 1
				rt_kprintf("系统参数OK:\r\n");
				rt_kprintf("开门时间段1:%02d:%02d - %02d:%02d\r\n开门时间段2:%02d:%02d - %02d:%02d\r\n", 
							__tdev_config_param.time_solt.start_hour1, __tdev_config_param.time_solt.start_min1,
							__tdev_config_param.time_solt.end_hour1,__tdev_config_param.time_solt.end_min1, 
							__tdev_config_param.time_solt.start_hour2, __tdev_config_param.time_solt.start_min2,
							__tdev_config_param.time_solt.end_hour2,__tdev_config_param.time_solt.end_min2);
				rt_kprintf("ip address: %d.%d.%d.%d\n", __tdev_config_param.__dev_ip_addr.ip_addr.addr0, __tdev_config_param.__dev_ip_addr.ip_addr.addr1, __tdev_config_param.__dev_ip_addr.ip_addr.addr2, __tdev_config_param.__dev_ip_addr.ip_addr.addr3);
				rt_kprintf("gw address: %d.%d.%d.%d\n", __tdev_config_param.__dev_ip_addr.gw_addr.addr0, __tdev_config_param.__dev_ip_addr.gw_addr.addr1, __tdev_config_param.__dev_ip_addr.gw_addr.addr2, __tdev_config_param.__dev_ip_addr.gw_addr.addr3);
				rt_kprintf("net mask : %d.%d.%d.%d\n", __tdev_config_param.__dev_ip_addr.net_mask.addr0, __tdev_config_param.__dev_ip_addr.net_mask.addr1, __tdev_config_param.__dev_ip_addr.net_mask.addr2, __tdev_config_param.__dev_ip_addr.net_mask.addr3);
				rt_kprintf("DNS addr : %d.%d.%d.%d\n", __tdev_config_param.__dev_ip_addr.DNS_addr & 0xff, (__tdev_config_param.__dev_ip_addr.DNS_addr >> 8) & 0xff, (__tdev_config_param.__dev_ip_addr.DNS_addr >> 16) & 0xff, (__tdev_config_param.__dev_ip_addr.DNS_addr >> 24) & 0xff);
				rt_kprintf("mac address: %02X %02X %02X %02X %02X %02X\n", 
						__tdev_config_param.__dev_ip_addr.mac_addr[0], __tdev_config_param.__dev_ip_addr.mac_addr[1], 
						__tdev_config_param.__dev_ip_addr.mac_addr[2], __tdev_config_param.__dev_ip_addr.mac_addr[3],
						__tdev_config_param.__dev_ip_addr.mac_addr[4], __tdev_config_param.__dev_ip_addr.mac_addr[5]);
				rt_kprintf("获取系统参数时间间隔: %d min\n", __tdev_config_param.get_sys_param_time);
#endif
            }
            else
            {
                // 使用初始化值
                rt_memset(&dummy_dev_conf_param, 0, sizeof(tDEV_CONF_PARAM));
                rt_memcpy(dummy_dev_conf_param.flag,FLAG_DEV_DATA,sizeof(dummy_dev_conf_param.flag));

                dummy_dev_conf_param.door_type = 0x01;
                rt_memcpy(dummy_dev_conf_param.__network_conn_param.tcp_conn_server_ip,server_ip_def,rt_strlen((char *)server_ip_def));
                rt_memcpy(dummy_dev_conf_param.__network_conn_param.tcp_conn_server_idn,server_idn_def,rt_strlen((char *)server_idn_def));
                dummy_dev_conf_param.__network_conn_param.tcp_conn_server_port = server_port_def;

                rt_memcpy(dummy_dev_conf_param.__network_conn_param.wifi_name,wifi_name_def,rt_strlen((char *)wifi_name_def));
                rt_memcpy(dummy_dev_conf_param.__network_conn_param.wifi_password,wifi_psswd_def,rt_strlen((char *)wifi_psswd_def));

                dummy_dev_conf_param.__dev_ip_addr.ip_mode = IPADDR_USE_STATIC;

                dummy_dev_conf_param.__dev_ip_addr.ip_addr.addr0 = RT_LWIP_IPADDR0;
                dummy_dev_conf_param.__dev_ip_addr.ip_addr.addr1 = RT_LWIP_IPADDR1;
                dummy_dev_conf_param.__dev_ip_addr.ip_addr.addr2 = RT_LWIP_IPADDR2;
                dummy_dev_conf_param.__dev_ip_addr.ip_addr.addr3 = RT_LWIP_IPADDR3;

                dummy_dev_conf_param.__dev_ip_addr.net_mask.addr0 = RT_LWIP_MSKADDR0;
                dummy_dev_conf_param.__dev_ip_addr.net_mask.addr1 = RT_LWIP_MSKADDR1;
                dummy_dev_conf_param.__dev_ip_addr.net_mask.addr2 = RT_LWIP_MSKADDR2;
                dummy_dev_conf_param.__dev_ip_addr.net_mask.addr3 = RT_LWIP_MSKADDR3;

                dummy_dev_conf_param.__dev_ip_addr.gw_addr.addr0 = RT_LWIP_GWADDR0;
                dummy_dev_conf_param.__dev_ip_addr.gw_addr.addr1 = RT_LWIP_GWADDR1;
                dummy_dev_conf_param.__dev_ip_addr.gw_addr.addr2 = RT_LWIP_GWADDR2;
                dummy_dev_conf_param.__dev_ip_addr.gw_addr.addr3 = RT_LWIP_GWADDR3;
                dummy_dev_conf_param.__dev_ip_addr.DNS_addr = RT_LWIP_DNSADDR;

                dummy_dev_conf_param.__dev_ip_addr.mac_addr[0] = 0x00;
                dummy_dev_conf_param.__dev_ip_addr.mac_addr[1] = 0x60;
                dummy_dev_conf_param.__dev_ip_addr.mac_addr[2] = 0x6E;
                dummy_dev_conf_param.__dev_ip_addr.mac_addr[3] = 0x11;
                dummy_dev_conf_param.__dev_ip_addr.mac_addr[4] = 0x22;
                dummy_dev_conf_param.__dev_ip_addr.mac_addr[5] = 0x36;
				// 开门时间段1
				dummy_dev_conf_param.time_solt.start_hour1 = 0x08; 	// 8:30
				dummy_dev_conf_param.time_solt.start_min1 =  0x1E;
				dummy_dev_conf_param.time_solt.end_hour1 =  0x0C; 	// 12:00
				dummy_dev_conf_param.time_solt.end_min1 =   0x00;
				// 开门时间段2
				dummy_dev_conf_param.time_solt.start_hour2 = 0x0E; 	// 14:30
				dummy_dev_conf_param.time_solt.start_min2 =  0x1E;
				dummy_dev_conf_param.time_solt.end_hour2 =  0x11;  // 17:00
				dummy_dev_conf_param.time_solt.end_min2 =   0x00;
                //rt_memcpy(dummy_dev_conf_param.__dev_ip_addr.DNS_addr,dns_addr,rt_strlen((char *)dns_addr));

				dummy_dev_conf_param.get_sys_param_time = 0x05; //5 分钟
				rt_kprintf("get_sys_param_time = %d\n", dummy_dev_conf_param.get_sys_param_time);
                rt_device_write(device, ADDR_DEV_DATA, &dummy_dev_conf_param, sizeof(tDEV_CONF_PARAM));
                rt_thread_delay(10);
                rt_memset(&dummy_dev_conf_param, 0, sizeof(tDEV_CONF_PARAM));
                rt_device_read(device, ADDR_DEV_DATA, &dummy_dev_conf_param, sizeof(tDEV_CONF_PARAM));
                if( rt_memcmp(dummy_dev_conf_param.flag, FLAG_DEV_DATA, sizeof(dummy_dev_conf_param.flag)) != 0 ) {
                    erro = RT_ERROR;
                } else {
                    rt_memcpy(&__tdev_config_param, (void *)&dummy_dev_conf_param,sizeof(tDEV_CONF_PARAM));
                    erro = RT_EOK;
            	}	
				
#if 1
				rt_kprintf("系统参数:\r\n");
				rt_kprintf("开门时间段1:%02d:%02d - %02d:%02d\r\n开门时间段2:%02d:%02d - %02d:%02d\r\n", 
							dummy_dev_conf_param.time_solt.start_hour1, dummy_dev_conf_param.time_solt.start_min1,
							dummy_dev_conf_param.time_solt.end_hour1,dummy_dev_conf_param.time_solt.end_min1, 
							dummy_dev_conf_param.time_solt.start_hour2, dummy_dev_conf_param.time_solt.start_min2,
							dummy_dev_conf_param.time_solt.end_hour2,dummy_dev_conf_param.time_solt.end_min2);
				rt_kprintf("ip address: %d.%d.%d.%d\n", dummy_dev_conf_param.__dev_ip_addr.ip_addr.addr0, dummy_dev_conf_param.__dev_ip_addr.ip_addr.addr1, dummy_dev_conf_param.__dev_ip_addr.ip_addr.addr2, dummy_dev_conf_param.__dev_ip_addr.ip_addr.addr3);
				rt_kprintf("gw address: %d.%d.%d.%d\n", dummy_dev_conf_param.__dev_ip_addr.gw_addr.addr0, dummy_dev_conf_param.__dev_ip_addr.gw_addr.addr1, dummy_dev_conf_param.__dev_ip_addr.gw_addr.addr2, dummy_dev_conf_param.__dev_ip_addr.gw_addr.addr3);
				rt_kprintf("net mask : %d.%d.%d.%d\n", dummy_dev_conf_param.__dev_ip_addr.net_mask.addr0, dummy_dev_conf_param.__dev_ip_addr.net_mask.addr1, dummy_dev_conf_param.__dev_ip_addr.net_mask.addr2, dummy_dev_conf_param.__dev_ip_addr.net_mask.addr3);
				rt_kprintf("DNS addr : %d.%d.%d.%d\n", dummy_dev_conf_param.__dev_ip_addr.DNS_addr & 0xff, (dummy_dev_conf_param.__dev_ip_addr.DNS_addr >> 8) & 0xff, (dummy_dev_conf_param.__dev_ip_addr.DNS_addr >> 16) & 0xff, (dummy_dev_conf_param.__dev_ip_addr.DNS_addr >> 24) & 0xff);
				rt_kprintf("mac address: %02X %02X %02X %02X %02X %02X\n", 
						dummy_dev_conf_param.__dev_ip_addr.mac_addr[0], dummy_dev_conf_param.__dev_ip_addr.mac_addr[1], 
						dummy_dev_conf_param.__dev_ip_addr.mac_addr[2], dummy_dev_conf_param.__dev_ip_addr.mac_addr[3],
						dummy_dev_conf_param.__dev_ip_addr.mac_addr[4], dummy_dev_conf_param.__dev_ip_addr.mac_addr[5]);
				rt_kprintf("获取系统参数时间间隔: %d min\n", dummy_dev_conf_param.get_sys_param_time);
#endif
       	 	}
    	}
   	}

	return erro;
}
MSH_CMD_EXPORT_ALIAS(dev_data_init, dev_init, dev_data_init);


#if 0
#ifdef RT_USING_DFS
/* dfs filesystem:ELM filesystem init */
#include <dfs_elm.h>
/* dfs Filesystem APIs */
#include <dfs_fs.h>
#endif
void file_system_test(void)
{
    //文件系统测试代码
    int fd=0;

    fd =open("/myfile.txt",DFS_O_CREAT|DFS_O_RDWR,0);
    if(fd <0)
    {
        rt_kprintf("open file failed!\r\n");
    }
    else
    {
        int count =write(fd,"123456",7);
        char buf[10];

        close(fd);
        fd =0;


        rt_thread_delay(50);
        rt_memset(buf,0,10);
        fd =open("/myfile.txt",DFS_O_RDONLY,0);
        if(read(fd,buf,7))
        {
            rt_kprintf("read=%s\r\n",buf);
        }
        else
        {
            rt_kprintf("read file err!\r\n");
        }

    }
}
#endif
