
#ifndef __DEV_DATA_H__
#define __DEV_DATA_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <rtthread.h>
//#include "network_global.h"
#include "data_def.h"


#define IPADDR_USE_STATIC                       1                               // 1:使用静态IP；0:使用动态IP

#define DEV_DATA_FLAG_LEN                       8
#define FLAG_DEV_DATA                           "DEV_qATA"
//#define SYSTEM_CODE                             0x01000001                      //高字节0x01:门禁   低三字节0x000001 测试系统用
//#define DEV_CODE                                0x10000001                      //高字节0x01:门禁   低三字节0x000001 测试系统用

//#define CONTRLER_ID                             0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,


#define POS_INFO_FLAG_LEN                       4
#define FLAG_POS_INFO                           "kOFO"

//typedef __packed struct
//{
//    rt_uint32_t  dev_id;
//}POS_DEV_ID;

//typedef __packed struct
//{
//    //rt_uint8_t addr_num;
//    //POS_DEV_ID pos[POS_DEV_MAX];
//    rt_uint32_t  pos_id;
//}tPOS_DEV_ID;

typedef __packed struct
{
    //rt_uint32_t pos_id;
    rt_uint16_t blacklist_num;
    //rt_uint32_t whitelist_rec_crc16;
    rt_uint32_t blacklist_rec_timestamp;//服务器最后一次更改黑名单的时间
    rt_uint8_t  open_door_time;
    rt_uint8_t  door_opening_limit_time;
     char swipe_card_status;	// zhhe
    //char pos_exist_status;	// zhhe
    rt_uint8_t  reserve[4];		// 12
}tPOS_INFO;

typedef __packed struct
{
    rt_uint8_t  tcp_conn_server_idn[30];                                        // 连接tcp服务器域名
    rt_uint8_t  tcp_conn_server_ip[15];                                         // 连接tcp服务器IP
    rt_uint16_t tcp_conn_server_port;                                           // 连接tcp服务器端口
    rt_uint8_t  wifi_name[30];                                                  // wifi名称
    rt_uint8_t  wifi_password[30];                                              // wifi密码
}tNETWORK_CONN_PARAM;

typedef __packed struct
{
    rt_uint8_t  addr0;
    rt_uint8_t  addr1;
    rt_uint8_t  addr2;
    rt_uint8_t  addr3;
}tIP_ADDR;

typedef __packed struct
{
	//时间段1 (上午)
	rt_uint8_t    start_hour1;	/* 开始时 */
	rt_uint8_t    start_min1; 		/* 开始分 */
	rt_uint8_t    end_hour1;		/* 结束时 */
	rt_uint8_t    end_min1;		/* 结束分 */
	//时间段2 (下午)
	rt_uint8_t 	start_hour2;	/* 开始时*/
	rt_uint8_t 	start_min2; 	/* 开始分*/
	rt_uint8_t 	end_hour2;		/* 结束时*/
	rt_uint8_t 	end_min2;		/* 结束分*/
}tTIME_SLOT;  /* 开门时间段结构体 */


typedef __packed struct
{
    rt_uint8_t  ip_mode;
    tIP_ADDR  ip_addr;
    tIP_ADDR  net_mask;
    tIP_ADDR gw_addr;
    rt_uint32_t DNS_addr;
    rt_uint32_t DNS_addr_bakup;
    rt_uint8_t  mac_addr[6];
}tDEVICE_IP_ADDR;

typedef __packed struct
{
    rt_uint8_t  flag[DEV_DATA_FLAG_LEN];
    //tNETWORK_CODE __tcode;
    rt_uint8_t  contrler_id[8];
    rt_uint8_t  door_type;    // 1 推拉门 2 自动哦们3其他
    tNETWORK_CONN_PARAM __network_conn_param;
    tDEVICE_IP_ADDR __dev_ip_addr;
    tTIME_SLOT    time_solt; /*开门时间段*/
	rt_uint8_t get_sys_param_time;/*获取系统参数时间间隔*/
    rt_uint8_t  reserve[285];
} tDEV_CONF_PARAM;                                                              // 注意设备参数不能超于3082

typedef __packed struct
{
    rt_uint8_t flag[POS_INFO_FLAG_LEN];
    tPOS_INFO  pos_info;
    rt_uint8_t reserve[100];
} tALL_POS_INFO;                                                                // 注意pos信息不能超于1024

extern tALL_POS_INFO __tall_pos_info;
extern tDEV_CONF_PARAM __tdev_config_param;

/*********************************************************************************************************
  宏定义
*********************************************************************************************************/
#define HTON2(x)  ((((x) & 0xff) << 8)| (((x) & 0xff00) >> 8))
#define HTON4(x)  ((HTON2((x) & 0xffff) << 16) | HTON2(((x) & 0xffff0000) >> 16))
#define NTOH2(x)  HTON2(x)
#define NTOH4(x)  HTON4(x)

typedef enum
{
    e_RecvSuccess = 0x01,                       //  接收成功
    e_RecvFail                                  //  接收失败
}eReplyStatus;                                  //  回复状态

/*********************************************************************************************************
**  实现的外部函数声明
*********************************************************************************************************/
void add_server_addr_to_header(rt_uint8_t* header, const rt_uint8_t * server_addr_str);
extern rt_err_t dev_data_init(void);
extern rt_err_t dev_data_save(void);

rt_err_t pos_info_init(void);
extern rt_err_t pos_info_save(void);

#ifdef __cplusplus
    }
#endif      // __cplusplus

#endif      // __STORAGE_DATA_MANAGER_H__
/*********************************************************************************************************
END FILE
*********************************************************************************************************/
