
#ifndef _SYSTEM_CONFIG_H
#define _SYSTEM_CONFIG_H
 
#ifdef __cplusplus
extern "C" {
#endif

#include <rtthread.h>

//#define HWTIMER3_DEV_NAME "timer3"
#define WATCH_DOG_ENABLE
#define IWDG_DEVICE_NAME		"wdt"    	/* 看门狗设备名称 */
#define RTC_DEVICE_NAME			"rtc"    	/* rtc设备名称 */
#define W25Q_SPI_DEVICE_NAME 	"flash0"	/* w25qxx设备名称 */

/* IN_And_OUT thread config*/
#define IN_And_OUT_THREAD_PRIORITY   	4
#define IN_And_OUT_THREAD_STACK_SIZE	512
 
/* UART1 thread config*/
#define UART1_THREAD_PRIORITY                   6
#define UART1_THREAD_STACK_SIZE                 512
/* UART2 thread config*/
#define UART2_THREAD_PRIORITY                   7
#define UART2_THREAD_STACK_SIZE                 512   
    
/* UART3 thread config*/
#define UART3_THREAD_PRIORITY                   8
#define UART3_THREAD_STACK_SIZE                 1024 
 
/* UART4 thread config*/
#define UART4_THREAD_PRIORITY                   5
#define UART4_THREAD_STACK_SIZE               2048

/* UART5 thread config*/
#define UART5_THREAD_PRIORITY                   5
#define UART5_THREAD_STACK_SIZE                 1024

/* UART4 thread config*/
#define POS_READER_THREAD_PRIORITY              3
#define POS_READER_THREAD_STACK_SIZE            1024

/* IDENTITY thread config*/
#define IDENTITY_READER_THREAD_PRIORITY              3
#define IDENTITY_READER_THREAD_STACK_SIZE            4096

/* MINSHENG thread config*/
#define MINSHENG_READER_THREAD_PRIORITY              3
#define MINSHENG_READER_THREAD_STACK_SIZE            1024

/* led thread config*/
#define LED_STATUS_THREAD_PRIORITY              25
#define LED_STATUS_THREAD_STACK_SIZE            256   
/* time verify thread config*/
#define TIME_VERIFY_THREAD_PRIORITY             26
#define TIME_VERIFY_THREAD_STACK_SIZE           2048 
    
/* swip card thread config*/
#define SWIP_CARD_REC_THREAD_PRIORITY           15
#define SWIP_CARD_REC_THREAD_STACK_SIZE         4096

/* swip card thread config*/
#define SWIP_IDENTITY_CARD_DATA_THREAD_PRIORITY           16
#define SWIP_IDENTITY_CARD_DATA_THREAD_STACK_SIZE         2048

/* swip card thread config*/
#define SWIP_MINSHENG_CARD_DATA_THREAD_PRIORITY           17
#define SWIP_MINSHENG_CARD_DATA_THREAD_STACK_SIZE         1024
    
/* network connect thread config*/
#define NETWORK_CONN_THREAD_PRIORITY            10
#define NETWORK_CONN_THREAD_STACK_SIZE          2048
    
/* watch dog thread config*/
#define WATCH_DOG_THREAD_PRIORITY               3
#define WATCH_DOG_THREAD_STACK_SIZE             512
    
/* watch dog thread config*/
#define UPDATE_TIMESTAMP_THREAD_PRIORITY        11
#define UPDATE_TIMESTAMP_THREAD_STACK_SIZE      512
    
/* remote update thread config*/
#define REMOTE_UPDATE_THREAD_PRIORITY           20
#define REMOTE_UPDATE_THREAD_STACK_SIZE         2048
    
/* whitelist sync thread config*/
#define WHITELIST_SYNC_THREAD_PRIORITY           18
#define WHITELIST_SYNC_THREAD_STACK_SIZE         2048

/* blacklist sync thread config*/
#define BLACKLIST_SYNC_THREAD_PRIORITY           18
#define BLACKLIST_SYNC_THREAD_STACK_SIZE         2048

/* udp server thread config*/
#define UDP_SERVER_THREAD_PRIORITY           	22
#define UDP_SERVER_THREAD_STACK_SIZE         	2048
 
#ifdef __cplusplus
    }
#endif      // __cplusplus
#endif /*_SYSTERM_CONFIG_H*/

