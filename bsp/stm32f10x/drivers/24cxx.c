#include "24cxx.h" 
#include "bsp_delay.h"

struct at24cxx_device at24cxx;

#if 0
//初始化 IIC 接口
void AT24CXX_Init(void)
{
	IIC_Init();
}

//在AT24CXX指定地址读出一个数据
//ReadAddr:开始读数的地址  
//返回值  :读到的数据
u8 AT24CXX_ReadOneByte(u16 ReadAddr)
{				  
	u8 temp=0;		  	    																 
    IIC_Start();  
	if(EE_TYPE>AT24C16)
	{
		IIC_Send_Byte(0XA0);	   //发送写命令
		IIC_Wait_Ack();
		IIC_Send_Byte(ReadAddr>>8);//发送高地址
		IIC_Wait_Ack();		 
	}else IIC_Send_Byte(0XA0+((ReadAddr/256)<<1));   //发送器件地址0XA0,写数据 	 

	IIC_Wait_Ack(); 
    IIC_Send_Byte(ReadAddr%256);   //发送低地址
	IIC_Wait_Ack();	    
	IIC_Start();  	 	   
	IIC_Send_Byte(0XA1);           //进入接收模式			   
	IIC_Wait_Ack();	 
    temp=IIC_Read_Byte(0);		   
    IIC_Stop();//产生一个停止条件	    
	return temp;
}
//在AT24CXX指定地址写入一个数据
//WriteAddr  :写入数据的目的地址    
//DataToWrite:要写入的数据
void AT24CXX_WriteOneByte(u16 WriteAddr,u8 DataToWrite)
{				   	  	    																 
    IIC_Start();  
	if(EE_TYPE>AT24C16)
	{
		IIC_Send_Byte(0XA0);	    //发送写命令
		IIC_Wait_Ack();
		IIC_Send_Byte(WriteAddr>>8);//发送高地址
 	}else
	{
		IIC_Send_Byte(0XA0+((WriteAddr/256)<<1));   //发送器件地址0XA0,写数据 
	}	 
	IIC_Wait_Ack();	   
    IIC_Send_Byte(WriteAddr%256);   //发送低地址
	IIC_Wait_Ack(); 	 										  		   
	IIC_Send_Byte(DataToWrite);     //发送字节							   
	IIC_Wait_Ack();  		    	   
    IIC_Stop();//产生一个停止条件 
	delay_ms(10);	 
}
//在AT24CXX里面的指定地址开始写入长度为Len的数据
//该函数用于写入16bit或者32bit的数据.
//WriteAddr  :开始写入的地址  
//DataToWrite:数据数组首地址
//Len        :要写入数据的长度2,4
void AT24CXX_WriteLenByte(u16 WriteAddr,u32 DataToWrite,u8 Len)
{  	
	u8 t;
	for(t=0;t<Len;t++)
	{
		AT24CXX_WriteOneByte(WriteAddr+t,(DataToWrite>>(8*t))&0xff);
	}												    
}

//在AT24CXX里面的指定地址开始读出长度为Len的数据
//该函数用于读出16bit或者32bit的数据.
//ReadAddr   :开始读出的地址 
//返回值     :数据
//Len        :要读出数据的长度2,4
u32 AT24CXX_ReadLenByte(u16 ReadAddr,u8 Len)
{  	
	u8 t;
	u32 temp=0;
	for(t=0;t<Len;t++)
	{
		temp<<=8;
		temp+=AT24CXX_ReadOneByte(ReadAddr+Len-t-1); 	 				   
	}
	return temp;												    
}
//检查AT24CXX是否正常
//这里用了24XX的最后一个地址(255)来存储标志字.
//如果用其他24C系列,这个地址要修改
//返回1:检测失败
//返回0:检测成功
u8 AT24CXX_Check(void)
{
	u8 temp;
	temp=AT24CXX_ReadOneByte(255);//避免每次开机都写AT24CXX			   
	if(temp==0X55)return 0;		   
	else//排除第一次初始化的情况
	{
		AT24CXX_WriteOneByte(255,0X55);
	    temp=AT24CXX_ReadOneByte(255);	  
		if(temp==0X55)return 0;
	}
	return 1;											  
}

//在AT24CXX里面的指定地址开始读出指定个数的数据
//ReadAddr :开始读出的地址 对24c02为0~255
//pBuffer  :数据数组首地址
//NumToRead:要读出数据的个数
void AT24CXX_Read(u16 ReadAddr,u8 *pBuffer,u16 NumToRead)
{
	while(NumToRead)
	{
		*pBuffer++=AT24CXX_ReadOneByte(ReadAddr++);	
		NumToRead--;
	}
}  
//在AT24CXX里面的指定地址开始写入指定个数的数据
//WriteAddr :开始写入的地址 对24c02为0~255
//pBuffer   :数据数组首地址
//NumToWrite:要写入数据的个数
void AT24CXX_Write(u16 WriteAddr,u8 *pBuffer,u16 NumToWrite)
{
	while(NumToWrite--)
	{
		AT24CXX_WriteOneByte(WriteAddr,*pBuffer);
		WriteAddr++;
		pBuffer++;
	}
}
#endif

static rt_size_t at24cxx_read_regs(rt_uint8_t addr,rt_uint8_t *pData,rt_uint8_t len)
{
    struct rt_i2c_msg msg[2];
	//unsigned char send_buffer[2]; // = addr;
	//send_buffer[0] = (addr >> 8) & 0xff;
	//send_buffer[1] = addr & 0xff;
	rt_uint8_t send_buffer = addr;

    rt_memset(msg, 0, sizeof(msg));
	if(EE_TYPE>AT24C16)
	{
		msg[0].addr = 0xA0;//发送写命令
 	}else
	{
		msg[0].addr = (0XA0+((addr/256)<<1));//发送器件地址0XA0,写数据  
	}

    //msg[0].addr = 0XA0;
    msg[0].flags = RT_I2C_WR;
    msg[0].len = 1;
    msg[0].buf = &send_buffer;
    
    msg[1].addr = 0xA1; // 进入接收模式		
    msg[1].flags = RT_I2C_RD;
    msg[1].len = len;
    msg[1].buf = pData;

    return rt_i2c_transfer(at24cxx.i2c_device, msg, 2);;
}

static rt_size_t at24cxx_write_regs(rt_uint8_t addr,rt_uint8_t *pData,rt_uint8_t len)
{
    struct rt_i2c_msg msg[2];
	//unsigned char send_buffer[2]; // = addr;
	//send_buffer[0] = (addr >> 8) & 0xff;
	//send_buffer[1] = addr & 0xff;
	rt_uint8_t send_buffer = addr;

    rt_memset(msg, 0, sizeof(msg));
	if(EE_TYPE>AT24C16)
	{
		msg[0].addr = 0xA0;//发送写命令
 	}else
	{
		msg[0].addr = (0XA0+((addr/256)<<1));//发送器件地址0XA0,写数据  
	}
   
    msg[0].flags = RT_I2C_WR;
    msg[0].len = 1;
    msg[0].buf = &send_buffer;

    //msg[1].addr = addr;
    msg[1].flags =  RT_I2C_WR | RT_I2C_NO_START;// | RT_I2C_IGNORE_NACK;
    msg[1].len = len;
    msg[1].buf = pData;

    return rt_i2c_transfer(at24cxx.i2c_device, msg, 2);
}


//检查AT24CXX是否正常
//这里用了24XX的最后一个地址(255)来存储标志字.
//如果用其他24C系列,这个地址要修改
//返回1:检测失败
//返回0:检测成功
static unsigned char at24cxx_check(void)
{
	unsigned char temp;
	unsigned char chk = 0x55;
	
	at24cxx_read_regs(255, &temp, sizeof(temp));
	if(temp==chk)
	{
		rt_kprintf("24cxx checked succeed!\r\n");
		return 0; 	   
	}
	else//排除第一次初始化的情况
	{
		at24cxx_write_regs(255, &chk , 1);
		at24cxx_read_regs(255, &temp, sizeof(temp));	  
		if(temp==chk)
		{
			rt_kprintf("24cxx init %02x succeed!\r\n", chk);
			return 0;
		}
	}

	return 1;
}

rt_err_t at24cxx_init(rt_device_t dev)
{
	return RT_EOK;
}

rt_err_t at24cxx_open(rt_device_t dev, rt_uint16_t oflag)
{
	return RT_EOK;
}

rt_err_t at24cxx_close(rt_device_t dev)
{
	return RT_EOK;
}

rt_size_t at24cxx_read(rt_device_t dev, rt_off_t pos, void *buffer, rt_size_t size)
{
	RT_ASSERT(dev != RT_NULL);
	RT_ASSERT(buffer != RT_NULL);
	RT_ASSERT(size != 0);

	return at24cxx_read_regs(pos, (rt_uint8_t *)buffer, size);
}

rt_size_t at24cxx_write(rt_device_t dev, rt_off_t pos, const void *buffer, rt_size_t size)
{
	RT_ASSERT(dev != RT_NULL);
	RT_ASSERT(buffer != RT_NULL);
	RT_ASSERT(size != 0);
	
	return at24cxx_write_regs(pos, (rt_uint8_t *)buffer, size);
}

rt_err_t at24cxx_control(rt_device_t dev, int cmd, void *args)
{
	return RT_EOK;
}

#ifdef RT_USING_I2C
int hw_at24cxx_init(const char * i2c_device_name, const char * i2c_bus_device_name)
{
	struct rt_i2c_bus_device * i2c_device;

    i2c_device = rt_i2c_bus_device_find(i2c_bus_device_name);
    if(i2c_device == RT_NULL)
    {
        rt_kprintf("i2c bus device %s not found!\r\n", i2c_bus_device_name);
        return -RT_ENOSYS;
    }
    at24cxx.i2c_device = i2c_device;

	if (at24cxx_check() == 1)
	{
		rt_kprintf("AT24CXX Check failed!\r\n");
		 return -RT_ERROR;
	}

	at24cxx.parent.type = RT_Device_Class_I2CBUS;
    at24cxx.parent.rx_indicate = RT_NULL;
    at24cxx.parent.tx_complete = RT_NULL;
    at24cxx.parent.user_data   = RT_NULL;

    at24cxx.parent.control = at24cxx_control;
    at24cxx.parent.init    = at24cxx_init;
    at24cxx.parent.open    = at24cxx_open;
    at24cxx.parent.close   = at24cxx_close;
    at24cxx.parent.read    = at24cxx_read;
    at24cxx.parent.write   = at24cxx_write;
	
    /* register the device */
    return rt_device_register(&at24cxx.parent, i2c_device_name, RT_DEVICE_FLAG_RDWR | RT_DEVICE_FLAG_STANDALONE);
}

int rt_hw_at24cxx_init(void)
{
	return hw_at24cxx_init("at24cxx", "i2c2");	
}INIT_DEVICE_EXPORT(rt_hw_at24cxx_init);
#endif /*RT_USING_I2C*/

#ifdef RT_USING_FINSH
#include <finsh.h>
void at24cxx_test(rt_off_t pos, rt_uint8_t *buf)
{	
	if (pos > EE_TYPE)
	{
		rt_kprintf("usge1: 24cxx_test [0-%d] str...\r\n", EE_TYPE);
		return ;
	}
	
	if (buf == RT_NULL)
	{
		rt_kprintf("usge2: 24cxx_test [0-%d] str...\r\n", EE_TYPE);
		return ;
	}
		
    /* 查找系统中的at24cxx设备*/
    rt_device_t at24cxx_device = rt_device_find("at24cxx");
    if (at24cxx_device != RT_NULL)
    {
        rt_device_open(at24cxx_device, RT_DEVICE_OFLAG_RDWR);
	}

	int len = rt_strlen((const char *)buf);
	rt_uint8_t *rbuf = rt_malloc(len+1);
	
	rt_device_write(at24cxx_device, pos, buf, len);
	rt_thread_mdelay(10);
	
	rt_device_read(at24cxx_device, pos, rbuf, len);	
	rbuf[len] = '\0';
	
	if (rt_strncmp((const char *)rbuf, (const char *)buf, len) != 0)
	{
		rt_kprintf("at24cxx_test failed...rbuf: %s\r\n", rbuf);
		return ;
	}

	rt_kprintf("at24cxx_test OK...w:%s, r:%s\r\n", buf, rbuf);

	rt_free(rbuf);
}
FINSH_FUNCTION_EXPORT(at24cxx_test, test at24cxx write and read);

void msh_at24cxx_test(int argc, char** argv)
{	
	rt_off_t pos;

	if (argc != 3)
    {
		rt_kprintf("usge: 24cxx_test [0-%d] str...\r\n", EE_TYPE);
		return ;
	}

	sscanf(argv[1], "%d", &pos);
	if (pos > EE_TYPE)
	{
		rt_kprintf("usge1: 24cxx_test [0-%d] str...\r\n", EE_TYPE);
		return ;
	}

	if (argv[2] == RT_NULL)
	{
		rt_kprintf("usge2: 24cxx_test [0-%d] str...\r\n", EE_TYPE);
		return ;
	}

	/* 查找系统中的at24cxx设备*/
    rt_device_t at24cxx_device = rt_device_find("at24cxx");
    if (at24cxx_device != RT_NULL)
    {
        rt_device_open(at24cxx_device, RT_DEVICE_OFLAG_RDWR);
	}

	int len = rt_strlen((const char *)argv[2]);
	rt_uint8_t *rbuf = rt_malloc(len+1);
	
	rt_device_write(at24cxx_device, pos, argv[2], len);
	rt_thread_mdelay(10);
	rt_device_read(at24cxx_device, pos, rbuf, len);	
	rbuf[len] = '\0';

	if (strncmp((const char *)rbuf, (const char *)argv[2], len) != 0)
	{
		rt_kprintf("at24cxx_test failed...argv2:%s, rbuf: %s\r\n", argv[2], rbuf);
		return ;
	}

	rt_kprintf("at24cxx_test OK...wbuf:%s, rbuf: %s\r\n", argv[2], rbuf);

	rt_free(rbuf);
}
MSH_CMD_EXPORT_ALIAS(msh_at24cxx_test, at24cxx_test, test at24cxx write and read);
#endif


