/*
 * Copyright (c) 2006-2018, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2009-01-05     Bernard      the first version
 */
#include <rtthread.h>
#include <stm32f10x.h>
#include "beep.h"

// beep define
#define beep_rcc                    RCC_APB2Periph_GPIOB
#define beep_gpio                   GPIOB
#define beep_pin                    (GPIO_Pin_8)

static rt_err_t hw_beep_init(rt_device_t dev)
{
    return RT_EOK;
}

static rt_err_t hw_beep_open(rt_device_t dev, rt_uint16_t oflag)
{
    GPIO_InitTypeDef GPIO_InitStructure;

    RCC_APB2PeriphClockCmd(beep_rcc,ENABLE);

    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;

    GPIO_InitStructure.GPIO_Pin   = beep_pin;
    GPIO_Init(beep_gpio, &GPIO_InitStructure);

    return RT_EOK;
}

rt_err_t  hw_beep_close(rt_device_t dev)
{
	return RT_EOK;
}

static void rt_hw_beep_on()
{
	GPIO_SetBits(beep_gpio, beep_pin);
}

static void rt_hw_beep_off()
{
	GPIO_ResetBits(beep_gpio, beep_pin);
}

static rt_err_t rt_hw_beep_ioctl(rt_device_t dev, int cmd, void *args)
{
    RT_ASSERT(dev != RT_NULL);

    switch(cmd) {
        case IOCTL_BEEP_ON:
            rt_hw_beep_on();
            break;
            
        case IOCTL_BEEP_OFF:
            rt_hw_beep_off();
            break;

        default:break;
    }

    return RT_EOK;
}

#ifdef RT_USING_DEVICE_OPS
const static struct rt_device_ops beep_ops = 
{
    hw_beep_init,
    hw_beep_open,
    hw_beep_close,
    RT_NULL,
    RT_NULL,
    rt_hw_beep_ioctl
};
#endif

static struct rt_device beep_device;
int rt_hw_beep_init(void)
{
	/* register device */
#ifdef RT_USING_DEVICE_OPS
	beep_device.ops = &beep_ops;
#else
   
    beep_device.type      = RT_Device_Class_Char;
    beep_device.init      = hw_beep_init;
    beep_device.open      = hw_beep_open;
    beep_device.close     = hw_beep_close;
    beep_device.read      = RT_NULL;
    beep_device.write     = RT_NULL;
    beep_device.control   = rt_hw_beep_ioctl;
    beep_device.user_data = RT_NULL;
#endif

    return rt_device_register(&beep_device, "beep", RT_DEVICE_FLAG_RDWR);
}
INIT_DEVICE_EXPORT(rt_hw_beep_init);



#ifdef RT_USING_FINSH
#include <finsh.h>
void beep(rt_uint32_t value)
{
    /* 查找系统中的beeps设备*/
    rt_device_t beep_device = rt_device_find("beep");
    if (beep_device != RT_NULL)
    {
        rt_device_open(beep_device, RT_DEVICE_OFLAG_RDWR);
	}

    /* set beep status */
    switch (value)
    {
    case 0:
        rt_hw_beep_off();
        break;
    case 1:
        rt_hw_beep_on();
        break;
    default:
        break;
    }
	
	rt_device_close(beep_device);
}
FINSH_FUNCTION_EXPORT(beep, set value on[1] or off[0].);

void msh_beep(int argc, char** argv)
{	
	if (argc != 2)
    {
        rt_kprintf("Please input 'beep <on|off>' or 'beep <1|0>'\n");
        return;
    }
		
    /* 查找系统中的beeps设备*/
    rt_device_t beep_device = rt_device_find("beep");
    if (beep_device != RT_NULL)
    {
        rt_device_open(beep_device, RT_DEVICE_OFLAG_RDWR);
	}

	if (!rt_strcmp(argv[1], "on"))
    {
        rt_hw_beep_on();
    }
	else if (!rt_strcmp(argv[1], "1"))
	{
        rt_hw_beep_on();
    }
    else if (!rt_strcmp(argv[1], "off"))
    {
        rt_hw_beep_off();
    }	
	else if (!rt_strcmp(argv[1], "0"))
    {
        rt_hw_beep_off();
    }
    else
    {
        rt_kprintf("end Please input 'beep <on|off>' or 'beep <1|0>'\n");
    }

	rt_device_close(beep_device);
}
MSH_CMD_EXPORT_ALIAS(msh_beep, beep, set beep on[1] or off[0]);
#endif


