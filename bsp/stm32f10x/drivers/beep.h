/*
 *
 * Copyright (c) 2006-2018, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2009-01-05     Bernard      the first version
 */

#ifndef __BEEP_H__
#define __BEEP_H__

#include <rtthread.h>

typedef enum __BEEP_OPT_STATUS {
	IOCTL_BEEP_ON=0,
	IOCTL_BEEP_OFF,
}BEEP_OPT_STATUS;

#endif

