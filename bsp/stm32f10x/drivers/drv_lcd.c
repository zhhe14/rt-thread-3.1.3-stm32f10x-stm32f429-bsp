#include "drv_lcd.h"
#include "stdlib.h"
#include "font.h" 
#include "usart.h"	 
//#include "bsp_delay.h"	   
//////////////////////////////////////////////////////////////////////////////////	 
//本程序只供学习使用，未经作者许可，不得用于其它任何用途
//ALIENTEK战舰STM32开发板
//2.4寸/2.8寸/3.5寸/4.3寸 TFT液晶驱动	  
//支持驱动IC型号包括:ILI9341/ILI9325/RM68042/RM68021/ILI9320/ILI9328/LGDP4531/LGDP4535/
//                  SPFD5408/SSD1289/1505/B505/C505/NT35310/NT35510等		    
//正点原子@ALIENTEK
//技术论坛:www.openedv.com
//修改日期:2014/2/11
//版本：V2.5
//版权所有，盗版必究。
//Copyright(C) 广州市星翼电子科技有限公司 2009-2019
//All rights reserved	
//********************************************************************************
//V1.2修改说明
//支持了SPFD5408的驱动,另外把液晶ID直接打印成HEX格式.方便查看LCD驱动IC.
//V1.3
//加入了快速IO的支持
//修改了背光控制的极性（适用于V1.8及以后的开发板版本）
//对于1.8版本之前(不包括1.8)的液晶模块,请修改LCD_Init函数的LCD_LED=1;为LCD_LED=1;
//V1.4
//修改了LCD_ShowChar函数，使用画点功能画字符。
//加入了横竖屏显示的支持
//V1.5 20110730
//1,修改了B505液晶读颜色有误的bug.
//2,修改了快速IO及横竖屏的设置方式.
//V1.6 20111116
//1,加入对LGDP4535液晶的驱动支持
//V1.7 20120713
//1,增加lcd_read_data函数
//2,增加对ILI9341的支持
//3,增加ILI9325的独立驱动代码
//4,增加lcd_set_scan_direction函数(慎重使用)	  
//6,另外修改了部分原来的函数,以适应9341的操作
//V1.8 20120905
//1,加入LCD重要参数设置结构体lcddev
//2,加入lcd_set_display_direction函数,支持在线横竖屏切换
//V1.9 20120911
//1,新增RM68042驱动（ID:6804），但是6804不支持横屏显示！！原因：改变扫描方式，
//导致6804坐标设置失效，试过很多方法都不行，暂时无解。
//V2.0 20120924
//在不硬件复位的情况下,ILI9341的ID读取会被误读成9300,修改LCD_Init,将无法识别
//的情况（读到ID为9300/非法ID）,强制指定驱动IC为ILI9341，执行9341的初始化。
//V2.1 20120930
//修正ILI9325读颜色的bug。
//V2.2 20121007
//修正lcd_set_scan_direction的bug。
//V2.3 20130120
//新增6804支持横屏显示
//V2.4 20131120
//1,新增NT35310（ID:5310）驱动器的支持
//2,新增LCD_Set_Window函数,用于设置窗口,对快速填充,比较有用,但是该函数在横屏时,不支持6804.
//V2.5 20140211
//1,新增NT35510（ID:5510）驱动器的支持
//////////////////////////////////////////////////////////////////////////////////

//输出重定向.当不进行重定向时.
#define printf               rt_kprintf //使用rt_kprintf来输出
//#define printf(...)                       //无输出

//LCD的画笔颜色和背景色	   
u16 POINT_COLOR=0x0000;	//画笔颜色
u16 BACK_COLOR=0xFFFF;  //背景色 

//管理LCD重要参数
//默认为竖屏
static lcd_info_t lcddev;


static void delay_us(rt_uint32_t nus)
{
	//rt_thread_delay(1);
	while (nus--) {
		__NOP();
	}
}

static void delay_ms(rt_uint32_t nms)
{
	//rt_thread_delay((RT_TICK_PER_SECOND * nms + 999) / 1000);
	while (nms--)
	{
		int i;
		for (i = 0; i < 10000; i++)
		{
			__NOP();
		}
	}
}


//写寄存器函数
//regval:寄存器值
static void lcd_write_reg(rt_uint16_t regval)
{
	LCD->reg = regval; //写入要写的寄存器序号
}

//写LCD数据
//data:要写入的值
static void lcd_write_data(rt_uint16_t data)
{
	LCD->ram = data;
}

//读LCD数据
//返回值:读到的值
rt_uint16_t lcd_read_data(void)
{
	return LCD->ram;
}

//写寄存器
//LCD_Reg:寄存器地址
//LCD_RegValue:要写入的数据
static void lcd_write_reg_with_value(rt_uint16_t reg, rt_uint16_t regValue)
{
	LCD->reg = reg;			//写入要写的寄存器序号	
	LCD->ram = regValue;	//写入数据
}

//读寄存器
//LCD_Reg:寄存器地址
//返回值:读到的数据
u16 lcd_read_reg(u16 LCD_Reg)
{										   
	lcd_write_reg(LCD_Reg);		//写入要读的寄存器序号
	delay_us(5);		  
	return lcd_read_data();		//返回读到的值
}   
//开始写GRAM
static void lcd_write_data_prepare(void)
{
	LCD->reg = lcddev.wramcmd;
}

//从ILI93xx读出的数据为GBR格式，而我们写入的时候为RGB格式。
//通过该函数转换
//c:GBR格式的颜色值
//返回值：RGB格式的颜色值
rt_uint16_t lcd_bgr2rgb(rt_uint16_t value)
{
	rt_uint16_t  red, green, blue;

	blue = (value >> 0) & 0x1f;
	green = (value >> 5) & 0x3f;
	red = (value >> 11) & 0x1f;

	return ((blue << 11) + (green << 5) + (red << 0));
}

//设置光标位置
//Xpos:横坐标
//Ypos:纵坐标
static void lcd_set_cursor(rt_uint16_t Xpos, rt_uint16_t Ypos)
{	 
 	if(lcddev.id==0X9341||lcddev.id==0X5310)
	{		    
		lcd_write_reg(lcddev.setxcmd); 
		lcd_write_data(Xpos>>8); 
		lcd_write_data(Xpos&0XFF);	 
		lcd_write_reg(lcddev.setycmd); 
		lcd_write_data(Ypos>>8); 
		lcd_write_data(Ypos&0XFF);
	}else if(lcddev.id==0X6804)
	{
		if(lcddev.dir==1)Xpos=lcddev.width-1-Xpos;//横屏时处理
		lcd_write_reg(lcddev.setxcmd); 
		lcd_write_data(Xpos>>8); 
		lcd_write_data(Xpos&0XFF);	 
		lcd_write_reg(lcddev.setycmd); 
		lcd_write_data(Ypos>>8); 
		lcd_write_data(Ypos&0XFF);
	}else if(lcddev.id==0X5510)
	{
		lcd_write_reg(lcddev.setxcmd); 
		lcd_write_data(Xpos>>8); 
		lcd_write_reg(lcddev.setxcmd+1); 
		lcd_write_data(Xpos&0XFF);	 
		lcd_write_reg(lcddev.setycmd); 
		lcd_write_data(Ypos>>8); 
		lcd_write_reg(lcddev.setycmd+1); 
		lcd_write_data(Ypos&0XFF);		
	}else
	{
		if(lcddev.dir==1)Xpos=lcddev.width-1-Xpos;//横屏其实就是调转x,y坐标
		lcd_write_reg_with_value(lcddev.setxcmd, Xpos);
		lcd_write_reg_with_value(lcddev.setycmd, Ypos);
	}	 
} 


//当mdk -O1时间优化时需要设置
//延时i
void opt_delay(u8 i)
{
	while(i--);
}

//读取个某点的颜色值	 
//x,y:坐标
//返回值:此点的颜色
u16 LCD_ReadPoint(u16 x,u16 y)
{
 	u16 r=0,g=0,b=0;
	if(x>=lcddev.width||y>=lcddev.height)return 0;	//超过了范围,直接返回		   
	lcd_set_cursor(x,y);	    
	if(lcddev.id==0X9341||lcddev.id==0X6804||lcddev.id==0X5310)lcd_write_reg(0X2E);//9341/6804/3510 发送读GRAM指令
	else if(lcddev.id==0X5510)lcd_write_reg(0X2E00);	//5510 发送读GRAM指令
	else lcd_write_reg(R34);      		 				//其他IC发送读GRAM指令
 	if(lcddev.id==0X9320)opt_delay(2);				//FOR 9320,延时2us	    
	if(LCD->ram)r=0;							//dummy Read	   
	opt_delay(2);	  
 	r=LCD->ram;  		  						//实际坐标颜色
 	if(lcddev.id==0X9341||lcddev.id==0X5310||lcddev.id==0X5510)		//9341/NT35310/NT35510要分2次读出
 	{
		opt_delay(2);	  
		b=LCD->ram; 
		g=r&0XFF;		//对于9341/5310/5510,第一次读取的是RG的值,R在前,G在后,各占8位
		g<<=8;
	}else if(lcddev.id==0X6804)r=LCD->ram;		//6804第二次读取的才是真实值 
	if(lcddev.id==0X9325||lcddev.id==0X4535||lcddev.id==0X4531||lcddev.id==0X8989||lcddev.id==0XB505)return r;	//这几种IC直接返回颜色值
	else if(lcddev.id==0X9341||lcddev.id==0X5310||lcddev.id==0X5510)return (((r>>11)<<11)|((g>>10)<<5)|(b>>11));//ILI9341/NT35310/NT35510需要公式转换一下
	else return lcd_bgr2rgb(r);						//其他IC
}			 
//LCD开启显示
void LCD_DisplayOn(void)
{					   
	if(lcddev.id==0X9341||lcddev.id==0X6804||lcddev.id==0X5310)lcd_write_reg(0X29);	//开启显示
	else if(lcddev.id==0X5510)lcd_write_reg(0X2900);	//开启显示
	else lcd_write_reg_with_value(R7,0x0173); 				 	//开启显示
}	 
//LCD关闭显示
void LCD_DisplayOff(void)
{	   
	if(lcddev.id==0X9341||lcddev.id==0X6804||lcddev.id==0X5310)lcd_write_reg(0X28);	//关闭显示
	else if(lcddev.id==0X5510)lcd_write_reg(0X2800);	//关闭显示
	else lcd_write_reg_with_value(R7,0x0);//关闭显示 
}   

//设置LCD的自动扫描方向
//注意:其他函数可能会受到此函数设置的影响(尤其是9341/6804这两个奇葩),
//所以,一般设置为L2R_U2D即可,如果设置为其他扫描方式,可能导致显示不正常.
//dir:0~7,代表8个方向(具体定义见lcd.h)
//9320/9325/9328/4531/4535/1505/b505/8989/5408/9341/5310/5510等IC已经实际测试	   
static void lcd_set_scan_direction(rt_uint8_t dir)
{
	rt_uint16_t regval = 0;
	rt_uint16_t dirreg = 0;
	rt_uint16_t temp;
	
	if(lcddev.dir==1&&lcddev.id!=0X6804)//横屏时，对6804不改变扫描方向！
	{			   
		switch(dir)//方向转换
		{
			case 0:dir=6;break;
			case 1:dir=7;break;
			case 2:dir=4;break;
			case 3:dir=5;break;
			case 4:dir=1;break;
			case 5:dir=0;break;
			case 6:dir=3;break;
			case 7:dir=2;break;	     
		}
	}
	if(lcddev.id==0x9341||lcddev.id==0X6804||lcddev.id==0X5310||lcddev.id==0X5510)//9341/6804/5310/5510,很特殊
	{
		switch(dir)
		{
			case L2R_U2D://从左到右,从上到下
				regval|=(0<<7)|(0<<6)|(0<<5); 
				break;
			case L2R_D2U://从左到右,从下到上
				regval|=(1<<7)|(0<<6)|(0<<5); 
				break;
			case R2L_U2D://从右到左,从上到下
				regval|=(0<<7)|(1<<6)|(0<<5); 
				break;
			case R2L_D2U://从右到左,从下到上
				regval|=(1<<7)|(1<<6)|(0<<5); 
				break;	 
			case U2D_L2R://从上到下,从左到右
				regval|=(0<<7)|(0<<6)|(1<<5); 
				break;
			case U2D_R2L://从上到下,从右到左
				regval|=(0<<7)|(1<<6)|(1<<5); 
				break;
			case D2U_L2R://从下到上,从左到右
				regval|=(1<<7)|(0<<6)|(1<<5); 
				break;
			case D2U_R2L://从下到上,从右到左
				regval|=(1<<7)|(1<<6)|(1<<5); 
				break;	 
		}
		if(lcddev.id==0X5510)dirreg=0X3600;
		else dirreg=0X36;
 		if((lcddev.id!=0X5310)&&(lcddev.id!=0X5510))regval|=0X08;//5310/5510不需要BGR   
		if(lcddev.id==0X6804)regval|=0x02;//6804的BIT6和9341的反了	   
		lcd_write_reg_with_value(dirreg,regval);
 		if((regval&0X20)||lcddev.dir==1)
		{
			if(lcddev.width<lcddev.height)//交换X,Y
			{
				temp=lcddev.width;
				lcddev.width=lcddev.height;
				lcddev.height=temp;
 			}
		}else  
		{
			if(lcddev.width>lcddev.height)//交换X,Y
			{
				temp=lcddev.width;
				lcddev.width=lcddev.height;
				lcddev.height=temp;
 			}
		}  
		if(lcddev.id==0X5510)
		{
			lcd_write_reg(lcddev.setxcmd);lcd_write_data(0); 
			lcd_write_reg(lcddev.setxcmd+1);lcd_write_data(0); 
			lcd_write_reg(lcddev.setxcmd+2);lcd_write_data((lcddev.width-1)>>8); 
			lcd_write_reg(lcddev.setxcmd+3);lcd_write_data((lcddev.width-1)&0XFF); 
			lcd_write_reg(lcddev.setycmd);lcd_write_data(0); 
			lcd_write_reg(lcddev.setycmd+1);lcd_write_data(0); 
			lcd_write_reg(lcddev.setycmd+2);lcd_write_data((lcddev.height-1)>>8); 
			lcd_write_reg(lcddev.setycmd+3);lcd_write_data((lcddev.height-1)&0XFF);
		}else
		{
			lcd_write_reg(lcddev.setxcmd); 
			lcd_write_data(0);lcd_write_data(0);
			lcd_write_data((lcddev.width-1)>>8);lcd_write_data((lcddev.width-1)&0XFF);
			lcd_write_reg(lcddev.setycmd); 
			lcd_write_data(0);lcd_write_data(0);
			lcd_write_data((lcddev.height-1)>>8);lcd_write_data((lcddev.height-1)&0XFF);  
		}
  	}else 
	{
		switch(dir)
		{
			case L2R_U2D://从左到右,从上到下
				regval|=(1<<5)|(1<<4)|(0<<3); 
				break;
			case L2R_D2U://从左到右,从下到上
				regval|=(0<<5)|(1<<4)|(0<<3); 
				break;
			case R2L_U2D://从右到左,从上到下
				regval|=(1<<5)|(0<<4)|(0<<3);
				break;
			case R2L_D2U://从右到左,从下到上
				regval|=(0<<5)|(0<<4)|(0<<3); 
				break;	 
			case U2D_L2R://从上到下,从左到右
				regval|=(1<<5)|(1<<4)|(1<<3); 
				break;
			case U2D_R2L://从上到下,从右到左
				regval|=(1<<5)|(0<<4)|(1<<3); 
				break;
			case D2U_L2R://从下到上,从左到右
				regval|=(0<<5)|(1<<4)|(1<<3); 
				break;
			case D2U_R2L://从下到上,从右到左
				regval|=(0<<5)|(0<<4)|(1<<3); 
				break;	 
		}
		if(lcddev.id==0x8989)//8989 IC
		{
			dirreg=0X11;
			regval|=0X6040;	//65K   
	 	}else//其他驱动IC		  
		{
			dirreg=0X03;
			regval|=1<<12;  
		}
		lcd_write_reg_with_value(dirreg,regval);
	}
}   
//画点
//x,y:坐标
//POINT_COLOR:此点的颜色
void LCD_DrawPoint(u16 x,u16 y)
{
	lcd_set_cursor(x,y);		//设置光标位置 
	lcd_write_data_prepare();	//开始写入GRAM
	LCD->ram=POINT_COLOR; 
}
//快速画点
//x,y:坐标
//color:颜色
void LCD_Fast_DrawPoint(u16 x,u16 y,u16 color)
{	   
	if(lcddev.id==0X9341||lcddev.id==0X5310)
	{
		lcd_write_reg(lcddev.setxcmd); 
		lcd_write_data(x>>8); 
		lcd_write_data(x&0XFF);	 
		lcd_write_reg(lcddev.setycmd); 
		lcd_write_data(y>>8); 
		lcd_write_data(y&0XFF);
	}else if(lcddev.id==0X5510)
	{
		lcd_write_reg(lcddev.setxcmd);lcd_write_data(x>>8);  
		lcd_write_reg(lcddev.setxcmd+1);lcd_write_data(x&0XFF);	  
		lcd_write_reg(lcddev.setycmd);lcd_write_data(y>>8);  
		lcd_write_reg(lcddev.setycmd+1);lcd_write_data(y&0XFF); 
	}else if(lcddev.id==0X6804)
	{		    
		if(lcddev.dir==1)x=lcddev.width-1-x;//横屏时处理
		lcd_write_reg(lcddev.setxcmd); 
		lcd_write_data(x>>8); 
		lcd_write_data(x&0XFF);	 
		lcd_write_reg(lcddev.setycmd); 
		lcd_write_data(y>>8); 
		lcd_write_data(y&0XFF);
	}else
	{
 		if(lcddev.dir==1)x=lcddev.width-1-x;//横屏其实就是调转x,y坐标
		lcd_write_reg_with_value(lcddev.setxcmd,x);
		lcd_write_reg_with_value(lcddev.setycmd,y);
	}			 
	LCD->reg=lcddev.wramcmd; 
	LCD->ram=color; 
}	 


//设置LCD显示方向
//dir:0,竖屏；1,横屏
void lcd_set_display_direction(rt_uint8_t dir)
{
	if(dir==0)			//竖屏
	{
		lcddev.dir=0;	//竖屏
		lcddev.width=240;
		lcddev.height=320;
		if(lcddev.id==0X9341||lcddev.id==0X6804||lcddev.id==0X5310)
		{
			lcddev.wramcmd=0X2C;
	 		lcddev.setxcmd=0X2A;
			lcddev.setycmd=0X2B;  	 
			if(lcddev.id==0X6804||lcddev.id==0X5310)
			{
				lcddev.width=320;
				lcddev.height=480;
			}
		}else if(lcddev.id==0X8989)
		{
			lcddev.wramcmd=R34;
	 		lcddev.setxcmd=0X4E;
			lcddev.setycmd=0X4F;  
		}else if(lcddev.id==0x5510)
		{
			lcddev.wramcmd=0X2C00;
	 		lcddev.setxcmd=0X2A00;
			lcddev.setycmd=0X2B00; 
			lcddev.width=480;
			lcddev.height=800;
		}else
		{
			lcddev.wramcmd=R34;
	 		lcddev.setxcmd=R32;
			lcddev.setycmd=R33;  
		}
	}else 				//横屏
	{	  				
		lcddev.dir=1;	//横屏
		lcddev.width=320;
		lcddev.height=240;
		if(lcddev.id==0X9341||lcddev.id==0X5310)
		{
			lcddev.wramcmd=0X2C;
	 		lcddev.setxcmd=0X2A;
			lcddev.setycmd=0X2B;  	 
		}else if(lcddev.id==0X6804)	 
		{
 			lcddev.wramcmd=0X2C;
	 		lcddev.setxcmd=0X2B;
			lcddev.setycmd=0X2A; 
		}else if(lcddev.id==0X8989)
		{
			lcddev.wramcmd=R34;
	 		lcddev.setxcmd=0X4F;
			lcddev.setycmd=0X4E;   
		}else if(lcddev.id==0x5510)
		{
			lcddev.wramcmd=0X2C00;
	 		lcddev.setxcmd=0X2A00;
			lcddev.setycmd=0X2B00; 
			lcddev.width=800;
			lcddev.height=480;
		}else
		{
			lcddev.wramcmd=R34;
	 		lcddev.setxcmd=R33;
			lcddev.setycmd=R32;  
		}
		if(lcddev.id==0X6804||lcddev.id==0X5310)
		{ 	 
			lcddev.width=480;
			lcddev.height=320; 			
		}
	} 
	lcd_set_scan_direction(DFT_SCAN_DIR);	//默认扫描方向
}	

//设置窗口,并自动设置画点坐标到窗口左上角(sx,sy).
//sx,sy:窗口起始坐标(左上角)
//width,height:窗口宽度和高度,必须大于0!!
//窗体大小:width*height.
//68042,横屏时不支持窗口设置!! 
void LCD_Set_Window(u16 sx,u16 sy,u16 width,u16 height)
{   
	u8 hsareg,heareg,vsareg,veareg;
	u16 hsaval,heaval,vsaval,veaval; 
	width=sx+width-1;
	height=sy+height-1;
	if(lcddev.id==0X9341||lcddev.id==0X5310||lcddev.id==0X6804)//6804横屏不支持
	{
		lcd_write_reg(lcddev.setxcmd); 
		lcd_write_data(sx>>8); 
		lcd_write_data(sx&0XFF);	 
		lcd_write_data(width>>8); 
		lcd_write_data(width&0XFF);  
		lcd_write_reg(lcddev.setycmd); 
		lcd_write_data(sy>>8); 
		lcd_write_data(sy&0XFF); 
		lcd_write_data(height>>8); 
		lcd_write_data(height&0XFF); 
	}else if(lcddev.id==0X5510)
	{
		lcd_write_reg(lcddev.setxcmd);lcd_write_data(sx>>8);  
		lcd_write_reg(lcddev.setxcmd+1);lcd_write_data(sx&0XFF);	  
		lcd_write_reg(lcddev.setxcmd+2);lcd_write_data(width>>8);   
		lcd_write_reg(lcddev.setxcmd+3);lcd_write_data(width&0XFF);   
		lcd_write_reg(lcddev.setycmd);lcd_write_data(sy>>8);   
		lcd_write_reg(lcddev.setycmd+1);lcd_write_data(sy&0XFF);  
		lcd_write_reg(lcddev.setycmd+2);lcd_write_data(height>>8);   
		lcd_write_reg(lcddev.setycmd+3);lcd_write_data(height&0XFF);  
	}else	//其他驱动IC
	{
		if(lcddev.dir==1)//横屏
		{
			//窗口值
			hsaval=sy;				
			heaval=height;
			vsaval=lcddev.width-width-1;
			veaval=lcddev.width-sx-1;				
		}else
		{ 
			hsaval=sx;				
			heaval=width;
			vsaval=sy;
			veaval=height;
		}
	 	if(lcddev.id==0X8989)//8989 IC
		{
			hsareg=0X44;heareg=0X44;//水平方向窗口寄存器 (1289的由一个寄存器控制)
			hsaval|=(heaval<<8);	//得到寄存器值.
			heaval=hsaval;
			vsareg=0X45;veareg=0X46;//垂直方向窗口寄存器	  
		}else  //其他驱动IC
		{
			hsareg=0X50;heareg=0X51;//水平方向窗口寄存器
			vsareg=0X52;veareg=0X53;//垂直方向窗口寄存器	  
		}								  
		//设置寄存器值
		lcd_write_reg_with_value(hsareg,hsaval);
		lcd_write_reg_with_value(heareg,heaval);
		lcd_write_reg_with_value(vsareg,vsaval);
		lcd_write_reg_with_value(veareg,veaval);		
		lcd_set_cursor(sx,sy);	//设置光标位置
	}
} 
//初始化lcd
//该初始化函数可以初始化各种ILI93XX液晶,但是其他函数是基于ILI9320的!!!
//在其他型号的驱动芯片上没有测试! 
void LCD_Init(void)
{ 										  
 	GPIO_InitTypeDef GPIO_InitStructure;
	FSMC_NORSRAMInitTypeDef  FSMC_NORSRAMInitStructure;
    FSMC_NORSRAMTimingInitTypeDef  readWriteTiming; 
	FSMC_NORSRAMTimingInitTypeDef  writeTiming;

    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_FSMC,ENABLE);	//使能FSMC时钟
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB|RCC_APB2Periph_GPIOD|RCC_APB2Periph_GPIOE|RCC_APB2Periph_GPIOG|RCC_APB2Periph_AFIO,ENABLE);//使能PORTB,D,E,G以及AFIO复用功能时钟

 	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;				 //PB0 推挽输出 背光
 	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 		 //推挽输出
 	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
 	GPIO_Init(GPIOB, &GPIO_InitStructure);

 	//PORTD复用推挽输出  
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0|GPIO_Pin_1|GPIO_Pin_4|GPIO_Pin_5|GPIO_Pin_8|GPIO_Pin_9|GPIO_Pin_10|GPIO_Pin_14|GPIO_Pin_15;				 //	//PORTD复用推挽输出  
 	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP; 		 //复用推挽输出   
 	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
 	GPIO_Init(GPIOD, &GPIO_InitStructure); 
	  
	//PORTE复用推挽输出  
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7|GPIO_Pin_8|GPIO_Pin_9|GPIO_Pin_10|GPIO_Pin_11|GPIO_Pin_12|GPIO_Pin_13|GPIO_Pin_14|GPIO_Pin_15;				 //	//PORTD复用推挽输出  
 	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP; 		 //复用推挽输出   
 	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
 	GPIO_Init(GPIOE, &GPIO_InitStructure); 
	  
   	//	//PORTG12复用推挽输出 A0	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0|GPIO_Pin_12;	 //	//PORTD复用推挽输出  
 	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP; 		 //复用推挽输出   
 	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
 	GPIO_Init(GPIOG, &GPIO_InitStructure); 
 
	readWriteTiming.FSMC_AddressSetupTime = 0x01;	 //地址建立时间（ADDSET）为2个HCLK 1/36M=27ns
    readWriteTiming.FSMC_AddressHoldTime = 0x00;	 //地址保持时间（ADDHLD）模式A未用到	
    readWriteTiming.FSMC_DataSetupTime = 0x0f;		 // 数据保存时间为16个HCLK,因为液晶驱动IC的读数据的时候，速度不能太快，尤其对1289这个IC。
    readWriteTiming.FSMC_BusTurnAroundDuration = 0x00;
    readWriteTiming.FSMC_CLKDivision = 0x00;
    readWriteTiming.FSMC_DataLatency = 0x00;
    readWriteTiming.FSMC_AccessMode = FSMC_AccessMode_A;	 //模式A 
    

	writeTiming.FSMC_AddressSetupTime = 0x00;	 //地址建立时间（ADDSET）为1个HCLK  
    writeTiming.FSMC_AddressHoldTime = 0x00;	 //地址保持时间（A		
    writeTiming.FSMC_DataSetupTime = 0x03;		 ////数据保存时间为4个HCLK	
    writeTiming.FSMC_BusTurnAroundDuration = 0x00;
    writeTiming.FSMC_CLKDivision = 0x00;
    writeTiming.FSMC_DataLatency = 0x00;
    writeTiming.FSMC_AccessMode = FSMC_AccessMode_A;	 //模式A 

 
    FSMC_NORSRAMInitStructure.FSMC_Bank = FSMC_Bank1_NORSRAM4;//  这里我们使用NE4 ，也就对应BTCR[6],[7]。
    FSMC_NORSRAMInitStructure.FSMC_DataAddressMux = FSMC_DataAddressMux_Disable; // 不复用数据地址
    FSMC_NORSRAMInitStructure.FSMC_MemoryType =FSMC_MemoryType_SRAM;// FSMC_MemoryType_SRAM;  //SRAM   
    FSMC_NORSRAMInitStructure.FSMC_MemoryDataWidth = FSMC_MemoryDataWidth_16b;//存储器数据宽度为16bit   
    FSMC_NORSRAMInitStructure.FSMC_BurstAccessMode =FSMC_BurstAccessMode_Disable;// FSMC_BurstAccessMode_Disable; 
    FSMC_NORSRAMInitStructure.FSMC_WaitSignalPolarity = FSMC_WaitSignalPolarity_Low;
	FSMC_NORSRAMInitStructure.FSMC_AsynchronousWait=FSMC_AsynchronousWait_Disable; 
    FSMC_NORSRAMInitStructure.FSMC_WrapMode = FSMC_WrapMode_Disable;   
    FSMC_NORSRAMInitStructure.FSMC_WaitSignalActive = FSMC_WaitSignalActive_BeforeWaitState;  
    FSMC_NORSRAMInitStructure.FSMC_WriteOperation = FSMC_WriteOperation_Enable;	//  存储器写使能
    FSMC_NORSRAMInitStructure.FSMC_WaitSignal = FSMC_WaitSignal_Disable;   
    FSMC_NORSRAMInitStructure.FSMC_ExtendedMode = FSMC_ExtendedMode_Enable; // 读写使用不同的时序
    FSMC_NORSRAMInitStructure.FSMC_WriteBurst = FSMC_WriteBurst_Disable; 
    FSMC_NORSRAMInitStructure.FSMC_ReadWriteTimingStruct = &readWriteTiming; //读写时序
    FSMC_NORSRAMInitStructure.FSMC_WriteTimingStruct = &writeTiming;  //写时序

    FSMC_NORSRAMInit(&FSMC_NORSRAMInitStructure);  //初始化FSMC配置

   	FSMC_NORSRAMCmd(FSMC_Bank1_NORSRAM4, ENABLE);  // 使能BANK1 
				 
 	delay_ms(50); // delay 50 ms 
 	lcd_write_reg_with_value(0x0000,0x0001);
	delay_ms(50); // delay 50 ms 
  	lcddev.id = lcd_read_reg(0x0000);   
  	if(lcddev.id<0XFF||lcddev.id==0XFFFF||lcddev.id==0X9300)//读到ID不正确,新增lcddev.id==0X9300判断，因为9341在未被复位的情况下会被读成9300
	{	
 		//尝试9341 ID的读取		
		lcd_write_reg(0XD3);				   
		lcd_read_data(); 				//dummy read 	
 		lcd_read_data();   	    	//读到0X00
  		lcddev.id=lcd_read_data();   	//读取93								   
 		lcddev.id<<=8;
		lcddev.id|=lcd_read_data();  	//读取41 	   			   
 		if(lcddev.id!=0X9341)		//非9341,尝试是不是6804
		{	
 			lcd_write_reg(0XBF);				   
			lcd_read_data(); 			//dummy read 	 
	 		lcd_read_data();   	    //读回0X01			   
	 		lcd_read_data(); 			//读回0XD0 			  	
	  		lcddev.id=lcd_read_data();//这里读回0X68 
			lcddev.id<<=8;
	  		lcddev.id|=lcd_read_data();//这里读回0X04	  
			if(lcddev.id!=0X6804)	//也不是6804,尝试看看是不是NT35310
			{ 
				lcd_write_reg(0XD4);				   
				lcd_read_data(); 				//dummy read  
				lcd_read_data();   			//读回0X01	 
				lcddev.id=lcd_read_data();	//读回0X53	
				lcddev.id<<=8;	 
				lcddev.id|=lcd_read_data();	//这里读回0X10	 
				if(lcddev.id!=0X5310)		//也不是NT35310,尝试看看是不是NT35510
				{
					lcd_write_reg(0XDA00);	
					lcd_read_data();   		//读回0X00	 
					lcd_write_reg(0XDB00);	
					lcddev.id=lcd_read_data();//读回0X80
					lcddev.id<<=8;	
					lcd_write_reg(0XDC00);	
					lcddev.id|=lcd_read_data();//读回0X00		
					if(lcddev.id==0x8000)lcddev.id=0x5510;//NT35510读回的ID是8000H,为方便区分,我们强制设置为5510
				}
			}
 		}  	
	} 
 	printf(" LCD ID:%x\r\n",lcddev.id); //打印LCD ID  
	if(lcddev.id==0X9341)	//9341初始化
	{	 
		lcd_write_reg(0xCF);  
		lcd_write_data(0x00); 
		lcd_write_data(0xC1); 
		lcd_write_data(0X30); 
		lcd_write_reg(0xED);  
		lcd_write_data(0x64); 
		lcd_write_data(0x03); 
		lcd_write_data(0X12); 
		lcd_write_data(0X81); 
		lcd_write_reg(0xE8);  
		lcd_write_data(0x85); 
		lcd_write_data(0x10); 
		lcd_write_data(0x7A); 
		lcd_write_reg(0xCB);  
		lcd_write_data(0x39); 
		lcd_write_data(0x2C); 
		lcd_write_data(0x00); 
		lcd_write_data(0x34); 
		lcd_write_data(0x02); 
		lcd_write_reg(0xF7);  
		lcd_write_data(0x20); 
		lcd_write_reg(0xEA);  
		lcd_write_data(0x00); 
		lcd_write_data(0x00); 
		lcd_write_reg(0xC0);    //Power control 
		lcd_write_data(0x1B);   //VRH[5:0] 
		lcd_write_reg(0xC1);    //Power control 
		lcd_write_data(0x01);   //SAP[2:0];BT[3:0] 
		lcd_write_reg(0xC5);    //VCM control 
		lcd_write_data(0x30); 	 //3F
		lcd_write_data(0x30); 	 //3C
		lcd_write_reg(0xC7);    //VCM control2 
		lcd_write_data(0XB7); 
		lcd_write_reg(0x36);    // Memory Access Control 
		lcd_write_data(0x48); 
		lcd_write_reg(0x3A);   
		lcd_write_data(0x55); 
		lcd_write_reg(0xB1);   
		lcd_write_data(0x00);   
		lcd_write_data(0x1A); 
		lcd_write_reg(0xB6);    // Display Function Control 
		lcd_write_data(0x0A); 
		lcd_write_data(0xA2); 
		lcd_write_reg(0xF2);    // 3Gamma Function Disable 
		lcd_write_data(0x00); 
		lcd_write_reg(0x26);    //Gamma curve selected 
		lcd_write_data(0x01); 
		lcd_write_reg(0xE0);    //Set Gamma 
		lcd_write_data(0x0F); 
		lcd_write_data(0x2A); 
		lcd_write_data(0x28); 
		lcd_write_data(0x08); 
		lcd_write_data(0x0E); 
		lcd_write_data(0x08); 
		lcd_write_data(0x54); 
		lcd_write_data(0XA9); 
		lcd_write_data(0x43); 
		lcd_write_data(0x0A); 
		lcd_write_data(0x0F); 
		lcd_write_data(0x00); 
		lcd_write_data(0x00); 
		lcd_write_data(0x00); 
		lcd_write_data(0x00); 		 
		lcd_write_reg(0XE1);    //Set Gamma 
		lcd_write_data(0x00); 
		lcd_write_data(0x15); 
		lcd_write_data(0x17); 
		lcd_write_data(0x07); 
		lcd_write_data(0x11); 
		lcd_write_data(0x06); 
		lcd_write_data(0x2B); 
		lcd_write_data(0x56); 
		lcd_write_data(0x3C); 
		lcd_write_data(0x05); 
		lcd_write_data(0x10); 
		lcd_write_data(0x0F); 
		lcd_write_data(0x3F); 
		lcd_write_data(0x3F); 
		lcd_write_data(0x0F); 
		lcd_write_reg(0x2B); 
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x01);
		lcd_write_data(0x3f);
		lcd_write_reg(0x2A); 
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0xef);	 
		lcd_write_reg(0x11); //Exit Sleep
		delay_ms(120);
		lcd_write_reg(0x29); //display on	
	}else if(lcddev.id==0x6804) //6804初始化
	{
		lcd_write_reg(0X11);
		delay_ms(20);
		lcd_write_reg(0XD0);//VCI1  VCL  VGH  VGL DDVDH VREG1OUT power amplitude setting
		lcd_write_data(0X07); 
		lcd_write_data(0X42); 
		lcd_write_data(0X1D); 
		lcd_write_reg(0XD1);//VCOMH VCOM_AC amplitude setting
		lcd_write_data(0X00);
		lcd_write_data(0X1a);
		lcd_write_data(0X09); 
		lcd_write_reg(0XD2);//Operational Amplifier Circuit Constant Current Adjust , charge pump frequency setting
		lcd_write_data(0X01);
		lcd_write_data(0X22);
		lcd_write_reg(0XC0);//REV SM GS 
		lcd_write_data(0X10);
		lcd_write_data(0X3B);
		lcd_write_data(0X00);
		lcd_write_data(0X02);
		lcd_write_data(0X11);
		
		lcd_write_reg(0XC5);// Frame rate setting = 72HZ  when setting 0x03
		lcd_write_data(0X03);
		
		lcd_write_reg(0XC8);//Gamma setting
		lcd_write_data(0X00);
		lcd_write_data(0X25);
		lcd_write_data(0X21);
		lcd_write_data(0X05);
		lcd_write_data(0X00);
		lcd_write_data(0X0a);
		lcd_write_data(0X65);
		lcd_write_data(0X25);
		lcd_write_data(0X77);
		lcd_write_data(0X50);
		lcd_write_data(0X0f);
		lcd_write_data(0X00);	  
						  
   		lcd_write_reg(0XF8);
		lcd_write_data(0X01);	  

 		lcd_write_reg(0XFE);
 		lcd_write_data(0X00);
 		lcd_write_data(0X02);
		
		lcd_write_reg(0X20);//Exit invert mode

		lcd_write_reg(0X36);
		lcd_write_data(0X08);//原来是a
		
		lcd_write_reg(0X3A);
		lcd_write_data(0X55);//16位模式	  
		lcd_write_reg(0X2B);
		lcd_write_data(0X00);
		lcd_write_data(0X00);
		lcd_write_data(0X01);
		lcd_write_data(0X3F);
		
		lcd_write_reg(0X2A);
		lcd_write_data(0X00);
		lcd_write_data(0X00);
		lcd_write_data(0X01);
		lcd_write_data(0XDF);
		delay_ms(120);
		lcd_write_reg(0X29); 	 
 	}else if(lcddev.id==0x5310)
	{ 
		lcd_write_reg(0xED);
		lcd_write_data(0x01);
		lcd_write_data(0xFE);

		lcd_write_reg(0xEE);
		lcd_write_data(0xDE);
		lcd_write_data(0x21);

		lcd_write_reg(0xF1);
		lcd_write_data(0x01);
		lcd_write_reg(0xDF);
		lcd_write_data(0x10);

		//VCOMvoltage//
		lcd_write_reg(0xC4);
		lcd_write_data(0x8F);	  //5f

		lcd_write_reg(0xC6);
		lcd_write_data(0x00);
		lcd_write_data(0xE2);
		lcd_write_data(0xE2);
		lcd_write_data(0xE2);
		lcd_write_reg(0xBF);
		lcd_write_data(0xAA);

		lcd_write_reg(0xB0);
		lcd_write_data(0x0D);
		lcd_write_data(0x00);
		lcd_write_data(0x0D);
		lcd_write_data(0x00);
		lcd_write_data(0x11);
		lcd_write_data(0x00);
		lcd_write_data(0x19);
		lcd_write_data(0x00);
		lcd_write_data(0x21);
		lcd_write_data(0x00);
		lcd_write_data(0x2D);
		lcd_write_data(0x00);
		lcd_write_data(0x3D);
		lcd_write_data(0x00);
		lcd_write_data(0x5D);
		lcd_write_data(0x00);
		lcd_write_data(0x5D);
		lcd_write_data(0x00);

		lcd_write_reg(0xB1);
		lcd_write_data(0x80);
		lcd_write_data(0x00);
		lcd_write_data(0x8B);
		lcd_write_data(0x00);
		lcd_write_data(0x96);
		lcd_write_data(0x00);

		lcd_write_reg(0xB2);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x02);
		lcd_write_data(0x00);
		lcd_write_data(0x03);
		lcd_write_data(0x00);

		lcd_write_reg(0xB3);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);

		lcd_write_reg(0xB4);
		lcd_write_data(0x8B);
		lcd_write_data(0x00);
		lcd_write_data(0x96);
		lcd_write_data(0x00);
		lcd_write_data(0xA1);
		lcd_write_data(0x00);

		lcd_write_reg(0xB5);
		lcd_write_data(0x02);
		lcd_write_data(0x00);
		lcd_write_data(0x03);
		lcd_write_data(0x00);
		lcd_write_data(0x04);
		lcd_write_data(0x00);

		lcd_write_reg(0xB6);
		lcd_write_data(0x00);
		lcd_write_data(0x00);

		lcd_write_reg(0xB7);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x3F);
		lcd_write_data(0x00);
		lcd_write_data(0x5E);
		lcd_write_data(0x00);
		lcd_write_data(0x64);
		lcd_write_data(0x00);
		lcd_write_data(0x8C);
		lcd_write_data(0x00);
		lcd_write_data(0xAC);
		lcd_write_data(0x00);
		lcd_write_data(0xDC);
		lcd_write_data(0x00);
		lcd_write_data(0x70);
		lcd_write_data(0x00);
		lcd_write_data(0x90);
		lcd_write_data(0x00);
		lcd_write_data(0xEB);
		lcd_write_data(0x00);
		lcd_write_data(0xDC);
		lcd_write_data(0x00);

		lcd_write_reg(0xB8);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);

		lcd_write_reg(0xBA);
		lcd_write_data(0x24);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);

		lcd_write_reg(0xC1);
		lcd_write_data(0x20);
		lcd_write_data(0x00);
		lcd_write_data(0x54);
		lcd_write_data(0x00);
		lcd_write_data(0xFF);
		lcd_write_data(0x00);

		lcd_write_reg(0xC2);
		lcd_write_data(0x0A);
		lcd_write_data(0x00);
		lcd_write_data(0x04);
		lcd_write_data(0x00);

		lcd_write_reg(0xC3);
		lcd_write_data(0x3C);
		lcd_write_data(0x00);
		lcd_write_data(0x3A);
		lcd_write_data(0x00);
		lcd_write_data(0x39);
		lcd_write_data(0x00);
		lcd_write_data(0x37);
		lcd_write_data(0x00);
		lcd_write_data(0x3C);
		lcd_write_data(0x00);
		lcd_write_data(0x36);
		lcd_write_data(0x00);
		lcd_write_data(0x32);
		lcd_write_data(0x00);
		lcd_write_data(0x2F);
		lcd_write_data(0x00);
		lcd_write_data(0x2C);
		lcd_write_data(0x00);
		lcd_write_data(0x29);
		lcd_write_data(0x00);
		lcd_write_data(0x26);
		lcd_write_data(0x00);
		lcd_write_data(0x24);
		lcd_write_data(0x00);
		lcd_write_data(0x24);
		lcd_write_data(0x00);
		lcd_write_data(0x23);
		lcd_write_data(0x00);
		lcd_write_data(0x3C);
		lcd_write_data(0x00);
		lcd_write_data(0x36);
		lcd_write_data(0x00);
		lcd_write_data(0x32);
		lcd_write_data(0x00);
		lcd_write_data(0x2F);
		lcd_write_data(0x00);
		lcd_write_data(0x2C);
		lcd_write_data(0x00);
		lcd_write_data(0x29);
		lcd_write_data(0x00);
		lcd_write_data(0x26);
		lcd_write_data(0x00);
		lcd_write_data(0x24);
		lcd_write_data(0x00);
		lcd_write_data(0x24);
		lcd_write_data(0x00);
		lcd_write_data(0x23);
		lcd_write_data(0x00);

		lcd_write_reg(0xC4);
		lcd_write_data(0x62);
		lcd_write_data(0x00);
		lcd_write_data(0x05);
		lcd_write_data(0x00);
		lcd_write_data(0x84);
		lcd_write_data(0x00);
		lcd_write_data(0xF0);
		lcd_write_data(0x00);
		lcd_write_data(0x18);
		lcd_write_data(0x00);
		lcd_write_data(0xA4);
		lcd_write_data(0x00);
		lcd_write_data(0x18);
		lcd_write_data(0x00);
		lcd_write_data(0x50);
		lcd_write_data(0x00);
		lcd_write_data(0x0C);
		lcd_write_data(0x00);
		lcd_write_data(0x17);
		lcd_write_data(0x00);
		lcd_write_data(0x95);
		lcd_write_data(0x00);
		lcd_write_data(0xF3);
		lcd_write_data(0x00);
		lcd_write_data(0xE6);
		lcd_write_data(0x00);

		lcd_write_reg(0xC5);
		lcd_write_data(0x32);
		lcd_write_data(0x00);
		lcd_write_data(0x44);
		lcd_write_data(0x00);
		lcd_write_data(0x65);
		lcd_write_data(0x00);
		lcd_write_data(0x76);
		lcd_write_data(0x00);
		lcd_write_data(0x88);
		lcd_write_data(0x00);

		lcd_write_reg(0xC6);
		lcd_write_data(0x20);
		lcd_write_data(0x00);
		lcd_write_data(0x17);
		lcd_write_data(0x00);
		lcd_write_data(0x01);
		lcd_write_data(0x00);

		lcd_write_reg(0xC7);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);

		lcd_write_reg(0xC8);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);

		lcd_write_reg(0xC9);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);

		lcd_write_reg(0xE0);
		lcd_write_data(0x16);
		lcd_write_data(0x00);
		lcd_write_data(0x1C);
		lcd_write_data(0x00);
		lcd_write_data(0x21);
		lcd_write_data(0x00);
		lcd_write_data(0x36);
		lcd_write_data(0x00);
		lcd_write_data(0x46);
		lcd_write_data(0x00);
		lcd_write_data(0x52);
		lcd_write_data(0x00);
		lcd_write_data(0x64);
		lcd_write_data(0x00);
		lcd_write_data(0x7A);
		lcd_write_data(0x00);
		lcd_write_data(0x8B);
		lcd_write_data(0x00);
		lcd_write_data(0x99);
		lcd_write_data(0x00);
		lcd_write_data(0xA8);
		lcd_write_data(0x00);
		lcd_write_data(0xB9);
		lcd_write_data(0x00);
		lcd_write_data(0xC4);
		lcd_write_data(0x00);
		lcd_write_data(0xCA);
		lcd_write_data(0x00);
		lcd_write_data(0xD2);
		lcd_write_data(0x00);
		lcd_write_data(0xD9);
		lcd_write_data(0x00);
		lcd_write_data(0xE0);
		lcd_write_data(0x00);
		lcd_write_data(0xF3);
		lcd_write_data(0x00);

		lcd_write_reg(0xE1);
		lcd_write_data(0x16);
		lcd_write_data(0x00);
		lcd_write_data(0x1C);
		lcd_write_data(0x00);
		lcd_write_data(0x22);
		lcd_write_data(0x00);
		lcd_write_data(0x36);
		lcd_write_data(0x00);
		lcd_write_data(0x45);
		lcd_write_data(0x00);
		lcd_write_data(0x52);
		lcd_write_data(0x00);
		lcd_write_data(0x64);
		lcd_write_data(0x00);
		lcd_write_data(0x7A);
		lcd_write_data(0x00);
		lcd_write_data(0x8B);
		lcd_write_data(0x00);
		lcd_write_data(0x99);
		lcd_write_data(0x00);
		lcd_write_data(0xA8);
		lcd_write_data(0x00);
		lcd_write_data(0xB9);
		lcd_write_data(0x00);
		lcd_write_data(0xC4);
		lcd_write_data(0x00);
		lcd_write_data(0xCA);
		lcd_write_data(0x00);
		lcd_write_data(0xD2);
		lcd_write_data(0x00);
		lcd_write_data(0xD8);
		lcd_write_data(0x00);
		lcd_write_data(0xE0);
		lcd_write_data(0x00);
		lcd_write_data(0xF3);
		lcd_write_data(0x00);

		lcd_write_reg(0xE2);
		lcd_write_data(0x05);
		lcd_write_data(0x00);
		lcd_write_data(0x0B);
		lcd_write_data(0x00);
		lcd_write_data(0x1B);
		lcd_write_data(0x00);
		lcd_write_data(0x34);
		lcd_write_data(0x00);
		lcd_write_data(0x44);
		lcd_write_data(0x00);
		lcd_write_data(0x4F);
		lcd_write_data(0x00);
		lcd_write_data(0x61);
		lcd_write_data(0x00);
		lcd_write_data(0x79);
		lcd_write_data(0x00);
		lcd_write_data(0x88);
		lcd_write_data(0x00);
		lcd_write_data(0x97);
		lcd_write_data(0x00);
		lcd_write_data(0xA6);
		lcd_write_data(0x00);
		lcd_write_data(0xB7);
		lcd_write_data(0x00);
		lcd_write_data(0xC2);
		lcd_write_data(0x00);
		lcd_write_data(0xC7);
		lcd_write_data(0x00);
		lcd_write_data(0xD1);
		lcd_write_data(0x00);
		lcd_write_data(0xD6);
		lcd_write_data(0x00);
		lcd_write_data(0xDD);
		lcd_write_data(0x00);
		lcd_write_data(0xF3);
		lcd_write_data(0x00);
		lcd_write_reg(0xE3);
		lcd_write_data(0x05);
		lcd_write_data(0x00);
		lcd_write_data(0xA);
		lcd_write_data(0x00);
		lcd_write_data(0x1C);
		lcd_write_data(0x00);
		lcd_write_data(0x33);
		lcd_write_data(0x00);
		lcd_write_data(0x44);
		lcd_write_data(0x00);
		lcd_write_data(0x50);
		lcd_write_data(0x00);
		lcd_write_data(0x62);
		lcd_write_data(0x00);
		lcd_write_data(0x78);
		lcd_write_data(0x00);
		lcd_write_data(0x88);
		lcd_write_data(0x00);
		lcd_write_data(0x97);
		lcd_write_data(0x00);
		lcd_write_data(0xA6);
		lcd_write_data(0x00);
		lcd_write_data(0xB7);
		lcd_write_data(0x00);
		lcd_write_data(0xC2);
		lcd_write_data(0x00);
		lcd_write_data(0xC7);
		lcd_write_data(0x00);
		lcd_write_data(0xD1);
		lcd_write_data(0x00);
		lcd_write_data(0xD5);
		lcd_write_data(0x00);
		lcd_write_data(0xDD);
		lcd_write_data(0x00);
		lcd_write_data(0xF3);
		lcd_write_data(0x00);

		lcd_write_reg(0xE4);
		lcd_write_data(0x01);
		lcd_write_data(0x00);
		lcd_write_data(0x01);
		lcd_write_data(0x00);
		lcd_write_data(0x02);
		lcd_write_data(0x00);
		lcd_write_data(0x2A);
		lcd_write_data(0x00);
		lcd_write_data(0x3C);
		lcd_write_data(0x00);
		lcd_write_data(0x4B);
		lcd_write_data(0x00);
		lcd_write_data(0x5D);
		lcd_write_data(0x00);
		lcd_write_data(0x74);
		lcd_write_data(0x00);
		lcd_write_data(0x84);
		lcd_write_data(0x00);
		lcd_write_data(0x93);
		lcd_write_data(0x00);
		lcd_write_data(0xA2);
		lcd_write_data(0x00);
		lcd_write_data(0xB3);
		lcd_write_data(0x00);
		lcd_write_data(0xBE);
		lcd_write_data(0x00);
		lcd_write_data(0xC4);
		lcd_write_data(0x00);
		lcd_write_data(0xCD);
		lcd_write_data(0x00);
		lcd_write_data(0xD3);
		lcd_write_data(0x00);
		lcd_write_data(0xDD);
		lcd_write_data(0x00);
		lcd_write_data(0xF3);
		lcd_write_data(0x00);
		lcd_write_reg(0xE5);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x02);
		lcd_write_data(0x00);
		lcd_write_data(0x29);
		lcd_write_data(0x00);
		lcd_write_data(0x3C);
		lcd_write_data(0x00);
		lcd_write_data(0x4B);
		lcd_write_data(0x00);
		lcd_write_data(0x5D);
		lcd_write_data(0x00);
		lcd_write_data(0x74);
		lcd_write_data(0x00);
		lcd_write_data(0x84);
		lcd_write_data(0x00);
		lcd_write_data(0x93);
		lcd_write_data(0x00);
		lcd_write_data(0xA2);
		lcd_write_data(0x00);
		lcd_write_data(0xB3);
		lcd_write_data(0x00);
		lcd_write_data(0xBE);
		lcd_write_data(0x00);
		lcd_write_data(0xC4);
		lcd_write_data(0x00);
		lcd_write_data(0xCD);
		lcd_write_data(0x00);
		lcd_write_data(0xD3);
		lcd_write_data(0x00);
		lcd_write_data(0xDC);
		lcd_write_data(0x00);
		lcd_write_data(0xF3);
		lcd_write_data(0x00);

		lcd_write_reg(0xE6);
		lcd_write_data(0x11);
		lcd_write_data(0x00);
		lcd_write_data(0x34);
		lcd_write_data(0x00);
		lcd_write_data(0x56);
		lcd_write_data(0x00);
		lcd_write_data(0x76);
		lcd_write_data(0x00);
		lcd_write_data(0x77);
		lcd_write_data(0x00);
		lcd_write_data(0x66);
		lcd_write_data(0x00);
		lcd_write_data(0x88);
		lcd_write_data(0x00);
		lcd_write_data(0x99);
		lcd_write_data(0x00);
		lcd_write_data(0xBB);
		lcd_write_data(0x00);
		lcd_write_data(0x99);
		lcd_write_data(0x00);
		lcd_write_data(0x66);
		lcd_write_data(0x00);
		lcd_write_data(0x55);
		lcd_write_data(0x00);
		lcd_write_data(0x55);
		lcd_write_data(0x00);
		lcd_write_data(0x45);
		lcd_write_data(0x00);
		lcd_write_data(0x43);
		lcd_write_data(0x00);
		lcd_write_data(0x44);
		lcd_write_data(0x00);

		lcd_write_reg(0xE7);
		lcd_write_data(0x32);
		lcd_write_data(0x00);
		lcd_write_data(0x55);
		lcd_write_data(0x00);
		lcd_write_data(0x76);
		lcd_write_data(0x00);
		lcd_write_data(0x66);
		lcd_write_data(0x00);
		lcd_write_data(0x67);
		lcd_write_data(0x00);
		lcd_write_data(0x67);
		lcd_write_data(0x00);
		lcd_write_data(0x87);
		lcd_write_data(0x00);
		lcd_write_data(0x99);
		lcd_write_data(0x00);
		lcd_write_data(0xBB);
		lcd_write_data(0x00);
		lcd_write_data(0x99);
		lcd_write_data(0x00);
		lcd_write_data(0x77);
		lcd_write_data(0x00);
		lcd_write_data(0x44);
		lcd_write_data(0x00);
		lcd_write_data(0x56);
		lcd_write_data(0x00);
		lcd_write_data(0x23); 
		lcd_write_data(0x00);
		lcd_write_data(0x33);
		lcd_write_data(0x00);
		lcd_write_data(0x45);
		lcd_write_data(0x00);

		lcd_write_reg(0xE8);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x99);
		lcd_write_data(0x00);
		lcd_write_data(0x87);
		lcd_write_data(0x00);
		lcd_write_data(0x88);
		lcd_write_data(0x00);
		lcd_write_data(0x77);
		lcd_write_data(0x00);
		lcd_write_data(0x66);
		lcd_write_data(0x00);
		lcd_write_data(0x88);
		lcd_write_data(0x00);
		lcd_write_data(0xAA);
		lcd_write_data(0x00);
		lcd_write_data(0xBB);
		lcd_write_data(0x00);
		lcd_write_data(0x99);
		lcd_write_data(0x00);
		lcd_write_data(0x66);
		lcd_write_data(0x00);
		lcd_write_data(0x55);
		lcd_write_data(0x00);
		lcd_write_data(0x55);
		lcd_write_data(0x00);
		lcd_write_data(0x44);
		lcd_write_data(0x00);
		lcd_write_data(0x44);
		lcd_write_data(0x00);
		lcd_write_data(0x55);
		lcd_write_data(0x00);

		lcd_write_reg(0xE9);
		lcd_write_data(0xAA);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);

		lcd_write_reg(0x00);
		lcd_write_data(0xAA);

		lcd_write_reg(0xCF);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);

		lcd_write_reg(0xF0);
		lcd_write_data(0x00);
		lcd_write_data(0x50);
		lcd_write_data(0x00);
		lcd_write_data(0x00);
		lcd_write_data(0x00);

		lcd_write_reg(0xF3);
		lcd_write_data(0x00);

		lcd_write_reg(0xF9);
		lcd_write_data(0x06);
		lcd_write_data(0x10);
		lcd_write_data(0x29);
		lcd_write_data(0x00);

		lcd_write_reg(0x3A);
		lcd_write_data(0x55);	//66

		lcd_write_reg(0x11);
		delay_ms(100);
		lcd_write_reg(0x29);
		lcd_write_reg(0x35);
		lcd_write_data(0x00);

		lcd_write_reg(0x51);
		lcd_write_data(0xFF);
		lcd_write_reg(0x53);
		lcd_write_data(0x2C);
		lcd_write_reg(0x55);
		lcd_write_data(0x82);
		lcd_write_reg(0x2c);
	}else if(lcddev.id==0x5510)
	{
		lcd_write_reg_with_value(0xF000,0x55);
		lcd_write_reg_with_value(0xF001,0xAA);
		lcd_write_reg_with_value(0xF002,0x52);
		lcd_write_reg_with_value(0xF003,0x08);
		lcd_write_reg_with_value(0xF004,0x01);
		//AVDD Set AVDD 5.2V
		lcd_write_reg_with_value(0xB000,0x0D);
		lcd_write_reg_with_value(0xB001,0x0D);
		lcd_write_reg_with_value(0xB002,0x0D);
		//AVDD ratio
		lcd_write_reg_with_value(0xB600,0x34);
		lcd_write_reg_with_value(0xB601,0x34);
		lcd_write_reg_with_value(0xB602,0x34);
		//AVEE -5.2V
		lcd_write_reg_with_value(0xB100,0x0D);
		lcd_write_reg_with_value(0xB101,0x0D);
		lcd_write_reg_with_value(0xB102,0x0D);
		//AVEE ratio
		lcd_write_reg_with_value(0xB700,0x34);
		lcd_write_reg_with_value(0xB701,0x34);
		lcd_write_reg_with_value(0xB702,0x34);
		//VCL -2.5V
		lcd_write_reg_with_value(0xB200,0x00);
		lcd_write_reg_with_value(0xB201,0x00);
		lcd_write_reg_with_value(0xB202,0x00);
		//VCL ratio
		lcd_write_reg_with_value(0xB800,0x24);
		lcd_write_reg_with_value(0xB801,0x24);
		lcd_write_reg_with_value(0xB802,0x24);
		//VGH 15V (Free pump)
		lcd_write_reg_with_value(0xBF00,0x01);
		lcd_write_reg_with_value(0xB300,0x0F);
		lcd_write_reg_with_value(0xB301,0x0F);
		lcd_write_reg_with_value(0xB302,0x0F);
		//VGH ratio
		lcd_write_reg_with_value(0xB900,0x34);
		lcd_write_reg_with_value(0xB901,0x34);
		lcd_write_reg_with_value(0xB902,0x34);
		//VGL_REG -10V
		lcd_write_reg_with_value(0xB500,0x08);
		lcd_write_reg_with_value(0xB501,0x08);
		lcd_write_reg_with_value(0xB502,0x08);
		lcd_write_reg_with_value(0xC200,0x03);
		//VGLX ratio
		lcd_write_reg_with_value(0xBA00,0x24);
		lcd_write_reg_with_value(0xBA01,0x24);
		lcd_write_reg_with_value(0xBA02,0x24);
		//VGMP/VGSP 4.5V/0V
		lcd_write_reg_with_value(0xBC00,0x00);
		lcd_write_reg_with_value(0xBC01,0x78);
		lcd_write_reg_with_value(0xBC02,0x00);
		//VGMN/VGSN -4.5V/0V
		lcd_write_reg_with_value(0xBD00,0x00);
		lcd_write_reg_with_value(0xBD01,0x78);
		lcd_write_reg_with_value(0xBD02,0x00);
		//VCOM
		lcd_write_reg_with_value(0xBE00,0x00);
		lcd_write_reg_with_value(0xBE01,0x64);
		//Gamma Setting
		lcd_write_reg_with_value(0xD100,0x00);
		lcd_write_reg_with_value(0xD101,0x33);
		lcd_write_reg_with_value(0xD102,0x00);
		lcd_write_reg_with_value(0xD103,0x34);
		lcd_write_reg_with_value(0xD104,0x00);
		lcd_write_reg_with_value(0xD105,0x3A);
		lcd_write_reg_with_value(0xD106,0x00);
		lcd_write_reg_with_value(0xD107,0x4A);
		lcd_write_reg_with_value(0xD108,0x00);
		lcd_write_reg_with_value(0xD109,0x5C);
		lcd_write_reg_with_value(0xD10A,0x00);
		lcd_write_reg_with_value(0xD10B,0x81);
		lcd_write_reg_with_value(0xD10C,0x00);
		lcd_write_reg_with_value(0xD10D,0xA6);
		lcd_write_reg_with_value(0xD10E,0x00);
		lcd_write_reg_with_value(0xD10F,0xE5);
		lcd_write_reg_with_value(0xD110,0x01);
		lcd_write_reg_with_value(0xD111,0x13);
		lcd_write_reg_with_value(0xD112,0x01);
		lcd_write_reg_with_value(0xD113,0x54);
		lcd_write_reg_with_value(0xD114,0x01);
		lcd_write_reg_with_value(0xD115,0x82);
		lcd_write_reg_with_value(0xD116,0x01);
		lcd_write_reg_with_value(0xD117,0xCA);
		lcd_write_reg_with_value(0xD118,0x02);
		lcd_write_reg_with_value(0xD119,0x00);
		lcd_write_reg_with_value(0xD11A,0x02);
		lcd_write_reg_with_value(0xD11B,0x01);
		lcd_write_reg_with_value(0xD11C,0x02);
		lcd_write_reg_with_value(0xD11D,0x34);
		lcd_write_reg_with_value(0xD11E,0x02);
		lcd_write_reg_with_value(0xD11F,0x67);
		lcd_write_reg_with_value(0xD120,0x02);
		lcd_write_reg_with_value(0xD121,0x84);
		lcd_write_reg_with_value(0xD122,0x02);
		lcd_write_reg_with_value(0xD123,0xA4);
		lcd_write_reg_with_value(0xD124,0x02);
		lcd_write_reg_with_value(0xD125,0xB7);
		lcd_write_reg_with_value(0xD126,0x02);
		lcd_write_reg_with_value(0xD127,0xCF);
		lcd_write_reg_with_value(0xD128,0x02);
		lcd_write_reg_with_value(0xD129,0xDE);
		lcd_write_reg_with_value(0xD12A,0x02);
		lcd_write_reg_with_value(0xD12B,0xF2);
		lcd_write_reg_with_value(0xD12C,0x02);
		lcd_write_reg_with_value(0xD12D,0xFE);
		lcd_write_reg_with_value(0xD12E,0x03);
		lcd_write_reg_with_value(0xD12F,0x10);
		lcd_write_reg_with_value(0xD130,0x03);
		lcd_write_reg_with_value(0xD131,0x33);
		lcd_write_reg_with_value(0xD132,0x03);
		lcd_write_reg_with_value(0xD133,0x6D);
		lcd_write_reg_with_value(0xD200,0x00);
		lcd_write_reg_with_value(0xD201,0x33);
		lcd_write_reg_with_value(0xD202,0x00);
		lcd_write_reg_with_value(0xD203,0x34);
		lcd_write_reg_with_value(0xD204,0x00);
		lcd_write_reg_with_value(0xD205,0x3A);
		lcd_write_reg_with_value(0xD206,0x00);
		lcd_write_reg_with_value(0xD207,0x4A);
		lcd_write_reg_with_value(0xD208,0x00);
		lcd_write_reg_with_value(0xD209,0x5C);
		lcd_write_reg_with_value(0xD20A,0x00);

		lcd_write_reg_with_value(0xD20B,0x81);
		lcd_write_reg_with_value(0xD20C,0x00);
		lcd_write_reg_with_value(0xD20D,0xA6);
		lcd_write_reg_with_value(0xD20E,0x00);
		lcd_write_reg_with_value(0xD20F,0xE5);
		lcd_write_reg_with_value(0xD210,0x01);
		lcd_write_reg_with_value(0xD211,0x13);
		lcd_write_reg_with_value(0xD212,0x01);
		lcd_write_reg_with_value(0xD213,0x54);
		lcd_write_reg_with_value(0xD214,0x01);
		lcd_write_reg_with_value(0xD215,0x82);
		lcd_write_reg_with_value(0xD216,0x01);
		lcd_write_reg_with_value(0xD217,0xCA);
		lcd_write_reg_with_value(0xD218,0x02);
		lcd_write_reg_with_value(0xD219,0x00);
		lcd_write_reg_with_value(0xD21A,0x02);
		lcd_write_reg_with_value(0xD21B,0x01);
		lcd_write_reg_with_value(0xD21C,0x02);
		lcd_write_reg_with_value(0xD21D,0x34);
		lcd_write_reg_with_value(0xD21E,0x02);
		lcd_write_reg_with_value(0xD21F,0x67);
		lcd_write_reg_with_value(0xD220,0x02);
		lcd_write_reg_with_value(0xD221,0x84);
		lcd_write_reg_with_value(0xD222,0x02);
		lcd_write_reg_with_value(0xD223,0xA4);
		lcd_write_reg_with_value(0xD224,0x02);
		lcd_write_reg_with_value(0xD225,0xB7);
		lcd_write_reg_with_value(0xD226,0x02);
		lcd_write_reg_with_value(0xD227,0xCF);
		lcd_write_reg_with_value(0xD228,0x02);
		lcd_write_reg_with_value(0xD229,0xDE);
		lcd_write_reg_with_value(0xD22A,0x02);
		lcd_write_reg_with_value(0xD22B,0xF2);
		lcd_write_reg_with_value(0xD22C,0x02);
		lcd_write_reg_with_value(0xD22D,0xFE);
		lcd_write_reg_with_value(0xD22E,0x03);
		lcd_write_reg_with_value(0xD22F,0x10);
		lcd_write_reg_with_value(0xD230,0x03);
		lcd_write_reg_with_value(0xD231,0x33);
		lcd_write_reg_with_value(0xD232,0x03);
		lcd_write_reg_with_value(0xD233,0x6D);
		lcd_write_reg_with_value(0xD300,0x00);
		lcd_write_reg_with_value(0xD301,0x33);
		lcd_write_reg_with_value(0xD302,0x00);
		lcd_write_reg_with_value(0xD303,0x34);
		lcd_write_reg_with_value(0xD304,0x00);
		lcd_write_reg_with_value(0xD305,0x3A);
		lcd_write_reg_with_value(0xD306,0x00);
		lcd_write_reg_with_value(0xD307,0x4A);
		lcd_write_reg_with_value(0xD308,0x00);
		lcd_write_reg_with_value(0xD309,0x5C);
		lcd_write_reg_with_value(0xD30A,0x00);

		lcd_write_reg_with_value(0xD30B,0x81);
		lcd_write_reg_with_value(0xD30C,0x00);
		lcd_write_reg_with_value(0xD30D,0xA6);
		lcd_write_reg_with_value(0xD30E,0x00);
		lcd_write_reg_with_value(0xD30F,0xE5);
		lcd_write_reg_with_value(0xD310,0x01);
		lcd_write_reg_with_value(0xD311,0x13);
		lcd_write_reg_with_value(0xD312,0x01);
		lcd_write_reg_with_value(0xD313,0x54);
		lcd_write_reg_with_value(0xD314,0x01);
		lcd_write_reg_with_value(0xD315,0x82);
		lcd_write_reg_with_value(0xD316,0x01);
		lcd_write_reg_with_value(0xD317,0xCA);
		lcd_write_reg_with_value(0xD318,0x02);
		lcd_write_reg_with_value(0xD319,0x00);
		lcd_write_reg_with_value(0xD31A,0x02);
		lcd_write_reg_with_value(0xD31B,0x01);
		lcd_write_reg_with_value(0xD31C,0x02);
		lcd_write_reg_with_value(0xD31D,0x34);
		lcd_write_reg_with_value(0xD31E,0x02);
		lcd_write_reg_with_value(0xD31F,0x67);
		lcd_write_reg_with_value(0xD320,0x02);
		lcd_write_reg_with_value(0xD321,0x84);
		lcd_write_reg_with_value(0xD322,0x02);
		lcd_write_reg_with_value(0xD323,0xA4);
		lcd_write_reg_with_value(0xD324,0x02);
		lcd_write_reg_with_value(0xD325,0xB7);
		lcd_write_reg_with_value(0xD326,0x02);
		lcd_write_reg_with_value(0xD327,0xCF);
		lcd_write_reg_with_value(0xD328,0x02);
		lcd_write_reg_with_value(0xD329,0xDE);
		lcd_write_reg_with_value(0xD32A,0x02);
		lcd_write_reg_with_value(0xD32B,0xF2);
		lcd_write_reg_with_value(0xD32C,0x02);
		lcd_write_reg_with_value(0xD32D,0xFE);
		lcd_write_reg_with_value(0xD32E,0x03);
		lcd_write_reg_with_value(0xD32F,0x10);
		lcd_write_reg_with_value(0xD330,0x03);
		lcd_write_reg_with_value(0xD331,0x33);
		lcd_write_reg_with_value(0xD332,0x03);
		lcd_write_reg_with_value(0xD333,0x6D);
		lcd_write_reg_with_value(0xD400,0x00);
		lcd_write_reg_with_value(0xD401,0x33);
		lcd_write_reg_with_value(0xD402,0x00);
		lcd_write_reg_with_value(0xD403,0x34);
		lcd_write_reg_with_value(0xD404,0x00);
		lcd_write_reg_with_value(0xD405,0x3A);
		lcd_write_reg_with_value(0xD406,0x00);
		lcd_write_reg_with_value(0xD407,0x4A);
		lcd_write_reg_with_value(0xD408,0x00);
		lcd_write_reg_with_value(0xD409,0x5C);
		lcd_write_reg_with_value(0xD40A,0x00);
		lcd_write_reg_with_value(0xD40B,0x81);

		lcd_write_reg_with_value(0xD40C,0x00);
		lcd_write_reg_with_value(0xD40D,0xA6);
		lcd_write_reg_with_value(0xD40E,0x00);
		lcd_write_reg_with_value(0xD40F,0xE5);
		lcd_write_reg_with_value(0xD410,0x01);
		lcd_write_reg_with_value(0xD411,0x13);
		lcd_write_reg_with_value(0xD412,0x01);
		lcd_write_reg_with_value(0xD413,0x54);
		lcd_write_reg_with_value(0xD414,0x01);
		lcd_write_reg_with_value(0xD415,0x82);
		lcd_write_reg_with_value(0xD416,0x01);
		lcd_write_reg_with_value(0xD417,0xCA);
		lcd_write_reg_with_value(0xD418,0x02);
		lcd_write_reg_with_value(0xD419,0x00);
		lcd_write_reg_with_value(0xD41A,0x02);
		lcd_write_reg_with_value(0xD41B,0x01);
		lcd_write_reg_with_value(0xD41C,0x02);
		lcd_write_reg_with_value(0xD41D,0x34);
		lcd_write_reg_with_value(0xD41E,0x02);
		lcd_write_reg_with_value(0xD41F,0x67);
		lcd_write_reg_with_value(0xD420,0x02);
		lcd_write_reg_with_value(0xD421,0x84);
		lcd_write_reg_with_value(0xD422,0x02);
		lcd_write_reg_with_value(0xD423,0xA4);
		lcd_write_reg_with_value(0xD424,0x02);
		lcd_write_reg_with_value(0xD425,0xB7);
		lcd_write_reg_with_value(0xD426,0x02);
		lcd_write_reg_with_value(0xD427,0xCF);
		lcd_write_reg_with_value(0xD428,0x02);
		lcd_write_reg_with_value(0xD429,0xDE);
		lcd_write_reg_with_value(0xD42A,0x02);
		lcd_write_reg_with_value(0xD42B,0xF2);
		lcd_write_reg_with_value(0xD42C,0x02);
		lcd_write_reg_with_value(0xD42D,0xFE);
		lcd_write_reg_with_value(0xD42E,0x03);
		lcd_write_reg_with_value(0xD42F,0x10);
		lcd_write_reg_with_value(0xD430,0x03);
		lcd_write_reg_with_value(0xD431,0x33);
		lcd_write_reg_with_value(0xD432,0x03);
		lcd_write_reg_with_value(0xD433,0x6D);
		lcd_write_reg_with_value(0xD500,0x00);
		lcd_write_reg_with_value(0xD501,0x33);
		lcd_write_reg_with_value(0xD502,0x00);
		lcd_write_reg_with_value(0xD503,0x34);
		lcd_write_reg_with_value(0xD504,0x00);
		lcd_write_reg_with_value(0xD505,0x3A);
		lcd_write_reg_with_value(0xD506,0x00);
		lcd_write_reg_with_value(0xD507,0x4A);
		lcd_write_reg_with_value(0xD508,0x00);
		lcd_write_reg_with_value(0xD509,0x5C);
		lcd_write_reg_with_value(0xD50A,0x00);
		lcd_write_reg_with_value(0xD50B,0x81);

		lcd_write_reg_with_value(0xD50C,0x00);
		lcd_write_reg_with_value(0xD50D,0xA6);
		lcd_write_reg_with_value(0xD50E,0x00);
		lcd_write_reg_with_value(0xD50F,0xE5);
		lcd_write_reg_with_value(0xD510,0x01);
		lcd_write_reg_with_value(0xD511,0x13);
		lcd_write_reg_with_value(0xD512,0x01);
		lcd_write_reg_with_value(0xD513,0x54);
		lcd_write_reg_with_value(0xD514,0x01);
		lcd_write_reg_with_value(0xD515,0x82);
		lcd_write_reg_with_value(0xD516,0x01);
		lcd_write_reg_with_value(0xD517,0xCA);
		lcd_write_reg_with_value(0xD518,0x02);
		lcd_write_reg_with_value(0xD519,0x00);
		lcd_write_reg_with_value(0xD51A,0x02);
		lcd_write_reg_with_value(0xD51B,0x01);
		lcd_write_reg_with_value(0xD51C,0x02);
		lcd_write_reg_with_value(0xD51D,0x34);
		lcd_write_reg_with_value(0xD51E,0x02);
		lcd_write_reg_with_value(0xD51F,0x67);
		lcd_write_reg_with_value(0xD520,0x02);
		lcd_write_reg_with_value(0xD521,0x84);
		lcd_write_reg_with_value(0xD522,0x02);
		lcd_write_reg_with_value(0xD523,0xA4);
		lcd_write_reg_with_value(0xD524,0x02);
		lcd_write_reg_with_value(0xD525,0xB7);
		lcd_write_reg_with_value(0xD526,0x02);
		lcd_write_reg_with_value(0xD527,0xCF);
		lcd_write_reg_with_value(0xD528,0x02);
		lcd_write_reg_with_value(0xD529,0xDE);
		lcd_write_reg_with_value(0xD52A,0x02);
		lcd_write_reg_with_value(0xD52B,0xF2);
		lcd_write_reg_with_value(0xD52C,0x02);
		lcd_write_reg_with_value(0xD52D,0xFE);
		lcd_write_reg_with_value(0xD52E,0x03);
		lcd_write_reg_with_value(0xD52F,0x10);
		lcd_write_reg_with_value(0xD530,0x03);
		lcd_write_reg_with_value(0xD531,0x33);
		lcd_write_reg_with_value(0xD532,0x03);
		lcd_write_reg_with_value(0xD533,0x6D);
		lcd_write_reg_with_value(0xD600,0x00);
		lcd_write_reg_with_value(0xD601,0x33);
		lcd_write_reg_with_value(0xD602,0x00);
		lcd_write_reg_with_value(0xD603,0x34);
		lcd_write_reg_with_value(0xD604,0x00);
		lcd_write_reg_with_value(0xD605,0x3A);
		lcd_write_reg_with_value(0xD606,0x00);
		lcd_write_reg_with_value(0xD607,0x4A);
		lcd_write_reg_with_value(0xD608,0x00);
		lcd_write_reg_with_value(0xD609,0x5C);
		lcd_write_reg_with_value(0xD60A,0x00);
		lcd_write_reg_with_value(0xD60B,0x81);

		lcd_write_reg_with_value(0xD60C,0x00);
		lcd_write_reg_with_value(0xD60D,0xA6);
		lcd_write_reg_with_value(0xD60E,0x00);
		lcd_write_reg_with_value(0xD60F,0xE5);
		lcd_write_reg_with_value(0xD610,0x01);
		lcd_write_reg_with_value(0xD611,0x13);
		lcd_write_reg_with_value(0xD612,0x01);
		lcd_write_reg_with_value(0xD613,0x54);
		lcd_write_reg_with_value(0xD614,0x01);
		lcd_write_reg_with_value(0xD615,0x82);
		lcd_write_reg_with_value(0xD616,0x01);
		lcd_write_reg_with_value(0xD617,0xCA);
		lcd_write_reg_with_value(0xD618,0x02);
		lcd_write_reg_with_value(0xD619,0x00);
		lcd_write_reg_with_value(0xD61A,0x02);
		lcd_write_reg_with_value(0xD61B,0x01);
		lcd_write_reg_with_value(0xD61C,0x02);
		lcd_write_reg_with_value(0xD61D,0x34);
		lcd_write_reg_with_value(0xD61E,0x02);
		lcd_write_reg_with_value(0xD61F,0x67);
		lcd_write_reg_with_value(0xD620,0x02);
		lcd_write_reg_with_value(0xD621,0x84);
		lcd_write_reg_with_value(0xD622,0x02);
		lcd_write_reg_with_value(0xD623,0xA4);
		lcd_write_reg_with_value(0xD624,0x02);
		lcd_write_reg_with_value(0xD625,0xB7);
		lcd_write_reg_with_value(0xD626,0x02);
		lcd_write_reg_with_value(0xD627,0xCF);
		lcd_write_reg_with_value(0xD628,0x02);
		lcd_write_reg_with_value(0xD629,0xDE);
		lcd_write_reg_with_value(0xD62A,0x02);
		lcd_write_reg_with_value(0xD62B,0xF2);
		lcd_write_reg_with_value(0xD62C,0x02);
		lcd_write_reg_with_value(0xD62D,0xFE);
		lcd_write_reg_with_value(0xD62E,0x03);
		lcd_write_reg_with_value(0xD62F,0x10);
		lcd_write_reg_with_value(0xD630,0x03);
		lcd_write_reg_with_value(0xD631,0x33);
		lcd_write_reg_with_value(0xD632,0x03);
		lcd_write_reg_with_value(0xD633,0x6D);
		//LV2 Page 0 enable
		lcd_write_reg_with_value(0xF000,0x55);
		lcd_write_reg_with_value(0xF001,0xAA);
		lcd_write_reg_with_value(0xF002,0x52);
		lcd_write_reg_with_value(0xF003,0x08);
		lcd_write_reg_with_value(0xF004,0x00);
		//Display control
		lcd_write_reg_with_value(0xB100, 0xCC);
		lcd_write_reg_with_value(0xB101, 0x00);
		//Source hold time
		lcd_write_reg_with_value(0xB600,0x05);
		//Gate EQ control
		lcd_write_reg_with_value(0xB700,0x70);
		lcd_write_reg_with_value(0xB701,0x70);
		//Source EQ control (Mode 2)
		lcd_write_reg_with_value(0xB800,0x01);
		lcd_write_reg_with_value(0xB801,0x03);
		lcd_write_reg_with_value(0xB802,0x03);
		lcd_write_reg_with_value(0xB803,0x03);
		//Inversion mode (2-dot)
		lcd_write_reg_with_value(0xBC00,0x02);
		lcd_write_reg_with_value(0xBC01,0x00);
		lcd_write_reg_with_value(0xBC02,0x00);
		//Timing control 4H w/ 4-delay
		lcd_write_reg_with_value(0xC900,0xD0);
		lcd_write_reg_with_value(0xC901,0x02);
		lcd_write_reg_with_value(0xC902,0x50);
		lcd_write_reg_with_value(0xC903,0x50);
		lcd_write_reg_with_value(0xC904,0x50);
		lcd_write_reg_with_value(0x3500,0x00);
		lcd_write_reg_with_value(0x3A00,0x55);  //16-bit/pixel
		lcd_write_reg(0x1100);
		delay_us(120);
		lcd_write_reg(0x2900);
	}else if(lcddev.id==0x9325)//9325
	{
		lcd_write_reg_with_value(0x00E5,0x78F0); 
		lcd_write_reg_with_value(0x0001,0x0100); 
		lcd_write_reg_with_value(0x0002,0x0700); 
		lcd_write_reg_with_value(0x0003,0x1030); 
		lcd_write_reg_with_value(0x0004,0x0000); 
		lcd_write_reg_with_value(0x0008,0x0202);  
		lcd_write_reg_with_value(0x0009,0x0000);
		lcd_write_reg_with_value(0x000A,0x0000); 
		lcd_write_reg_with_value(0x000C,0x0000); 
		lcd_write_reg_with_value(0x000D,0x0000);
		lcd_write_reg_with_value(0x000F,0x0000);
		//power on sequence VGHVGL
		lcd_write_reg_with_value(0x0010,0x0000);   
		lcd_write_reg_with_value(0x0011,0x0007);  
		lcd_write_reg_with_value(0x0012,0x0000);  
		lcd_write_reg_with_value(0x0013,0x0000); 
		lcd_write_reg_with_value(0x0007,0x0000); 
		//vgh 
		lcd_write_reg_with_value(0x0010,0x1690);   
		lcd_write_reg_with_value(0x0011,0x0227);
		//delayms(100);
		//vregiout 
		lcd_write_reg_with_value(0x0012,0x009D); //0x001b
		//delayms(100); 
		//vom amplitude
		lcd_write_reg_with_value(0x0013,0x1900);
		//delayms(100); 
		//vom H
		lcd_write_reg_with_value(0x0029,0x0025); 
		lcd_write_reg_with_value(0x002B,0x000D); 
		//gamma
		lcd_write_reg_with_value(0x0030,0x0007);
		lcd_write_reg_with_value(0x0031,0x0303);
		lcd_write_reg_with_value(0x0032,0x0003);// 0006
		lcd_write_reg_with_value(0x0035,0x0206);
		lcd_write_reg_with_value(0x0036,0x0008);
		lcd_write_reg_with_value(0x0037,0x0406); 
		lcd_write_reg_with_value(0x0038,0x0304);//0200
		lcd_write_reg_with_value(0x0039,0x0007); 
		lcd_write_reg_with_value(0x003C,0x0602);// 0504
		lcd_write_reg_with_value(0x003D,0x0008); 
		//ram
		lcd_write_reg_with_value(0x0050,0x0000); 
		lcd_write_reg_with_value(0x0051,0x00EF);
		lcd_write_reg_with_value(0x0052,0x0000); 
		lcd_write_reg_with_value(0x0053,0x013F);  
		lcd_write_reg_with_value(0x0060,0xA700); 
		lcd_write_reg_with_value(0x0061,0x0001); 
		lcd_write_reg_with_value(0x006A,0x0000); 
		//
		lcd_write_reg_with_value(0x0080,0x0000); 
		lcd_write_reg_with_value(0x0081,0x0000); 
		lcd_write_reg_with_value(0x0082,0x0000); 
		lcd_write_reg_with_value(0x0083,0x0000); 
		lcd_write_reg_with_value(0x0084,0x0000); 
		lcd_write_reg_with_value(0x0085,0x0000); 
		//
		lcd_write_reg_with_value(0x0090,0x0010); 
		lcd_write_reg_with_value(0x0092,0x0600); 
		
		lcd_write_reg_with_value(0x0007,0x0133);
		lcd_write_reg_with_value(0x00,0x0022);//
	}else if(lcddev.id==0x9328)//ILI9328   OK  
	{
  		lcd_write_reg_with_value(0x00EC,0x108F);// internal timeing      
 		lcd_write_reg_with_value(0x00EF,0x1234);// ADD        
		//lcd_write_reg_with_value(0x00e7,0x0010);      
        //lcd_write_reg_with_value(0x0000,0x0001);//开启内部时钟
        lcd_write_reg_with_value(0x0001,0x0100);     
        lcd_write_reg_with_value(0x0002,0x0700);//电源开启                    
		//lcd_write_reg_with_value(0x0003,(1<<3)|(1<<4) ); 	//65K  RGB
		//DRIVE TABLE(寄存器 03H)
		//BIT3=AM BIT4:5=ID0:1
		//AM ID0 ID1   FUNCATION
		// 0  0   0	   R->L D->U
		// 1  0   0	   D->U	R->L
		// 0  1   0	   L->R D->U
		// 1  1   0    D->U	L->R
		// 0  0   1	   R->L U->D
		// 1  0   1    U->D	R->L
		// 0  1   1    L->R U->D 正常就用这个.
		// 1  1   1	   U->D	L->R
        lcd_write_reg_with_value(0x0003,(1<<12)|(3<<4)|(0<<3) );//65K    
        lcd_write_reg_with_value(0x0004,0x0000);                                   
        lcd_write_reg_with_value(0x0008,0x0202);	           
        lcd_write_reg_with_value(0x0009,0x0000);         
        lcd_write_reg_with_value(0x000a,0x0000);//display setting         
        lcd_write_reg_with_value(0x000c,0x0001);//display setting          
        lcd_write_reg_with_value(0x000d,0x0000);//0f3c          
        lcd_write_reg_with_value(0x000f,0x0000);
		//电源配置
        lcd_write_reg_with_value(0x0010,0x0000);   
        lcd_write_reg_with_value(0x0011,0x0007);
        lcd_write_reg_with_value(0x0012,0x0000);                                                                 
        lcd_write_reg_with_value(0x0013,0x0000);                 
     	lcd_write_reg_with_value(0x0007,0x0001);                 
       	delay_ms(50); 
        lcd_write_reg_with_value(0x0010,0x1490);   
        lcd_write_reg_with_value(0x0011,0x0227);
        delay_ms(50); 
        lcd_write_reg_with_value(0x0012,0x008A);                  
        delay_ms(50); 
        lcd_write_reg_with_value(0x0013,0x1a00);   
        lcd_write_reg_with_value(0x0029,0x0006);
        lcd_write_reg_with_value(0x002b,0x000d);
        delay_ms(50); 
        lcd_write_reg_with_value(0x0020,0x0000);                                                            
        lcd_write_reg_with_value(0x0021,0x0000);           
		delay_ms(50); 
		//伽马校正
        lcd_write_reg_with_value(0x0030,0x0000); 
        lcd_write_reg_with_value(0x0031,0x0604);   
        lcd_write_reg_with_value(0x0032,0x0305);
        lcd_write_reg_with_value(0x0035,0x0000);
        lcd_write_reg_with_value(0x0036,0x0C09); 
        lcd_write_reg_with_value(0x0037,0x0204);
        lcd_write_reg_with_value(0x0038,0x0301);        
        lcd_write_reg_with_value(0x0039,0x0707);     
        lcd_write_reg_with_value(0x003c,0x0000);
        lcd_write_reg_with_value(0x003d,0x0a0a);
        delay_ms(50); 
        lcd_write_reg_with_value(0x0050,0x0000); //水平GRAM起始位置 
        lcd_write_reg_with_value(0x0051,0x00ef); //水平GRAM终止位置                    
        lcd_write_reg_with_value(0x0052,0x0000); //垂直GRAM起始位置                    
        lcd_write_reg_with_value(0x0053,0x013f); //垂直GRAM终止位置  
 
         lcd_write_reg_with_value(0x0060,0xa700);        
        lcd_write_reg_with_value(0x0061,0x0001); 
        lcd_write_reg_with_value(0x006a,0x0000);
        lcd_write_reg_with_value(0x0080,0x0000);
        lcd_write_reg_with_value(0x0081,0x0000);
        lcd_write_reg_with_value(0x0082,0x0000);
        lcd_write_reg_with_value(0x0083,0x0000);
        lcd_write_reg_with_value(0x0084,0x0000);
        lcd_write_reg_with_value(0x0085,0x0000);
      
        lcd_write_reg_with_value(0x0090,0x0010);     
        lcd_write_reg_with_value(0x0092,0x0600);  
        //开启显示设置    
        lcd_write_reg_with_value(0x0007,0x0133); 
	}else if(lcddev.id==0x9320)//测试OK.
	{
		lcd_write_reg_with_value(0x00,0x0000);
		lcd_write_reg_with_value(0x01,0x0100);	//Driver Output Contral.
		lcd_write_reg_with_value(0x02,0x0700);	//LCD Driver Waveform Contral.
		lcd_write_reg_with_value(0x03,0x1030);//Entry Mode Set.
		//lcd_write_reg_with_value(0x03,0x1018);	//Entry Mode Set.
	
		lcd_write_reg_with_value(0x04,0x0000);	//Scalling Contral.
		lcd_write_reg_with_value(0x08,0x0202);	//Display Contral 2.(0x0207)
		lcd_write_reg_with_value(0x09,0x0000);	//Display Contral 3.(0x0000)
		lcd_write_reg_with_value(0x0a,0x0000);	//Frame Cycle Contal.(0x0000)
		lcd_write_reg_with_value(0x0c,(1<<0));	//Extern Display Interface Contral 1.(0x0000)
		lcd_write_reg_with_value(0x0d,0x0000);	//Frame Maker Position.
		lcd_write_reg_with_value(0x0f,0x0000);	//Extern Display Interface Contral 2.	    
		delay_ms(50); 
		lcd_write_reg_with_value(0x07,0x0101);	//Display Contral.
		delay_ms(50); 								  
		lcd_write_reg_with_value(0x10,(1<<12)|(0<<8)|(1<<7)|(1<<6)|(0<<4));	//Power Control 1.(0x16b0)
		lcd_write_reg_with_value(0x11,0x0007);								//Power Control 2.(0x0001)
		lcd_write_reg_with_value(0x12,(1<<8)|(1<<4)|(0<<0));				//Power Control 3.(0x0138)
		lcd_write_reg_with_value(0x13,0x0b00);								//Power Control 4.
		lcd_write_reg_with_value(0x29,0x0000);								//Power Control 7.
	
		lcd_write_reg_with_value(0x2b,(1<<14)|(1<<4));	    
		lcd_write_reg_with_value(0x50,0);	//Set X Star
		//水平GRAM终止位置Set X End.
		lcd_write_reg_with_value(0x51,239);	//Set Y Star
		lcd_write_reg_with_value(0x52,0);	//Set Y End.t.
		lcd_write_reg_with_value(0x53,319);	//
	
		lcd_write_reg_with_value(0x60,0x2700);	//Driver Output Control.
		lcd_write_reg_with_value(0x61,0x0001);	//Driver Output Control.
		lcd_write_reg_with_value(0x6a,0x0000);	//Vertical Srcoll Control.
	
		lcd_write_reg_with_value(0x80,0x0000);	//Display Position? Partial Display 1.
		lcd_write_reg_with_value(0x81,0x0000);	//RAM Address Start? Partial Display 1.
		lcd_write_reg_with_value(0x82,0x0000);	//RAM Address End-Partial Display 1.
		lcd_write_reg_with_value(0x83,0x0000);	//Displsy Position? Partial Display 2.
		lcd_write_reg_with_value(0x84,0x0000);	//RAM Address Start? Partial Display 2.
		lcd_write_reg_with_value(0x85,0x0000);	//RAM Address End? Partial Display 2.
	
		lcd_write_reg_with_value(0x90,(0<<7)|(16<<0));	//Frame Cycle Contral.(0x0013)
		lcd_write_reg_with_value(0x92,0x0000);	//Panel Interface Contral 2.(0x0000)
		lcd_write_reg_with_value(0x93,0x0001);	//Panel Interface Contral 3.
		lcd_write_reg_with_value(0x95,0x0110);	//Frame Cycle Contral.(0x0110)
		lcd_write_reg_with_value(0x97,(0<<8));	//
		lcd_write_reg_with_value(0x98,0x0000);	//Frame Cycle Contral.	   
		lcd_write_reg_with_value(0x07,0x0173);	//(0x0173)
	}else if(lcddev.id==0X9331)//OK |/|/|			 
	{
		lcd_write_reg_with_value(0x00E7, 0x1014);
		lcd_write_reg_with_value(0x0001, 0x0100); // set SS and SM bit
		lcd_write_reg_with_value(0x0002, 0x0200); // set 1 line inversion
        lcd_write_reg_with_value(0x0003,(1<<12)|(3<<4)|(1<<3));//65K    
		//lcd_write_reg_with_value(0x0003, 0x1030); // set GRAM write direction and BGR=1.
		lcd_write_reg_with_value(0x0008, 0x0202); // set the back porch and front porch
		lcd_write_reg_with_value(0x0009, 0x0000); // set non-display area refresh cycle ISC[3:0]
		lcd_write_reg_with_value(0x000A, 0x0000); // FMARK function
		lcd_write_reg_with_value(0x000C, 0x0000); // RGB interface setting
		lcd_write_reg_with_value(0x000D, 0x0000); // Frame marker Position
		lcd_write_reg_with_value(0x000F, 0x0000); // RGB interface polarity
		//*************Power On sequence ****************//
		lcd_write_reg_with_value(0x0010, 0x0000); // SAP, BT[3:0], AP, DSTB, SLP, STB
		lcd_write_reg_with_value(0x0011, 0x0007); // DC1[2:0], DC0[2:0], VC[2:0]
		lcd_write_reg_with_value(0x0012, 0x0000); // VREG1OUT voltage
		lcd_write_reg_with_value(0x0013, 0x0000); // VDV[4:0] for VCOM amplitude
		delay_ms(200); // Dis-charge capacitor power voltage
		lcd_write_reg_with_value(0x0010, 0x1690); // SAP, BT[3:0], AP, DSTB, SLP, STB
		lcd_write_reg_with_value(0x0011, 0x0227); // DC1[2:0], DC0[2:0], VC[2:0]
		delay_ms(50); // Delay 50ms
		lcd_write_reg_with_value(0x0012, 0x000C); // Internal reference voltage= Vci;
		delay_ms(50); // Delay 50ms
		lcd_write_reg_with_value(0x0013, 0x0800); // Set VDV[4:0] for VCOM amplitude
		lcd_write_reg_with_value(0x0029, 0x0011); // Set VCM[5:0] for VCOMH
		lcd_write_reg_with_value(0x002B, 0x000B); // Set Frame Rate
		delay_ms(50); // Delay 50ms
		lcd_write_reg_with_value(0x0020, 0x0000); // GRAM horizontal Address
		lcd_write_reg_with_value(0x0021, 0x013f); // GRAM Vertical Address
		// ----------- Adjust the Gamma Curve ----------//
		lcd_write_reg_with_value(0x0030, 0x0000);
		lcd_write_reg_with_value(0x0031, 0x0106);
		lcd_write_reg_with_value(0x0032, 0x0000);
		lcd_write_reg_with_value(0x0035, 0x0204);
		lcd_write_reg_with_value(0x0036, 0x160A);
		lcd_write_reg_with_value(0x0037, 0x0707);
		lcd_write_reg_with_value(0x0038, 0x0106);
		lcd_write_reg_with_value(0x0039, 0x0707);
		lcd_write_reg_with_value(0x003C, 0x0402);
		lcd_write_reg_with_value(0x003D, 0x0C0F);
		//------------------ Set GRAM area ---------------//
		lcd_write_reg_with_value(0x0050, 0x0000); // Horizontal GRAM Start Address
		lcd_write_reg_with_value(0x0051, 0x00EF); // Horizontal GRAM End Address
		lcd_write_reg_with_value(0x0052, 0x0000); // Vertical GRAM Start Address
		lcd_write_reg_with_value(0x0053, 0x013F); // Vertical GRAM Start Address
		lcd_write_reg_with_value(0x0060, 0x2700); // Gate Scan Line
		lcd_write_reg_with_value(0x0061, 0x0001); // NDL,VLE, REV 
		lcd_write_reg_with_value(0x006A, 0x0000); // set scrolling line
		//-------------- Partial Display Control ---------//
		lcd_write_reg_with_value(0x0080, 0x0000);
		lcd_write_reg_with_value(0x0081, 0x0000);
		lcd_write_reg_with_value(0x0082, 0x0000);
		lcd_write_reg_with_value(0x0083, 0x0000);
		lcd_write_reg_with_value(0x0084, 0x0000);
		lcd_write_reg_with_value(0x0085, 0x0000);
		//-------------- Panel Control -------------------//
		lcd_write_reg_with_value(0x0090, 0x0010);
		lcd_write_reg_with_value(0x0092, 0x0600);
		lcd_write_reg_with_value(0x0007, 0x0133); // 262K color and display ON
	}else if(lcddev.id==0x5408)
	{
		lcd_write_reg_with_value(0x01,0x0100);								  
		lcd_write_reg_with_value(0x02,0x0700);//LCD Driving Waveform Contral 
		lcd_write_reg_with_value(0x03,0x1030);//Entry Mode设置 	   
		//指针从左至右自上而下的自动增模式
		//Normal Mode(Window Mode disable)
		//RGB格式
		//16位数据2次传输的8总线设置
		lcd_write_reg_with_value(0x04,0x0000); //Scalling Control register     
		lcd_write_reg_with_value(0x08,0x0207); //Display Control 2 
		lcd_write_reg_with_value(0x09,0x0000); //Display Control 3	 
		lcd_write_reg_with_value(0x0A,0x0000); //Frame Cycle Control	 
		lcd_write_reg_with_value(0x0C,0x0000); //External Display Interface Control 1 
		lcd_write_reg_with_value(0x0D,0x0000); //Frame Maker Position		 
		lcd_write_reg_with_value(0x0F,0x0000); //External Display Interface Control 2 
 		delay_ms(20);
		//TFT 液晶彩色图像显示方法14
		lcd_write_reg_with_value(0x10,0x16B0); //0x14B0 //Power Control 1
		lcd_write_reg_with_value(0x11,0x0001); //0x0007 //Power Control 2
		lcd_write_reg_with_value(0x17,0x0001); //0x0000 //Power Control 3
		lcd_write_reg_with_value(0x12,0x0138); //0x013B //Power Control 4
		lcd_write_reg_with_value(0x13,0x0800); //0x0800 //Power Control 5
		lcd_write_reg_with_value(0x29,0x0009); //NVM read data 2
		lcd_write_reg_with_value(0x2a,0x0009); //NVM read data 3
		lcd_write_reg_with_value(0xa4,0x0000);	 
		lcd_write_reg_with_value(0x50,0x0000); //设置操作窗口的X轴开始列
		lcd_write_reg_with_value(0x51,0x00EF); //设置操作窗口的X轴结束列
		lcd_write_reg_with_value(0x52,0x0000); //设置操作窗口的Y轴开始行
		lcd_write_reg_with_value(0x53,0x013F); //设置操作窗口的Y轴结束行
		lcd_write_reg_with_value(0x60,0x2700); //Driver Output Control
		//设置屏幕的点数以及扫描的起始行
		lcd_write_reg_with_value(0x61,0x0001); //Driver Output Control
		lcd_write_reg_with_value(0x6A,0x0000); //Vertical Scroll Control
		lcd_write_reg_with_value(0x80,0x0000); //Display Position – Partial Display 1
		lcd_write_reg_with_value(0x81,0x0000); //RAM Address Start – Partial Display 1
		lcd_write_reg_with_value(0x82,0x0000); //RAM address End - Partial Display 1
		lcd_write_reg_with_value(0x83,0x0000); //Display Position – Partial Display 2
		lcd_write_reg_with_value(0x84,0x0000); //RAM Address Start – Partial Display 2
		lcd_write_reg_with_value(0x85,0x0000); //RAM address End – Partail Display2
		lcd_write_reg_with_value(0x90,0x0013); //Frame Cycle Control
		lcd_write_reg_with_value(0x92,0x0000);  //Panel Interface Control 2
		lcd_write_reg_with_value(0x93,0x0003); //Panel Interface control 3
		lcd_write_reg_with_value(0x95,0x0110);  //Frame Cycle Control
		lcd_write_reg_with_value(0x07,0x0173);		 
		delay_ms(50);
	}	
	else if(lcddev.id==0x1505)//OK
	{
		// second release on 3/5  ,luminance is acceptable,water wave appear during camera preview
        lcd_write_reg_with_value(0x0007,0x0000);
        delay_ms(50); 
        lcd_write_reg_with_value(0x0012,0x011C);//0x011A   why need to set several times?
        lcd_write_reg_with_value(0x00A4,0x0001);//NVM	 
        lcd_write_reg_with_value(0x0008,0x000F);
        lcd_write_reg_with_value(0x000A,0x0008);
        lcd_write_reg_with_value(0x000D,0x0008);	    
  		//伽马校正
        lcd_write_reg_with_value(0x0030,0x0707);
        lcd_write_reg_with_value(0x0031,0x0007); //0x0707
        lcd_write_reg_with_value(0x0032,0x0603); 
        lcd_write_reg_with_value(0x0033,0x0700); 
        lcd_write_reg_with_value(0x0034,0x0202); 
        lcd_write_reg_with_value(0x0035,0x0002); //?0x0606
        lcd_write_reg_with_value(0x0036,0x1F0F);
        lcd_write_reg_with_value(0x0037,0x0707); //0x0f0f  0x0105
        lcd_write_reg_with_value(0x0038,0x0000); 
        lcd_write_reg_with_value(0x0039,0x0000); 
        lcd_write_reg_with_value(0x003A,0x0707); 
        lcd_write_reg_with_value(0x003B,0x0000); //0x0303
        lcd_write_reg_with_value(0x003C,0x0007); //?0x0707
        lcd_write_reg_with_value(0x003D,0x0000); //0x1313//0x1f08
        delay_ms(50); 
        lcd_write_reg_with_value(0x0007,0x0001);
        lcd_write_reg_with_value(0x0017,0x0001);//开启电源
        delay_ms(50); 
  		//电源配置
        lcd_write_reg_with_value(0x0010,0x17A0); 
        lcd_write_reg_with_value(0x0011,0x0217);//reference voltage VC[2:0]   Vciout = 1.00*Vcivl
        lcd_write_reg_with_value(0x0012,0x011E);//0x011c  //Vreg1out = Vcilvl*1.80   is it the same as Vgama1out ?
        lcd_write_reg_with_value(0x0013,0x0F00);//VDV[4:0]-->VCOM Amplitude VcomL = VcomH - Vcom Ampl
        lcd_write_reg_with_value(0x002A,0x0000);  
        lcd_write_reg_with_value(0x0029,0x000A);//0x0001F  Vcomh = VCM1[4:0]*Vreg1out    gate source voltage??
        lcd_write_reg_with_value(0x0012,0x013E);// 0x013C  power supply on
        //Coordinates Control//
        lcd_write_reg_with_value(0x0050,0x0000);//0x0e00
        lcd_write_reg_with_value(0x0051,0x00EF); 
        lcd_write_reg_with_value(0x0052,0x0000); 
        lcd_write_reg_with_value(0x0053,0x013F); 
    	//Pannel Image Control//
        lcd_write_reg_with_value(0x0060,0x2700); 
        lcd_write_reg_with_value(0x0061,0x0001); 
        lcd_write_reg_with_value(0x006A,0x0000); 
        lcd_write_reg_with_value(0x0080,0x0000); 
    	//Partial Image Control//
        lcd_write_reg_with_value(0x0081,0x0000); 
        lcd_write_reg_with_value(0x0082,0x0000); 
        lcd_write_reg_with_value(0x0083,0x0000); 
        lcd_write_reg_with_value(0x0084,0x0000); 
        lcd_write_reg_with_value(0x0085,0x0000); 
  		//Panel Interface Control//
        lcd_write_reg_with_value(0x0090,0x0013);//0x0010 frenqucy
        lcd_write_reg_with_value(0x0092,0x0300); 
        lcd_write_reg_with_value(0x0093,0x0005); 
        lcd_write_reg_with_value(0x0095,0x0000); 
        lcd_write_reg_with_value(0x0097,0x0000); 
        lcd_write_reg_with_value(0x0098,0x0000); 
  
        lcd_write_reg_with_value(0x0001,0x0100); 
        lcd_write_reg_with_value(0x0002,0x0700); 
        lcd_write_reg_with_value(0x0003,0x1038);//扫描方向 上->下  左->右 
        lcd_write_reg_with_value(0x0004,0x0000); 
        lcd_write_reg_with_value(0x000C,0x0000); 
        lcd_write_reg_with_value(0x000F,0x0000); 
        lcd_write_reg_with_value(0x0020,0x0000); 
        lcd_write_reg_with_value(0x0021,0x0000); 
        lcd_write_reg_with_value(0x0007,0x0021); 
        delay_ms(20);
        lcd_write_reg_with_value(0x0007,0x0061); 
        delay_ms(20);
        lcd_write_reg_with_value(0x0007,0x0173); 
        delay_ms(20);
	}else if(lcddev.id==0xB505)
	{
		lcd_write_reg_with_value(0x0000,0x0000);
		lcd_write_reg_with_value(0x0000,0x0000);
		lcd_write_reg_with_value(0x0000,0x0000);
		lcd_write_reg_with_value(0x0000,0x0000);
		
		lcd_write_reg_with_value(0x00a4,0x0001);
		delay_ms(20);		  
		lcd_write_reg_with_value(0x0060,0x2700);
		lcd_write_reg_with_value(0x0008,0x0202);
		
		lcd_write_reg_with_value(0x0030,0x0214);
		lcd_write_reg_with_value(0x0031,0x3715);
		lcd_write_reg_with_value(0x0032,0x0604);
		lcd_write_reg_with_value(0x0033,0x0e16);
		lcd_write_reg_with_value(0x0034,0x2211);
		lcd_write_reg_with_value(0x0035,0x1500);
		lcd_write_reg_with_value(0x0036,0x8507);
		lcd_write_reg_with_value(0x0037,0x1407);
		lcd_write_reg_with_value(0x0038,0x1403);
		lcd_write_reg_with_value(0x0039,0x0020);
		
		lcd_write_reg_with_value(0x0090,0x001a);
		lcd_write_reg_with_value(0x0010,0x0000);
		lcd_write_reg_with_value(0x0011,0x0007);
		lcd_write_reg_with_value(0x0012,0x0000);
		lcd_write_reg_with_value(0x0013,0x0000);
		delay_ms(20);
		
		lcd_write_reg_with_value(0x0010,0x0730);
		lcd_write_reg_with_value(0x0011,0x0137);
		delay_ms(20);
		
		lcd_write_reg_with_value(0x0012,0x01b8);
		delay_ms(20);
		
		lcd_write_reg_with_value(0x0013,0x0f00);
		lcd_write_reg_with_value(0x002a,0x0080);
		lcd_write_reg_with_value(0x0029,0x0048);
		delay_ms(20);
		
		lcd_write_reg_with_value(0x0001,0x0100);
		lcd_write_reg_with_value(0x0002,0x0700);
        lcd_write_reg_with_value(0x0003,0x1038);//扫描方向 上->下  左->右 
		lcd_write_reg_with_value(0x0008,0x0202);
		lcd_write_reg_with_value(0x000a,0x0000);
		lcd_write_reg_with_value(0x000c,0x0000);
		lcd_write_reg_with_value(0x000d,0x0000);
		lcd_write_reg_with_value(0x000e,0x0030);
		lcd_write_reg_with_value(0x0050,0x0000);
		lcd_write_reg_with_value(0x0051,0x00ef);
		lcd_write_reg_with_value(0x0052,0x0000);
		lcd_write_reg_with_value(0x0053,0x013f);
		lcd_write_reg_with_value(0x0060,0x2700);
		lcd_write_reg_with_value(0x0061,0x0001);
		lcd_write_reg_with_value(0x006a,0x0000);
		//lcd_write_reg_with_value(0x0080,0x0000);
		//lcd_write_reg_with_value(0x0081,0x0000);
		lcd_write_reg_with_value(0x0090,0X0011);
		lcd_write_reg_with_value(0x0092,0x0600);
		lcd_write_reg_with_value(0x0093,0x0402);
		lcd_write_reg_with_value(0x0094,0x0002);
		delay_ms(20);
		
		lcd_write_reg_with_value(0x0007,0x0001);
		delay_ms(20);
		lcd_write_reg_with_value(0x0007,0x0061);
		lcd_write_reg_with_value(0x0007,0x0173);
		
		lcd_write_reg_with_value(0x0020,0x0000);
		lcd_write_reg_with_value(0x0021,0x0000);	  
		lcd_write_reg_with_value(0x00,0x22);  
	}else if(lcddev.id==0xC505)
	{
		lcd_write_reg_with_value(0x0000,0x0000);
		lcd_write_reg_with_value(0x0000,0x0000);
		delay_ms(20);		  
		lcd_write_reg_with_value(0x0000,0x0000);
		lcd_write_reg_with_value(0x0000,0x0000);
		lcd_write_reg_with_value(0x0000,0x0000);
		lcd_write_reg_with_value(0x0000,0x0000);
 		lcd_write_reg_with_value(0x00a4,0x0001);
		delay_ms(20);		  
		lcd_write_reg_with_value(0x0060,0x2700);
		lcd_write_reg_with_value(0x0008,0x0806);
		
		lcd_write_reg_with_value(0x0030,0x0703);//gamma setting
		lcd_write_reg_with_value(0x0031,0x0001);
		lcd_write_reg_with_value(0x0032,0x0004);
		lcd_write_reg_with_value(0x0033,0x0102);
		lcd_write_reg_with_value(0x0034,0x0300);
		lcd_write_reg_with_value(0x0035,0x0103);
		lcd_write_reg_with_value(0x0036,0x001F);
		lcd_write_reg_with_value(0x0037,0x0703);
		lcd_write_reg_with_value(0x0038,0x0001);
		lcd_write_reg_with_value(0x0039,0x0004);
		
		
		
		lcd_write_reg_with_value(0x0090, 0x0015);	//80Hz
		lcd_write_reg_with_value(0x0010, 0X0410);	//BT,AP
		lcd_write_reg_with_value(0x0011,0x0247);	//DC1,DC0,VC
		lcd_write_reg_with_value(0x0012, 0x01BC);
		lcd_write_reg_with_value(0x0013, 0x0e00);
		delay_ms(120);
		lcd_write_reg_with_value(0x0001, 0x0100);
		lcd_write_reg_with_value(0x0002, 0x0200);
		lcd_write_reg_with_value(0x0003, 0x1030);
		
		lcd_write_reg_with_value(0x000A, 0x0008);
		lcd_write_reg_with_value(0x000C, 0x0000);
		
		lcd_write_reg_with_value(0x000E, 0x0020);
		lcd_write_reg_with_value(0x000F, 0x0000);
		lcd_write_reg_with_value(0x0020, 0x0000);	//H Start
		lcd_write_reg_with_value(0x0021, 0x0000);	//V Start
		lcd_write_reg_with_value(0x002A,0x003D);	//vcom2
		delay_ms(20);
		lcd_write_reg_with_value(0x0029, 0x002d);
		lcd_write_reg_with_value(0x0050, 0x0000);
		lcd_write_reg_with_value(0x0051, 0xD0EF);
		lcd_write_reg_with_value(0x0052, 0x0000);
		lcd_write_reg_with_value(0x0053, 0x013F);
		lcd_write_reg_with_value(0x0061, 0x0000);
		lcd_write_reg_with_value(0x006A, 0x0000);
		lcd_write_reg_with_value(0x0092,0x0300); 
 
 		lcd_write_reg_with_value(0x0093, 0x0005);
		lcd_write_reg_with_value(0x0007, 0x0100);
	}else if(lcddev.id==0x8989)//OK |/|/|
	{	   
		lcd_write_reg_with_value(0x0000,0x0001);//打开晶振
    	lcd_write_reg_with_value(0x0003,0xA8A4);//0xA8A4
    	lcd_write_reg_with_value(0x000C,0x0000);    
    	lcd_write_reg_with_value(0x000D,0x080C);   
    	lcd_write_reg_with_value(0x000E,0x2B00);    
    	lcd_write_reg_with_value(0x001E,0x00B0);    
    	lcd_write_reg_with_value(0x0001,0x2B3F);//驱动输出控制320*240  0x6B3F
    	lcd_write_reg_with_value(0x0002,0x0600);
    	lcd_write_reg_with_value(0x0010,0x0000);  
    	lcd_write_reg_with_value(0x0011,0x6078); //定义数据格式  16位色 		横屏 0x6058
    	lcd_write_reg_with_value(0x0005,0x0000);  
    	lcd_write_reg_with_value(0x0006,0x0000);  
    	lcd_write_reg_with_value(0x0016,0xEF1C);  
    	lcd_write_reg_with_value(0x0017,0x0003);  
    	lcd_write_reg_with_value(0x0007,0x0233); //0x0233       
    	lcd_write_reg_with_value(0x000B,0x0000);  
    	lcd_write_reg_with_value(0x000F,0x0000); //扫描开始地址
    	lcd_write_reg_with_value(0x0041,0x0000);  
    	lcd_write_reg_with_value(0x0042,0x0000);  
    	lcd_write_reg_with_value(0x0048,0x0000);  
    	lcd_write_reg_with_value(0x0049,0x013F);  
    	lcd_write_reg_with_value(0x004A,0x0000);  
    	lcd_write_reg_with_value(0x004B,0x0000);  
    	lcd_write_reg_with_value(0x0044,0xEF00);  
    	lcd_write_reg_with_value(0x0045,0x0000);  
    	lcd_write_reg_with_value(0x0046,0x013F);  
    	lcd_write_reg_with_value(0x0030,0x0707);  
    	lcd_write_reg_with_value(0x0031,0x0204);  
    	lcd_write_reg_with_value(0x0032,0x0204);  
    	lcd_write_reg_with_value(0x0033,0x0502);  
    	lcd_write_reg_with_value(0x0034,0x0507);  
    	lcd_write_reg_with_value(0x0035,0x0204);  
    	lcd_write_reg_with_value(0x0036,0x0204);  
    	lcd_write_reg_with_value(0x0037,0x0502);  
    	lcd_write_reg_with_value(0x003A,0x0302);  
    	lcd_write_reg_with_value(0x003B,0x0302);  
    	lcd_write_reg_with_value(0x0023,0x0000);  
    	lcd_write_reg_with_value(0x0024,0x0000);  
    	lcd_write_reg_with_value(0x0025,0x8000);  
    	lcd_write_reg_with_value(0x004f,0);        //行首址0
    	lcd_write_reg_with_value(0x004e,0);        //列首址0
	}else if(lcddev.id==0x4531)//OK |/|/|
	{
		lcd_write_reg_with_value(0X00,0X0001);   
		delay_ms(10);   
		lcd_write_reg_with_value(0X10,0X1628);   
		lcd_write_reg_with_value(0X12,0X000e);//0x0006    
		lcd_write_reg_with_value(0X13,0X0A39);   
		delay_ms(10);   
		lcd_write_reg_with_value(0X11,0X0040);   
		lcd_write_reg_with_value(0X15,0X0050);   
		delay_ms(10);   
		lcd_write_reg_with_value(0X12,0X001e);//16    
		delay_ms(10);   
		lcd_write_reg_with_value(0X10,0X1620);   
		lcd_write_reg_with_value(0X13,0X2A39);   
		delay_ms(10);   
		lcd_write_reg_with_value(0X01,0X0100);   
		lcd_write_reg_with_value(0X02,0X0300);   
		lcd_write_reg_with_value(0X03,0X1038);//改变方向的   
		lcd_write_reg_with_value(0X08,0X0202);   
		lcd_write_reg_with_value(0X0A,0X0008);   
		lcd_write_reg_with_value(0X30,0X0000);   
		lcd_write_reg_with_value(0X31,0X0402);   
		lcd_write_reg_with_value(0X32,0X0106);   
		lcd_write_reg_with_value(0X33,0X0503);   
		lcd_write_reg_with_value(0X34,0X0104);   
		lcd_write_reg_with_value(0X35,0X0301);   
		lcd_write_reg_with_value(0X36,0X0707);   
		lcd_write_reg_with_value(0X37,0X0305);   
		lcd_write_reg_with_value(0X38,0X0208);   
		lcd_write_reg_with_value(0X39,0X0F0B);   
		lcd_write_reg_with_value(0X41,0X0002);   
		lcd_write_reg_with_value(0X60,0X2700);   
		lcd_write_reg_with_value(0X61,0X0001);   
		lcd_write_reg_with_value(0X90,0X0210);   
		lcd_write_reg_with_value(0X92,0X010A);   
		lcd_write_reg_with_value(0X93,0X0004);   
		lcd_write_reg_with_value(0XA0,0X0100);   
		lcd_write_reg_with_value(0X07,0X0001);   
		lcd_write_reg_with_value(0X07,0X0021);   
		lcd_write_reg_with_value(0X07,0X0023);   
		lcd_write_reg_with_value(0X07,0X0033);   
		lcd_write_reg_with_value(0X07,0X0133);   
		lcd_write_reg_with_value(0XA0,0X0000); 
	}else if(lcddev.id==0x4535)
	{			      
		lcd_write_reg_with_value(0X15,0X0030);   
		lcd_write_reg_with_value(0X9A,0X0010);   
 		lcd_write_reg_with_value(0X11,0X0020);   
 		lcd_write_reg_with_value(0X10,0X3428);   
		lcd_write_reg_with_value(0X12,0X0002);//16    
 		lcd_write_reg_with_value(0X13,0X1038);   
		delay_ms(40);   
		lcd_write_reg_with_value(0X12,0X0012);//16    
		delay_ms(40);   
  		lcd_write_reg_with_value(0X10,0X3420);   
 		lcd_write_reg_with_value(0X13,0X3038);   
		delay_ms(70);   
		lcd_write_reg_with_value(0X30,0X0000);   
		lcd_write_reg_with_value(0X31,0X0402);   
		lcd_write_reg_with_value(0X32,0X0307);   
		lcd_write_reg_with_value(0X33,0X0304);   
		lcd_write_reg_with_value(0X34,0X0004);   
		lcd_write_reg_with_value(0X35,0X0401);   
		lcd_write_reg_with_value(0X36,0X0707);   
		lcd_write_reg_with_value(0X37,0X0305);   
		lcd_write_reg_with_value(0X38,0X0610);   
		lcd_write_reg_with_value(0X39,0X0610); 
		  
		lcd_write_reg_with_value(0X01,0X0100);   
		lcd_write_reg_with_value(0X02,0X0300);   
		lcd_write_reg_with_value(0X03,0X1030);//改变方向的   
		lcd_write_reg_with_value(0X08,0X0808);   
		lcd_write_reg_with_value(0X0A,0X0008);   
 		lcd_write_reg_with_value(0X60,0X2700);   
		lcd_write_reg_with_value(0X61,0X0001);   
		lcd_write_reg_with_value(0X90,0X013E);   
		lcd_write_reg_with_value(0X92,0X0100);   
		lcd_write_reg_with_value(0X93,0X0100);   
 		lcd_write_reg_with_value(0XA0,0X3000);   
 		lcd_write_reg_with_value(0XA3,0X0010);   
		lcd_write_reg_with_value(0X07,0X0001);   
		lcd_write_reg_with_value(0X07,0X0021);   
		lcd_write_reg_with_value(0X07,0X0023);   
		lcd_write_reg_with_value(0X07,0X0033);   
		lcd_write_reg_with_value(0X07,0X0133);   
	}		 
	lcd_set_display_direction(0);		 	//默认为竖屏
	LCD_LED(1);					//点亮背光
	LCD_Clear(WHITE);
}  
//清屏函数
//color:要清屏的填充色
void LCD_Clear(u16 color)
{
	u32 index=0;      
	u32 totalpoint=lcddev.width;
	totalpoint*=lcddev.height; 			//得到总点数
	if((lcddev.id==0X6804)&&(lcddev.dir==1))//6804横屏的时候特殊处理  
	{						    
 		lcddev.dir=0;	 
 		lcddev.setxcmd=0X2A;
		lcddev.setycmd=0X2B;  	 			
		lcd_set_cursor(0x00,0x0000);		//设置光标位置  
 		lcddev.dir=1;	 
  		lcddev.setxcmd=0X2B;
		lcddev.setycmd=0X2A;  	 
 	}else lcd_set_cursor(0x00,0x0000);	//设置光标位置 
	lcd_write_data_prepare();     		//开始写入GRAM	 	  
	for(index=0;index<totalpoint;index++)
	{
		LCD->ram=color;	   
	}
}  
//在指定区域内填充单个颜色
//(sx,sy),(ex,ey):填充矩形对角坐标,区域大小为:(ex-sx+1)*(ey-sy+1)   
//color:要填充的颜色
void LCD_Fill(u16 sx,u16 sy,u16 ex,u16 ey,u16 color)
{          
	u16 i,j;
	u16 xlen=0;
	u16 temp;
	if((lcddev.id==0X6804)&&(lcddev.dir==1))	//6804横屏的时候特殊处理  
	{
		temp=sx;
		sx=sy;
		sy=lcddev.width-ex-1;	  
		ex=ey;
		ey=lcddev.width-temp-1;
 		lcddev.dir=0;	 
 		lcddev.setxcmd=0X2A;
		lcddev.setycmd=0X2B;  	 			
		LCD_Fill(sx,sy,ex,ey,color);  
 		lcddev.dir=1;	 
  		lcddev.setxcmd=0X2B;
		lcddev.setycmd=0X2A;  	 
 	}else
	{
		xlen=ex-sx+1;	 
		for(i=sy;i<=ey;i++)
		{
		 	lcd_set_cursor(sx,i);      				//设置光标位置 
			lcd_write_data_prepare();     			//开始写入GRAM	  
			for(j=0;j<xlen;j++)lcd_write_data(color);	//设置光标位置 	    
		}
	}	 
}  
//在指定区域内填充指定颜色块			 
//(sx,sy),(ex,ey):填充矩形对角坐标,区域大小为:(ex-sx+1)*(ey-sy+1)   
//color:要填充的颜色
void LCD_Color_Fill(u16 sx,u16 sy,u16 ex,u16 ey,u16 *color)
{  
	u16 height,width;
	u16 i,j;
	width=ex-sx+1; 			//得到填充的宽度
	height=ey-sy+1;			//高度
 	for(i=0;i<height;i++)
	{
 		lcd_set_cursor(sx,sy+i);   	//设置光标位置 
		lcd_write_data_prepare();     //开始写入GRAM
		for(j=0;j<width;j++)LCD->ram=color[i*height+j];//写入数据 
	}	  
}  
//画线
//x1,y1:起点坐标
//x2,y2:终点坐标  
void LCD_DrawLine(u16 x1, u16 y1, u16 x2, u16 y2)
{
	u16 t; 
	int xerr=0,yerr=0,delta_x,delta_y,distance; 
	int incx,incy,uRow,uCol; 
	delta_x=x2-x1; //计算坐标增量 
	delta_y=y2-y1; 
	uRow=x1; 
	uCol=y1; 
	if(delta_x>0)incx=1; //设置单步方向 
	else if(delta_x==0)incx=0;//垂直线 
	else {incx=-1;delta_x=-delta_x;} 
	if(delta_y>0)incy=1; 
	else if(delta_y==0)incy=0;//水平线 
	else{incy=-1;delta_y=-delta_y;} 
	if( delta_x>delta_y)distance=delta_x; //选取基本增量坐标轴 
	else distance=delta_y; 
	for(t=0;t<=distance+1;t++ )//画线输出 
	{  
		LCD_DrawPoint(uRow,uCol);//画点 
		xerr+=delta_x ; 
		yerr+=delta_y ; 
		if(xerr>distance) 
		{ 
			xerr-=distance; 
			uRow+=incx; 
		} 
		if(yerr>distance) 
		{ 
			yerr-=distance; 
			uCol+=incy; 
		} 
	}  
}    
//画矩形	  
//(x1,y1),(x2,y2):矩形的对角坐标
void LCD_DrawRectangle(u16 x1, u16 y1, u16 x2, u16 y2)
{
	LCD_DrawLine(x1,y1,x2,y1);
	LCD_DrawLine(x1,y1,x1,y2);
	LCD_DrawLine(x1,y2,x2,y2);
	LCD_DrawLine(x2,y1,x2,y2);
}
//在指定位置画一个指定大小的圆
//(x,y):中心点
//r    :半径
void Draw_Circle(u16 x0,u16 y0,u8 r)
{
	int a,b;
	int di;
	a=0;b=r;	  
	di=3-(r<<1);             //判断下个点位置的标志
	while(a<=b)
	{
		LCD_DrawPoint(x0+a,y0-b);             //5
 		LCD_DrawPoint(x0+b,y0-a);             //0           
		LCD_DrawPoint(x0+b,y0+a);             //4               
		LCD_DrawPoint(x0+a,y0+b);             //6 
		LCD_DrawPoint(x0-a,y0+b);             //1       
 		LCD_DrawPoint(x0-b,y0+a);             
		LCD_DrawPoint(x0-a,y0-b);             //2             
  		LCD_DrawPoint(x0-b,y0-a);             //7     	         
		a++;
		//使用Bresenham算法画圆     
		if(di<0)di +=4*a+6;	  
		else
		{
			di+=10+4*(a-b);   
			b--;
		} 						    
	}
} 									  
//在指定位置显示一个字符
//x,y:起始坐标
//num:要显示的字符:" "--->"~"
//size:字体大小 12/16
//mode:叠加方式(1)还是非叠加方式(0)
void LCD_ShowChar(u16 x,u16 y,u8 num,u8 size,u8 mode)
{  							  
    u8 temp,t1,t;
	u16 y0=y;
	u16 colortemp=POINT_COLOR;      			     
	//设置窗口		   
	num=num-' ';//得到偏移后的值
	if(!mode) //非叠加方式
	{
	    for(t=0;t<size;t++)
	    {   
			if(size==12)temp=asc2_1206[num][t];  //调用1206字体
			else temp=asc2_1608[num][t];		 //调用1608字体 	                          
	        for(t1=0;t1<8;t1++)
			{			    
		        if(temp&0x80)POINT_COLOR=colortemp;
				else POINT_COLOR=BACK_COLOR;
				LCD_DrawPoint(x,y);	
				temp<<=1;
				y++;
				if(x>=lcddev.width){POINT_COLOR=colortemp;return;}//超区域了
				if((y-y0)==size)
				{
					y=y0;
					x++;
					if(x>=lcddev.width){POINT_COLOR=colortemp;return;}//超区域了
					break;
				}
			}  	 
	    }    
	}else//叠加方式
	{
	    for(t=0;t<size;t++)
	    {   
			if(size==12)temp=asc2_1206[num][t];  //调用1206字体
			else temp=asc2_1608[num][t];		 //调用1608字体 	                          
	        for(t1=0;t1<8;t1++)
			{			    
		        if(temp&0x80)LCD_DrawPoint(x,y); 
				temp<<=1;
				y++;
				if(x>=lcddev.height){POINT_COLOR=colortemp;return;}//超区域了
				if((y-y0)==size)
				{
					y=y0;
					x++;
					if(x>=lcddev.width){POINT_COLOR=colortemp;return;}//超区域了
					break;
				}
			}  	 
	    }     
	}
	POINT_COLOR=colortemp;	    	   	 	  
}   
//m^n函数
//返回值:m^n次方.
u32 LCD_Pow(u8 m,u8 n)
{
	u32 result=1;	 
	while(n--)result*=m;    
	return result;
}			 
//显示数字,高位为0,则不显示
//x,y :起点坐标	 
//len :数字的位数
//size:字体大小
//color:颜色 
//num:数值(0~4294967295);	 
void LCD_ShowNum(u16 x,u16 y,u32 num,u8 len,u8 size)
{         	
	u8 t,temp;
	u8 enshow=0;						   
	for(t=0;t<len;t++)
	{
		temp=(num/LCD_Pow(10,len-t-1))%10;
		if(enshow==0&&t<(len-1))
		{
			if(temp==0)
			{
				LCD_ShowChar(x+(size/2)*t,y,' ',size,0);
				continue;
			}else enshow=1; 
		 	 
		}
	 	LCD_ShowChar(x+(size/2)*t,y,temp+'0',size,0); 
	}
} 
//显示数字,高位为0,还是显示
//x,y:起点坐标
//num:数值(0~999999999);	 
//len:长度(即要显示的位数)
//size:字体大小
//mode:
//[7]:0,不填充;1,填充0.
//[6:1]:保留
//[0]:0,非叠加显示;1,叠加显示.
void LCD_ShowxNum(u16 x,u16 y,u32 num,u8 len,u8 size,u8 mode)
{  
	u8 t,temp;
	u8 enshow=0;						   
	for(t=0;t<len;t++)
	{
		temp=(num/LCD_Pow(10,len-t-1))%10;
		if(enshow==0&&t<(len-1))
		{
			if(temp==0)
			{
				if(mode&0X80)LCD_ShowChar(x+(size/2)*t,y,'0',size,mode&0X01);  
				else LCD_ShowChar(x+(size/2)*t,y,' ',size,mode&0X01);  
 				continue;
			}else enshow=1; 
		 	 
		}
	 	LCD_ShowChar(x+(size/2)*t,y,temp+'0',size,mode&0X01); 
	}
} 
//显示字符串
//x,y:起点坐标
//width,height:区域大小  
//size:字体大小
//*p:字符串起始地址		  
void LCD_ShowString(u16 x,u16 y,u16 width,u16 height,u8 size,u8 *p)
{         
	u8 x0=x;
	width+=x;
	height+=y;
    while((*p<='~')&&(*p>=' '))//判断是不是非法字符!
    {       
        if(x>=width){x=x0;y+=size;}
        if(y>=height)break;//退出
        LCD_ShowChar(x,y,*p,size,0);
        x+=size/2;
        p++;
    }  
}

void set_backlight(rt_uint8_t pwm)
{
	lcd_write_reg(0xBE);
	lcd_write_data(0x05);
	lcd_write_data(pwm*2.55);
	lcd_write_data(0x01);
	lcd_write_data(0xFF);
	lcd_write_data(0x00);
	lcd_write_data(0x00);
}

void _lcd_low_level_init(void)
{
	LCD_Init();
}

static rt_err_t lcd_init(rt_device_t dev)
{
	return RT_EOK;
}

static rt_err_t lcd_open(rt_device_t dev, rt_uint16_t oflag)
{
	return RT_EOK;
}

static rt_err_t lcd_close(rt_device_t dev)
{
	return RT_EOK;
}

static rt_err_t lcd_control(rt_device_t dev, int cmd, void *args)
{
	switch (cmd)
	{
	case RTGRAPHIC_CTRL_GET_INFO:
	{
		struct rt_device_graphic_info *info;

		info = (struct rt_device_graphic_info*) args;
		RT_ASSERT(info != RT_NULL);

		info->bits_per_pixel = 16;
		info->pixel_format = RTGRAPHIC_PIXEL_FORMAT_RGB565;
		info->framebuffer = RT_NULL;
		info->width = 240;
		info->height = 320;
	}
	break;

	case RTGRAPHIC_CTRL_RECT_UPDATE:
		/* nothong to be done */
		break;

	default:
		break;
	}

	return RT_EOK;
}

static void lcd_set_pixel(const char* pixel, int x, int y)
{
	lcd_set_cursor(x, y);
	lcd_write_data_prepare();
	lcd_write_data(*(uint16_t *)pixel);
}
#ifdef RT_USING_FINSH
static void finsh_lcd_set_pixel(uint16_t color, int x, int y)
{
	rt_kprintf("lcd set pixel, color: %X, x: %d, y: %d", color, x, y);
	lcd_set_pixel((const char *)&color, x, y);
}
FINSH_FUNCTION_EXPORT_ALIAS(finsh_lcd_set_pixel, lcd_set_pixel, set pixel in lcd display);
#endif

static void lcd_get_pixel(char* pixel, int x, int y)
{
	rt_uint16_t red = 0;
	rt_uint16_t green = 0;
	rt_uint16_t blue = 0;

	if (x >= lcddev.width || y >= lcddev.height)
	{
		*(rt_uint16_t*)pixel = 0;
		return;
	}

	lcd_set_cursor(x, y);

	lcd_write_reg(0X2E);
	lcd_read_data();
	red = lcd_read_data();
	delay_us(2);

	blue = lcd_read_data();
	green = red & 0XFF;

	*(rt_uint16_t*)pixel = (((red >> 11) << 11) | ((green >> 10) << 5) | (blue >> 11));
}
#ifdef RT_USING_FINSH
static void finsh_lcd_get_pixel(int x, int y)
{
	uint16_t pixel;
	lcd_get_pixel((char *)&pixel, x, y);
	rt_kprintf("lcd get pixel, pixel: 0x%X, x: %d, y: %d", pixel, x, y);
}
FINSH_FUNCTION_EXPORT_ALIAS(finsh_lcd_get_pixel, lcd_get_pixel, get pixel in lcd display);
#endif

static void lcd_draw_hline(const char* pixel, int x1, int x2, int y)
{
	lcd_set_cursor(x1, y);
	lcd_write_data_prepare();

	for (; x1 < x2; x1++)
	{
		lcd_write_data(*(uint16_t *)pixel);
	}
}
#ifdef RT_USING_FINSH
static void finsh_lcd_draw_hline(uint16_t pixel, int x1, int x2, int y)
{
	lcd_draw_hline((const char *)&pixel, x1, x2, y);
	rt_kprintf("lcd draw hline, pixel: 0x%X, x1: %d, x2: %d, y: %d", pixel, x1, x2, y);
}
FINSH_FUNCTION_EXPORT_ALIAS(finsh_lcd_draw_hline, lcd_draw_hline, draw hline in lcd display);
#endif

static void lcd_draw_vline(const char* pixel, int x, int y1, int y2)
{
	for (; y1 < y2; y1++)
	{
		lcd_set_pixel(pixel, x, y1);  //write red data
	}
}
#ifdef RT_USING_FINSH
static void finsh_lcd_draw_vline(uint16_t pixel, int x, int y1, int y2)
{
	lcd_draw_vline((const char *)&pixel, x, y1, y2);
	rt_kprintf("lcd draw hline, pixel: 0x%X, x: %d, y: %d", pixel, y1, y2);
}
FINSH_FUNCTION_EXPORT_ALIAS(finsh_lcd_draw_vline, lcd_draw_vline, draw vline in lcd display);
#endif

static void lcd_blit_line(const char* pixels, int x, int y, rt_size_t size)
{
	rt_uint16_t *ptr = (rt_uint16_t*)pixels;

	lcd_set_cursor(x, y);
	lcd_write_data_prepare();

	while (size--)
	{
		lcd_write_data(*ptr++);
	}
}
#ifdef RT_USING_FINSH
#define LINE_LEN 30
static void finsh_lcd_blit_line(int x, int y)
{
	uint16_t pixels[LINE_LEN];
	int i;

	for (i = 0; i < LINE_LEN; i++)
	{
		pixels[i] = i * 40 + 50;
	}

	lcd_blit_line((const char *)pixels, x, y, LINE_LEN);
	rt_kprintf("lcd blit line, x: %d, y: %d", x, y);
}
FINSH_FUNCTION_EXPORT_ALIAS(finsh_lcd_blit_line, lcd_blit_line, draw blit line in lcd display);
#endif

static int rt_hw_lcd_init(void)
{
	_lcd_low_level_init();

	static struct rt_device lcd_device;

	static struct rt_device_graphic_ops ops =
	{
		lcd_set_pixel,
		lcd_get_pixel,
		lcd_draw_hline,
		lcd_draw_vline,
		lcd_blit_line
	};

	/* register lcd device */
	lcd_device.type = RT_Device_Class_Graphic;
	lcd_device.init = lcd_init;
	lcd_device.open = lcd_open;
	lcd_device.close = lcd_close;
	lcd_device.control = lcd_control;
	lcd_device.read = RT_NULL;
	lcd_device.write = RT_NULL;

	lcd_device.user_data = &ops;

	/* register graphic device driver */
	rt_device_register(&lcd_device, "lcd",
		RT_DEVICE_FLAG_RDWR | RT_DEVICE_FLAG_STANDALONE);

	return 0;
}
INIT_BOARD_EXPORT(rt_hw_lcd_init);

