#include <stm32f10x.h>
#include <drivers/rt_drv_pwm.h>

#include "drv_pwm.h"

typedef struct
{
  TIM_TypeDef                    *Instance;     /*!< Register base address             */
  TIM_TimeBaseInitTypeDef        Init;          /*!< TIM Time Base required parameters */
  unsigned short                 Channel;       /*!< Active channel                    */
}TIM_HandleTypeDef;

static rt_err_t drv_pwm_enable(TIM_TypeDef *htim, struct rt_pwm_configuration *configuration, rt_bool_t enable)
{
    if((htim == TIM1) || (htim == TIM8))
    {
        rt_kprintf("The TIMX error\n");
        return RT_ERROR;
    }
    if((configuration->channel != TIM_Channel_1) && (configuration->channel != TIM_Channel_2) && 
        (configuration->channel != TIM_Channel_3) && (configuration->channel != TIM_Channel_4))
    {
        rt_kprintf("The TIMX Chx %d error\n",configuration->channel);
        return RT_ERROR;
    }
    if(enable)
    {
        TIM_CCxCmd(htim,configuration->channel,TIM_CCx_Enable);
    }
    else
    {
        TIM_CCxCmd(htim,configuration->channel,TIM_CCx_Disable);
    }
    return RT_EOK;
}

static rt_err_t pwm_set(TIM_TypeDef *htim, struct rt_pwm_configuration *configuration)
{
    if((htim == TIM1) || (htim == TIM8))
    {
        rt_kprintf("The TIMX error\n");
        return RT_ERROR;
    }
	
    if((configuration->channel != TIM_Channel_1) && (configuration->channel != TIM_Channel_2) && 
        (configuration->channel != TIM_Channel_3) && (configuration->channel != TIM_Channel_4))
    {
        rt_kprintf("The TIMX Chx %d error\n",configuration->channel);
        return RT_ERROR;
    }	

    TIM_SetAutoreload(htim,configuration->period);

     #ifdef RT_USING_TIMx_CH1
        TIM_SetCompare1(htim,configuration->pulse);
    #endif    
    #ifdef RT_USING_TIMx_CH2
        TIM_SetCompare2(htim,configuration->pulse);
    #endif    
    #ifdef RT_USING_TIMx_CH3
        TIM_SetCompare3(htim,configuration->pulse);
    #endif
    #ifdef RT_USING_TIMx_CH4
        TIM_SetCompare4(htim,configuration->pulse);
    #endif
	
    return RT_EOK;
}

static rt_err_t pwm_get(TIM_TypeDef *htim, struct rt_pwm_configuration *configuration)
{
    return RT_EOK;
}

static rt_err_t drv_pwm_control(struct rt_device_pwm *device, int cmd, void *arg)
{
    struct rt_pwm_configuration *configuration = (struct rt_pwm_configuration *)arg;
    TIM_HandleTypeDef *htim = (TIM_HandleTypeDef *)device->parent.user_data;
    switch(cmd)
    {
        case PWM_CMD_ENABLE:
            return drv_pwm_enable(htim->Instance,configuration,RT_TRUE);
        case PWM_CMD_DISABLE:
            return drv_pwm_enable(htim->Instance,configuration,RT_FALSE);
        case PWM_CMD_SET:
            return pwm_set(htim->Instance,configuration);
        case PWM_CMD_GET:
            return pwm_get(htim->Instance,configuration);
        default:
            return RT_EINVAL;
    }
}

TIM_HandleTypeDef htim3;
// TIM2 : ch1 = PA0,ch2 = PA1,ch3 = PA2,ch4 = PA3
// TIM3 : ch1 = PA6,ch2 = PA7,ch3 = PB0,ch4 = PB1
static rt_err_t Timex_PWMChannelx_Config(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    TIM_OCInitTypeDef  TIM_OCInitStructure;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	
    htim3.Init.TIM_Period = 0;
    htim3.Init.TIM_Prescaler = 9;
    htim3.Init.TIM_ClockDivision = 0;
    htim3.Init.TIM_CounterMode = TIM_CounterMode_Up;
    
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;

    #ifdef RT_USING_PWM_TIM3
        htim3.Instance = TIM3;
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
        #ifdef RT_USING_PWM_TIM3_CH1
            RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
            GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;
            GPIO_Init(GPIOA, &GPIO_InitStructure);
            TIM_OC1Init(htim3.Instance, &TIM_OCInitStructure);
            TIM_OC1PreloadConfig(htim3.Instance, TIM_OCPreload_Enable);
        #endif
        #ifdef RT_USING_PWM_TIM3_CH2
            RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
            GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7;
            GPIO_Init(GPIOA, &GPIO_InitStructure);
            TIM_OC2Init(htim3.Instance, &TIM_OCInitStructure);
            TIM_OC2PreloadConfig(htim3.Instance, TIM_OCPreload_Enable);
        #endif
        #ifdef RT_USING_PWM_TIM3_CH3
            RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB | RCC_APB2Periph_AFIO, ENABLE);
            GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
            GPIO_Init(GPIOB, &GPIO_InitStructure);
            TIM_OC3Init(htim3.Instance, &TIM_OCInitStructure);
            TIM_OC3PreloadConfig(htim3.Instance, TIM_OCPreload_Enable);
        #endif
        #ifdef RT_USING_PWM_TIM3_CH4
            RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB | RCC_APB2Periph_AFIO, ENABLE);
            GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;
            GPIO_Init(GPIOB, &GPIO_InitStructure);
            TIM_OC4Init(htim3.Instance, &TIM_OCInitStructure);
            TIM_OC4PreloadConfig(htim3.Instance, TIM_OCPreload_Enable);
        #endif
    
    #endif
  
    TIM_TimeBaseInit(htim3.Instance, &htim3.Init);
 
    TIM_Cmd(htim3.Instance, ENABLE);

	return 0;
}

static struct rt_pwm_ops drv_ops =
{
    drv_pwm_control
};

int drv_pwm_init(void)
{
#ifdef RT_USING_PWM_TIM1
    MX_TIM1_Init();
    rt_device_pwm_register(rt_calloc(1, sizeof(struct rt_device_pwm)), "pwm1", &drv_ops, &htim1);
#endif

#ifdef RT_USING_PWM_TIM3
    Timex_PWMChannelx_Config();
    rt_device_pwm_register(rt_calloc(1, sizeof(struct rt_device_pwm)), "pwm3", &drv_ops, &htim3);
#endif
    return 0;
}
//INIT_DEVICE_EXPORT(drv_pwm_init);

