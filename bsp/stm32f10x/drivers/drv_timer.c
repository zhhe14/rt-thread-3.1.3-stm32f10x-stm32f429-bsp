#include <rtthread.h>
#include <rtdevice.h>
#include <stm32f10x.h>
#include <stdio.h>

#include "system_config.h"

#include "drv_timer.h"
#include "led.h"

static rt_hwtimer_t hwtimer3;

//通用定时器3中断初始化
//这里时钟选择为APB1的2倍，而APB1为36M
//arr：自动重装值。
//psc：时钟预分频数
//这里使用的是定时器3!

static void TIM3_NVIC_Configuration(void)
{
	NVIC_InitTypeDef NVIC_InitStructure;

	//中断优先级NVIC设置
	NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;  //TIM3中断
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;  //先占优先级0级
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;  //从优先级3级
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE; //IRQ通道被使能
	NVIC_Init(&NVIC_InitStructure);  //初始化NVIC寄存器
}

/*
	Tout= ((arr+1)*(psc+1))/Tclk
		= ((4999+1)*(7199+1))/72MHz
		= 500000us
		= 500ms
*/
static void TIM3_Int_Init(u16 arr, u16 psc)
{
    TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE); //时钟使能
	
	//定时器TIM3初始化
	TIM_TimeBaseStructure.TIM_Period = arr; //设置在下一个更新事件装入活动的自动重装载寄存器周期的值	
	TIM_TimeBaseStructure.TIM_Prescaler = psc; //设置用来作为TIMx时钟频率除数的预分频值
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1; //设置时钟分割:TDTS = Tck_tim	
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;  //TIM向上计数模式
	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure); //根据指定的参数初始化TIMx的时间基数单位
 
	TIM_ITConfig(TIM3,TIM_IT_Update, ENABLE); //使能指定的TIM3中断,允许更新中断
	TIM3_NVIC_Configuration(); //定时器3的中断配置

	//先不启动定时器
	TIM_Cmd(TIM3, DISABLE);
}

int TIM3_Configuration(void)
{
	TIM3_Int_Init(10000-1, 7200-1);

	return 0;
}
//INIT_DEVICE_EXPORT(TIM3_Configuration);

//定时器3中断服务程序
void TIM3_IRQHandler(void)   //TIM3中断
{
	rt_interrupt_enter();//进入临界区
	
	if (TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET)  //检查TIM3更新中断发生与否
	{
		rt_device_hwtimer_isr(&hwtimer3);//调用驱动框架的中断通知函数

		/* 查找系统中的leds设备*/
	    rt_device_t led_device = rt_device_find("leds");
	    if (led_device != RT_NULL)
	    {
	        rt_device_open(led_device, RT_DEVICE_OFLAG_RDWR);
		}
		
		char buf[2] = {0};
		rt_device_read(led_device, READ_ALLLED_STATUS, buf, 2);

		if (buf[0] == 0)
			rt_device_control(led_device, IOCTL_LED0_OFF, NULL);
		else if (buf[0] == 1)
			rt_device_control(led_device, IOCTL_LED0_ON, NULL);

		if (buf[1] == 0)
			rt_device_control(led_device, IOCTL_LED1_OFF, NULL);
		else if (buf[1] == 1)
		rt_device_control(led_device, IOCTL_LED1_ON, NULL);

		rt_device_close(led_device);

		TIM_ClearITPendingBit(TIM3, TIM_IT_Update);  //清除TIMx更新中断标志 
	}

	rt_interrupt_leave();//离开临界区
}


void hwtimer_init(struct rt_hwtimer_device *timer, rt_uint32_t state)
{
	if(state==1){
		TIM3_Int_Init(10000-1, timer->freq-1);
	}
}

rt_err_t hwtimer_start(struct rt_hwtimer_device *timer, rt_uint32_t cnt, rt_hwtimer_mode_t mode)
{
	/*
		溢出值 TIM_Period = cnt - 1  
		时钟分频：72MHz / (7200-1)
	*/
	//配置定时器
	TIM3_Int_Init(cnt-1, (SystemCoreClock / timer->freq) - 1);
	//启动定时器
	TIM_Cmd(TIM3, ENABLE);

	return RT_EOK;
}

void hwtimer_stop(struct rt_hwtimer_device *timer)
{
	TIM_Cmd(TIM3, DISABLE);
}

rt_uint32_t hwtimer_count_get(struct rt_hwtimer_device *timer)
{
	return TIM_GetCounter(TIM3);
}

rt_err_t hwtimer_control(struct rt_hwtimer_device *timer, rt_uint32_t cmd, void *args)
{
	rt_uint32_t freq;//用于获取参数中的频率设定值
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	
	//判定命令是不是 设置频率命令
	if(cmd == HWTIMER_CTRL_FREQ_SET){
		freq = *((rt_uint32_t*)args);//从参数中获取频率设定值
		TIM_TimeBaseStructure.TIM_Prescaler = SystemCoreClock / freq -1;//设置硬件分频系数
		TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);//初始化TIM3硬件
	}
	
	return RT_EOK;
}

static struct rt_hwtimer_ops hwtimer_ops={
	hwtimer_init,
	hwtimer_start,
	hwtimer_stop,
	hwtimer_count_get,
	hwtimer_control,
};

static struct rt_hwtimer_info hw_timer3_info= {
	100000,					//最大频率1MHz
	10000,					//最小频率5KHz     每秒计的次数
	0xFFFF,					//最大计数值
	HWTIMER_MODE_PERIOD,	//周期模式
};

//static struct rt_device hwtimer_device;
int hw_timer3_init(void)
{	
	hwtimer3.info = &hw_timer3_info;
	hwtimer3.ops = &hwtimer_ops;
	hwtimer3.mode = HWTIMER_MODE_PERIOD;

	rt_device_hwtimer_register(&hwtimer3, "timer3", RT_NULL);

	return RT_EOK;
}INIT_DEVICE_EXPORT(hw_timer3_init);

