/*
 * Copyright (c) 2006-2018, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2015-01-05     Bernard      the first version
 */
#ifndef GPIO_H__
#define GPIO_H__


struct stm32_hw_pin_userdata
{
    int pin;
    uint32_t mode;
};

#define PIN_USERDATA_END {-1,0}

extern struct stm32_hw_pin_userdata stm32_pins[];

int stm32_hw_pin_init(void);

void stm32_pin_write(rt_device_t dev, rt_base_t pin, rt_base_t value);
int stm32_pin_read(rt_device_t dev, rt_base_t pin);

void stm32_pin_mode(rt_device_t dev, rt_base_t pin, rt_base_t mode);

rt_err_t stm32_pin_attach_irq(struct rt_device *device, rt_int32_t pin,
                  rt_uint32_t mode, void (*hdr)(void *args), void *args);

rt_err_t stm32_pin_detach_irq(struct rt_device *device, rt_int32_t pin);

rt_err_t stm32_pin_irq_enable(struct rt_device *device, rt_base_t pin,
                                                  rt_uint32_t enabled);



#endif
