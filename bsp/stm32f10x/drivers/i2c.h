#ifndef __I2C2_SOFT_INIT_H__
#define __I2C2_SOFT_INIT_H__

#include <rtthread.h>
#include <rtdevice.h>

#ifdef BSP_USING_I2C1
#define I2C1_GPIO 		GPIOB
#define I2C1_GPIO_SCL 	GPIO_Pin_6
#define I2C1_GPIO_SDA 	GPIO_Pin_7
#endif

#ifdef BSP_USING_I2C2
#define I2C2_GPIO 		GPIOB
#define I2C2_GPIO_SCL 	GPIO_Pin_10
#define I2C2_GPIO_SDA 	GPIO_Pin_11
#endif
#define RCC_I2C 		RCC_APB2Periph_GPIOB

int rt_hw_i2c_init(void);

#endif
