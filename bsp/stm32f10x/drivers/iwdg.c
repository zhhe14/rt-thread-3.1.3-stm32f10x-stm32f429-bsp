#include <rtthread.h>
#include <rtdevice.h>
#include <stm32f10x.h>
#include "system_config.h"

#include <drivers/watchdog.h>
#include <math.h>
#include <stdio.h>

#include "iwdg.h"

#ifdef WATCH_DOG_ENABLE
//初始化独立看门狗
//prer:分频数:0~7(只有低3位有效!)
//分频因子=4*2^prer.但最大值只能是256!
//rlr:重装载寄存器值:低11位有效.
//时间计算(大概):Tout=((4*2^prer)*rlr)/40 (ms).
static void IWDG_Init(u8 prer,u16 rlr) 
{	
 	IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);  //使能对寄存器IWDG_PR和IWDG_RLR的写操作
	
	IWDG_SetPrescaler(prer);  //设置IWDG预分频值:设置IWDG预分频值为64
	
	IWDG_SetReload(rlr);  //设置IWDG重装载值
	
	IWDG_ReloadCounter();  //按照IWDG重装载寄存器的值重装载IWDG计数器
	
	//IWDG_Enable();  //使能IWDG
}

static void watch_door_thread_entry(void* parameter)
{
    rt_err_t ret = RT_EOK;
    rt_uint32_t timeout = 10;    /* 溢出时间 10 秒 */
    char device_name[RT_NAME_MAX];
	
    rt_strncpy(device_name, (const char *)parameter, RT_NAME_MAX);

    /* 根据设备名称查找看门狗设备，获取设备句柄 */
    rt_device_t wdg_dev = rt_device_find(device_name);
    if (!wdg_dev)
    {
        rt_kprintf("find %s failed!\n", device_name);
        return ;
    }
    /* 初始化设备 */
    ret = rt_device_init(wdg_dev);
    if (ret != RT_EOK)
    {
        rt_kprintf("initialize %s failed!\n", device_name);
        return ;
    }
    /* 设置看门狗溢出时间 */
    ret = rt_device_control(wdg_dev, RT_DEVICE_CTRL_WDT_SET_TIMEOUT, &timeout);
    if (ret != RT_EOK)
    {
        rt_kprintf("set %s timeout failed!\n", device_name);
        return ;
    }
    ret = rt_device_control(wdg_dev, RT_DEVICE_CTRL_WDT_START, RT_NULL);
    if (ret != RT_EOK)
    {
        rt_kprintf("start %s failed!\n", device_name);
        return ;
    }

	while(1)
    {
		rt_device_control(wdg_dev, RT_DEVICE_CTRL_WDT_KEEPALIVE, NULL);
        rt_thread_mdelay(5000);
    }
}


#endif

void watch_dog_init(void)
{
    rt_thread_t thread;
	
#ifdef WATCH_DOG_ENABLE
	// 最大计数为0xFFF=4095  当前3000*3=9000ms
    //IWDG_Init(IWDG_Prescaler_128, 3000);  //开启看门狗

    /* init led thread */
    thread = rt_thread_create("watch_dog",
                               watch_door_thread_entry, IWDG_DEVICE_NAME,
                               WATCH_DOG_THREAD_STACK_SIZE, WATCH_DOG_THREAD_PRIORITY, 10);

    if (thread != RT_NULL)
    {
        rt_thread_startup(thread);
    }
#endif
}


#ifdef RT_USING_DEVICE_OPS
rt_err_t hw_wdt_init(rt_watchdog_t *wdt)
{
	//rt_kprintf("%s\r\n", __FUNCTION__);
	
	IWDG_Init(IWDG_Prescaler_128, 3000);  //初始化看门狗
	return RT_EOK;
}

rt_err_t hw_wdt_control(rt_watchdog_t *wdt, int cmd, void *arg)
{
	//rt_kprintf("%s\r\n", __FUNCTION__);

	switch (cmd)
	{
	case RT_DEVICE_CTRL_WDT_GET_TIMEOUT:
		break;
	case RT_DEVICE_CTRL_WDT_SET_TIMEOUT:
		IWDG_SetReload((uint16_t)(cmd * 40000 / IWDG_Prescaler_128));
		break;
	case RT_DEVICE_CTRL_WDT_GET_TIMELEFT:
		break;
	case RT_DEVICE_CTRL_WDT_KEEPALIVE:
		IWDG_ReloadCounter();// reload feed dog
		break;
	case RT_DEVICE_CTRL_WDT_START:
		IWDG_Enable();  //使能IWDG
		break;
	case RT_DEVICE_CTRL_WDT_STOP:
		break;
	default:
		break;
	}
	
	return RT_EOK;
}

const static struct rt_watchdog_ops wdt_ops = 
{
    hw_wdt_init,
    hw_wdt_control,
};

#else /* RT_USING_DEVICE_OPS */

rt_err_t hw_watchdog_init(rt_device_t dev)
{
	rt_kprintf("%s\r\n", __FUNCTION__);
	
	RT_ASSERT(dev != RT_NULL);
	IWDG_Init(IWDG_Prescaler_128, 3000);  //初始化看门狗
	
	return RT_EOK;
}

rt_err_t hw_watchdog_open(rt_device_t dev, rt_uint16_t oflag)
{
	//rt_kprintf("%s\r\n", __FUNCTION__);
	
	RT_ASSERT(dev != RT_NULL);
	return RT_EOK;
}

rt_err_t hw_watchdog_close(rt_device_t dev)
{
	//rt_kprintf("%s\r\n", __FUNCTION__);
	
	RT_ASSERT(dev != RT_NULL);
	return RT_EOK;
}

rt_err_t hw_watchdog_control(rt_device_t dev, int cmd, void *args)
{
	//rt_kprintf("%s\r\n", __FUNCTION__);
	RT_ASSERT(dev != RT_NULL);

	float ret = 0;
	rt_uint32_t ts = *(rt_uint32_t *)(args);

	switch (cmd)
	{
	case RT_DEVICE_CTRL_WDT_GET_TIMEOUT:
		break;
	case RT_DEVICE_CTRL_WDT_SET_TIMEOUT:		
		ret =  (ts * 40000 / (4 * pow(2, IWDG_Prescaler_128)));
		//rt_kprintf("ts = %d, pow = %d\r\n", ts, (uint16_t)ret);
		IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);  //使能对寄存器IWDG_PR和IWDG_RLR的写操作		
		IWDG_SetPrescaler(IWDG_Prescaler_128);  //设置IWDG预分频值:设置IWDG预分频值为64		
		IWDG_SetReload((uint16_t)ret);  //设置IWDG重装载值		
		IWDG_ReloadCounter();  //按照IWDG重装载寄存器的值重装载IWDG计数器
		break;
	case RT_DEVICE_CTRL_WDT_GET_TIMELEFT:
		break;
	case RT_DEVICE_CTRL_WDT_KEEPALIVE:
		IWDG_ReloadCounter();// reload feed dog
		break;
	case RT_DEVICE_CTRL_WDT_START:
		IWDG_Enable();	//使能IWDG
		break;
	case RT_DEVICE_CTRL_WDT_STOP:
		break;
	default:
		break;
	}
	
	return RT_EOK;
}

#endif /* RT_USING_DEVICE_OPS */

int iwdg_init(void)
{
#ifdef RT_USING_DEVICE_OPS
	struct rt_watchdog_device iwdg_device;
	iwdg_device.ops = &wdt_ops;

 	rt_hw_watchdog_register(&iwdg_device, IWDG_DEVICE_NAME, RT_DEVICE_FLAG_RDWR, RT_NULL);

    return RT_EOK;
#else /* RT_USING_DEVICE_OPS */
	static struct rt_device iwdg_device;

    /* register device */
    iwdg_device.type      	= RT_Device_Class_Miscellaneous;
	iwdg_device.init        = hw_watchdog_init;
    iwdg_device.open        = hw_watchdog_open;
    iwdg_device.close       = hw_watchdog_close;
    iwdg_device.read        = RT_NULL;
    iwdg_device.write       = RT_NULL;
    iwdg_device.control     = hw_watchdog_control;
	iwdg_device.user_data 	= RT_NULL;

    return rt_device_register(&iwdg_device, IWDG_DEVICE_NAME, RT_DEVICE_FLAG_RDWR);
#endif /* RT_USING_DEVICE_OPS */
}INIT_DEVICE_EXPORT(iwdg_init);
