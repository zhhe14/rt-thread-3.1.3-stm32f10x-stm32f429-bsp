#include <rtthread.h>
#include <rtdevice.h>
#include <stm32f10x.h>
#include "gpio.h"
#include "led.h"
#include "beep.h"
#include "key.h"

/* 引脚编号，通过查看设备驱动文件gpio.c确定 */
#ifndef BEEP_PIN_NUM
    #define BEEP_PIN_NUM            139  /* PB8 */
#endif

#ifndef KEY0_PIN_NUM
    #define KEY0_PIN_NUM            3  /* PE4 */
#endif
#ifndef KEY1_PIN_NUM
    #define KEY1_PIN_NUM            2  /* PE3 */
#endif
#ifndef KEY2_PIN_NUM
    #define KEY2_PIN_NUM            1  /* PE2 */
#endif
#ifndef KEY_WKUP_PIN_NUM
    #define KEY_WKUP_PIN_NUM        34  /* PA0 */
#endif

void key0_isr(void *args)
{
    rt_kprintf("key0_isr!\n");

	/* 查找系统中的leds设备*/
    rt_device_t led_device = rt_device_find("leds");
    if (led_device != RT_NULL)
    {
        rt_device_open(led_device, RT_DEVICE_OFLAG_RDWR);
	}

	char buf[2] = {0};
	rt_device_read(led_device, READ_ALLLED_STATUS, buf, 2);

	if (buf[0] == 0)
		rt_device_control(led_device, IOCTL_LED0_OFF, NULL);
	else if (buf[0] == 1)
		rt_device_control(led_device, IOCTL_LED0_ON, NULL);

	if (buf[1] == 0)
		rt_device_control(led_device, IOCTL_LED1_OFF, NULL);
	else if (buf[1] == 1)
		rt_device_control(led_device, IOCTL_LED1_ON, NULL);
}

void key1_isr(void *args)
{
    rt_kprintf("key1_isr!\n");

	/* 查找系统中的leds设备*/
    rt_device_t led_device = rt_device_find("leds");
    if (led_device != RT_NULL)
    {
        rt_device_open(led_device, RT_DEVICE_OFLAG_RDWR);
	}

	rt_device_control(led_device, IOCTL_LED_ALLON, NULL);
}

void key2_isr(void *args)
{
    rt_kprintf("key2_isr!\n");

	/* 查找系统中的leds设备*/
    rt_device_t led_device = rt_device_find("leds");
    if (led_device != RT_NULL)
    {
        rt_device_open(led_device, RT_DEVICE_OFLAG_RDWR);
	}

	rt_device_control(led_device, IOCTL_LED1_ON, NULL);
	rt_device_control(led_device, IOCTL_LED0_OFF, NULL);
}

void key_up_isr(void *args)
{
    rt_kprintf("key_up_isr!\n");
	
	/* 查找系统中的leds设备*/
    rt_device_t led_device = rt_device_find("leds");
    if (led_device != RT_NULL)
    {
        rt_device_open(led_device, RT_DEVICE_OFLAG_RDWR);
	}

	rt_device_control(led_device, IOCTL_LED_ALLOFF, NULL);
}

static rt_err_t hw_keys_init(rt_device_t dev)
{
    return RT_EOK;
}

static rt_err_t hw_keys_open(rt_device_t dev, rt_uint16_t oflag)
{
	/* 按键0引脚为输入模式 */
	rt_pin_mode(KEY0_PIN_NUM, PIN_MODE_INPUT_PULLUP);
	/* 绑定中断，下降沿模式，回调函数名为beep_on */
	rt_pin_attach_irq(KEY0_PIN_NUM, PIN_IRQ_MODE_FALLING, key0_isr, RT_NULL);
	/* 使能中断 */
	rt_pin_irq_enable(KEY0_PIN_NUM, PIN_IRQ_ENABLE);

	/* 按键1引脚为输入模式 */
	rt_pin_mode(KEY1_PIN_NUM, PIN_MODE_INPUT_PULLUP);
	/* 绑定中断，下降沿模式，回调函数名为beep_off */
	rt_pin_attach_irq(KEY1_PIN_NUM, PIN_IRQ_MODE_FALLING, key1_isr, RT_NULL);
	/* 使能中断 */
	rt_pin_irq_enable(KEY1_PIN_NUM, PIN_IRQ_ENABLE);

	/* 按键2引脚为输入模式 */
	rt_pin_mode(KEY2_PIN_NUM, PIN_MODE_INPUT_PULLUP);
	/* 绑定中断，下降沿模式，回调函数名为beep_off */
	rt_pin_attach_irq(KEY2_PIN_NUM, PIN_IRQ_MODE_FALLING, key2_isr, RT_NULL);
	/* 使能中断 */
	rt_pin_irq_enable(KEY2_PIN_NUM, PIN_IRQ_ENABLE);

	/* 按键UP引脚为输入模式 */
	rt_pin_mode(KEY_WKUP_PIN_NUM, PIN_MODE_OUTPUT_OD);
	/* 绑定中断，下降沿模式，回调函数名为beep_off */
	rt_pin_attach_irq(KEY_WKUP_PIN_NUM, PIN_IRQ_MODE_RISING, key_up_isr, RT_NULL);
	/* 使能中断 */
	rt_pin_irq_enable(KEY_WKUP_PIN_NUM, PIN_IRQ_ENABLE);
	
    return RT_EOK;
}

rt_err_t  hw_keys_close(rt_device_t dev)
{
	return RT_EOK;
}

rt_size_t rt_hw_keys_read(rt_device_t dev, rt_off_t pos, void *buffer, rt_size_t size)
{
	return RT_EOK;
}

static rt_err_t  rt_hw_keys_ioctl(rt_device_t dev, int cmd, void *args)
{
    RT_ASSERT(dev != RT_NULL);

    switch(cmd) {
        case IOCTL_READ_KEY0:

            break;
            
        case IOCTL_READ_KEY1:
            break;

		case IOCTL_READ_KEY2:
            break;
            
        case IOCTL_READ_KEYUP:
            break;

        default:break;
    }

    return RT_EOK;
}

#ifdef RT_USING_DEVICE_OPS
const static struct rt_device_ops keys_ops = 
{
    hw_keys_init,
    hw_keys_open,
    hw_keys_close,
    rt_hw_keys_read,
    RT_NULL,
    rt_hw_keys_ioctl
};
#endif


static struct rt_device keys_device;
int rt_hw_keys_init(void)
{
    /* register device */
#ifdef RT_USING_DEVICE_OPS
	keys_device.ops = &keys_ops;
#else
    keys_device.type      = RT_Device_Class_Char;
    keys_device.init      = hw_keys_init;
    keys_device.open      = hw_keys_open;
    keys_device.close     = hw_keys_close;
    keys_device.read      = rt_hw_keys_read;
    keys_device.write     = RT_NULL;
    keys_device.control   = rt_hw_keys_ioctl;
    keys_device.user_data = RT_NULL;
#endif

    return rt_device_register(&keys_device, "keys", RT_DEVICE_FLAG_RDWR);
}
INIT_DEVICE_EXPORT(rt_hw_keys_init);

static int isopen = 0;
static void keys_enable(void)
{
		/* 查找系统中的pin设备*/
    rt_device_t key_device = rt_device_find("keys");
    if (key_device!= RT_NULL)
    {
        rt_device_open(key_device, RT_DEVICE_OFLAG_RDWR);
		isopen = 1;
	}
}
/* 导出到 msh 命令列表中 */
MSH_CMD_EXPORT(keys_enable, enable keys function);

static void keys_disable(void)
{
	/* 查找系统中的pin设备*/
    rt_device_t key_device = rt_device_find("keys");
    if (key_device != RT_NULL)
    {
    	if (isopen)
    	{
    		isopen = 0;
        	rt_device_close(key_device);
    	}
	}
}
/* 导出到 msh 命令列表中 */
MSH_CMD_EXPORT(keys_disable, disable keys function);


