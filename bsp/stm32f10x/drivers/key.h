#ifndef __KEY_H__
#define __KEY_H__

#include <rtthread.h>

typedef enum __KEYS_OPT_STATUS {
	IOCTL_READ_KEY0=0,
	IOCTL_READ_KEY1,
	IOCTL_READ_KEY2,
	IOCTL_READ_KEYUP,
}KEYS_OPT_STATUS;

#endif

