/*
 * Copyright (c) 2006-2018, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2009-01-05     Bernard      the first version
 */
#include <rtthread.h>
#include <rtdevice.h>
#include <stm32f10x.h>
#include "led.h"

#ifndef LED0_PIN_NUM
    #define LED0_PIN_NUM            135  /* PB5 */
#endif

#ifndef LED1_PIN_NUM
    #define LED1_PIN_NUM            4  	/* PE5 */
#endif

static rt_err_t hw_led_open(rt_device_t dev, rt_uint16_t oflag)
{
/*
	GPIO_InitTypeDef GPIO_InitStructure;
	
    RCC_APB2PeriphClockCmd(led0_rcc|led1_rcc,ENABLE);

    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;

    GPIO_InitStructure.GPIO_Pin   = led0_pin;
    GPIO_Init(led0_gpio, &GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin   = led1_pin;
    GPIO_Init(led1_gpio, &GPIO_InitStructure);
*/
	rt_pin_mode(LED0_PIN_NUM, PIN_MODE_OUTPUT);
	rt_pin_mode(LED1_PIN_NUM, PIN_MODE_OUTPUT);

    return RT_EOK;
}

rt_err_t  hw_led_close(rt_device_t dev)
{
	return RT_EOK;
}

static void rt_hw_led_on(rt_uint32_t n)
{
    switch (n)
    {
    case 0:
		rt_pin_write(LED0_PIN_NUM, Bit_RESET);
        //GPIO_ResetBits(led0_gpio, led0_pin);
        break;
    case 1:
		rt_pin_write(LED1_PIN_NUM, Bit_RESET);
        //GPIO_ResetBits(led1_gpio, led1_pin);
        break;
    default:
        break;
    }
}

static void rt_hw_led_off(rt_uint32_t n)
{
    switch (n)
    {
    case 0:
		rt_pin_write(LED0_PIN_NUM, Bit_SET);
        //GPIO_SetBits(led0_gpio, led0_pin);
        break;
    case 1:
		rt_pin_write(LED1_PIN_NUM, Bit_SET);
        //GPIO_SetBits(led1_gpio, led1_pin);
        break;
    default:
        break;
    }
}

static rt_err_t hw_leds_init(rt_device_t dev)
{
    return RT_EOK;
}

rt_size_t rt_hw_leds_read(rt_device_t dev, rt_off_t pos, void *buffer, rt_size_t size)
{	
	RT_ASSERT(dev != RT_NULL);
	RT_ASSERT(buffer != RT_NULL);
	RT_ASSERT(size != 0);
	
	char sta0 = 0;
	char sta1 = 0;
	char *ptr = buffer;
	rt_size_t ret = 0;
	
    switch(pos) {
        case READ_LED0_STATUS:
            sta0 = READ_LED0;
			if (size >= 1)
			{
				rt_memcpy(ptr, &sta0, 1);
				ret = 1;
			}
            break;
            
        case READ_LED1_STATUS:
            sta1 = READ_LED1;
			if (size >= 1)
			{
				rt_memcpy(ptr, &sta1, 1);
				ret = 1;
			}
            break;
            
        case READ_ALLLED_STATUS:
            sta0 = READ_LED0;
			sta1 = READ_LED1;
			if (size >= 2)
			{
				rt_memcpy(ptr++, &sta0, 1);
				rt_memcpy(ptr, &sta1, 1);
				ret = 2;
			}
            break;           

        default:break;
    }
	
	return ret;
}

rt_size_t rt_hw_leds_write(rt_device_t dev, rt_off_t pos, const void *buffer, rt_size_t size)
{
	RT_ASSERT(dev != RT_NULL);
	
	return 0;
}    

static rt_err_t rt_hw_leds_control(rt_device_t dev, int cmd, void *args)
{
    RT_ASSERT(dev != RT_NULL);

    switch(cmd) {
        case IOCTL_LED0_ON:
            rt_hw_led_on(0);
            break;
            
        case IOCTL_LED0_OFF:
            rt_hw_led_off(0);
            break;
            
        case IOCTL_LED1_ON:
            rt_hw_led_on(1);
            break;
            
        case IOCTL_LED1_OFF:
            rt_hw_led_off(1);
            break;
            
        case IOCTL_LED_ALLON:
            rt_hw_led_on(0);
            rt_hw_led_on(1);
            break;

        case IOCTL_LED_ALLOFF:
			rt_hw_led_off(0);
            rt_hw_led_off(1);
            break;

        default:break;
    }

    return RT_EOK;
}

#ifdef RT_USING_DEVICE_OPS
const static struct rt_device_ops leds_ops = 
{
    hw_leds_init,
    hw_led_open,
    hw_led_close,
    rt_hw_leds_read,
    rt_hw_leds_write,
    rt_hw_leds_control
};
#endif

static struct rt_device leds_device;
int rt_hw_leds_init(void)
{
#ifdef RT_USING_DEVICE_OPS
	leds_device.ops = &leds_ops;
#else
    /* register device */
    leds_device.type      = RT_Device_Class_Char;
    leds_device.init      = hw_leds_init;
    leds_device.open      = hw_led_open;
    leds_device.close     = hw_led_close;
    leds_device.read      = rt_hw_leds_read;
    leds_device.write     = rt_hw_leds_write;
    leds_device.control   = rt_hw_leds_control;
    leds_device.user_data = RT_NULL;
#endif

    return rt_device_register(&leds_device, "leds", RT_DEVICE_FLAG_RDWR);
}
INIT_DEVICE_EXPORT(rt_hw_leds_init);

#ifdef RT_USING_FINSH
#include <finsh.h>
void led(rt_uint32_t led, rt_uint32_t value)
{
    /* 查找系统中的leds设备*/
    rt_device_t led_device = rt_device_find("leds");
    if (led_device != RT_NULL)
    {
        rt_device_open(led_device, RT_DEVICE_OFLAG_RDWR);
	}
	
    if (led == 0)
    {
        /* set led status */
        switch (value)
        {
        case 0:
            rt_hw_led_off(0);
            break;
        case 1:
            rt_hw_led_on(0);
            break;
        default:
            break;
        }
    }

    if (led == 1)
    {
        /* set led status */
        switch (value)
        {
        case 0:
            rt_hw_led_off(1);
            break;
        case 1:
            rt_hw_led_on(1);
            break;
        default:
            break;
        }
    }
}
FINSH_FUNCTION_EXPORT(led, set led[0 - 1] on[1] or off[0].)

void msh_led(int argc, char** argv)
{	
	rt_uint32_t led;
	rt_uint32_t value;

	if (argc != 3)
    {
        rt_kprintf("Please input 'led <1|0> <on|off>' or 'led <1|0> <1|0>'\n");
        return;
    }
		
    /* 查找系统中的leds设备*/
    rt_device_t led_device = rt_device_find("leds");
    if (led_device != RT_NULL)
    {
        rt_device_open(led_device, RT_DEVICE_OFLAG_RDWR);
	}		

	if (!rt_strcmp(argv[1], "1"))
	{
       led = 1;
    }
	else if (!rt_strcmp(argv[1], "0"))
    {
        led = 0;
    }

	
	if (!rt_strcmp(argv[2], "on"))
    {
        value = 1;
    }
	else if (!rt_strcmp(argv[2], "1"))
	{
        value = 1;
    }
    else if (!rt_strcmp(argv[2], "off"))
    {
        value = 0;
    }	
	else if (!rt_strcmp(argv[2], "0"))
    {
        value = 0;
    }
    else
    {
        rt_kprintf("end Please input 'led <on|off>' or 'led <1|0>'\n");
    }

	if (led == 0)
    {
        /* set led status */
        switch (value)
        {
        case 0:
            rt_hw_led_off(0);
            break;
        case 1:
            rt_hw_led_on(0);
            break;
        default:
            break;
        }
    }

    if (led == 1)
    {
        /* set led status */
        switch (value)
        {
        case 0:
            rt_hw_led_off(1);
            break;
        case 1:
            rt_hw_led_on(1);
            break;
        default:
            break;
        }
    }
}
MSH_CMD_EXPORT_ALIAS(msh_led, led, set led on[1] or off[0]);
#endif

