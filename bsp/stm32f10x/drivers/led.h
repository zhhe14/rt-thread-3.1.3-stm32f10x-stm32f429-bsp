/*
 *
 * Copyright (c) 2006-2018, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2009-01-05     Bernard      the first version
 */

#ifndef __LED_H__
#define __LED_H__

#include <rtthread.h>

#define LEDON 	Bit_RESET
#define LEDOFF Bit_SET

#define led0_rcc                    RCC_APB2Periph_GPIOB
#define led0_gpio                   GPIOB
#define led0_pin                    (GPIO_Pin_5)

#define led1_rcc                    RCC_APB2Periph_GPIOE
#define led1_gpio                   GPIOE
#define led1_pin                    (GPIO_Pin_5)

#define READ_LED0  GPIO_ReadInputDataBit(led0_gpio,led0_pin)//��LED0
#define READ_LED1  GPIO_ReadInputDataBit(led1_gpio,led1_pin)//��LED1

typedef enum __LED_PIN {
	READ_LED0_STATUS=0,
	READ_LED1_STATUS,
	READ_ALLLED_STATUS,
}LED_PIN;

typedef enum __LED_CTL_STATUS {
	IOCTL_LED0_ON=0,
	IOCTL_LED0_OFF,
	IOCTL_LED1_ON,
	IOCTL_LED1_OFF,
	IOCTL_LED_ALLON,
	IOCTL_LED_ALLOFF,
}LED_CTL_STATUS;

#endif

