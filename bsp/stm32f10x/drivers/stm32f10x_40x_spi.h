#ifndef __STM32F10X_40X_SPI_H__
#define __STM32F10X_40X_SPI_H__

#include <rtthread.h>
#include <stm32f10x.h>
#include <drivers/spi.h>

struct stm32_spi_bus
{
    struct rt_spi_bus parent;
	
    SPI_TypeDef * SPI;
	
#ifdef SPI_USE_DMA
    DMA_Stream_TypeDef * DMA_Stream_TX;
    uint32_t DMA_Channel_TX;

    DMA_Stream_TypeDef * DMA_Stream_RX;
    uint32_t DMA_Channel_RX;

    uint32_t DMA_Channel_TX_FLAG_TC;
    uint32_t DMA_Channel_RX_FLAG_TC;
#endif /* #ifdef SPI_USE_DMA */
};

struct stm32_spi_cs
{
    GPIO_TypeDef * GPIOx;
    uint16_t GPIO_Pin;
};

#endif /* __STM32F10X_40X_SPI_H__ */


