#ifndef __KEY_H__
#define __KEY_H__

#include <rtthread.h>


/* 引脚编号，通过查看设备驱动文件drv_gpio.c确定 */
#ifndef KEY0_PIN_NUM
    #define KEY0_PIN_NUM		44  	/* PH3 */
#endif
#ifndef KEY1_PIN_NUM
    #define KEY1_PIN_NUM		43  	/* PH2 */
#endif
#ifndef KEY2_PIN_NUM
    #define KEY2_PIN_NUM		8  		/* PC13 */
#endif
#ifndef KEYUP_PIN_NUM
    #define KEYUP_PIN_NUM		40  	/* PA0 */
#endif

typedef enum __KEYS_OPT_STATUS {
	IOCTL_READ_KEYADD=0,
	IOCTL_READ_KEYSUB,
	IOCTL_READ_KEYOK,
	IOCTL_READ_KEYESC,
}KEYS_OPT_STATUS;

#endif

