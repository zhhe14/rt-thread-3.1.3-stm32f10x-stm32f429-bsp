/*
 *
 * Copyright (c) 2006-2018, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2009-01-05     Bernard      the first version
 */

#ifndef __DRV_LED_H__
#define __DRV_LED_H__

#include <rtthread.h>
#include <rtdevice.h>

/* 引脚编号，通过查看设备驱动文件drv_gpio.c确定 */
#ifndef LED0_PIN_NUM
    #define LED0_PIN_NUM		57  	/* PB1 */
#endif
#ifndef LED1_PIN_NUM
    #define LED1_PIN_NUM		56  	/* PB0 */
#endif

#define LEDON 	Bit_RESET
#define LEDOFF	Bit_SET

#define SET_LED0(x)		rt_pin_write(LED0_PIN_NUM, x) //GPIO_WriteBit(run_led_gpio,run_led_pin, (BitAction)x)
#define SET_LED1(x)		rt_pin_write(LED1_PIN_NUM, x) //GPIO_WriteBit(rled_gpio, rled_pin, (BitAction)x)

#define READ_LED0 		rt_pin_read(LED0_PIN_NUM) //GPIO_ReadInputDataBit(run_led_gpio, run_led_pin)
#define READ_LED1  		rt_pin_read(LED1_PIN_NUM) //GPIO_ReadInputDataBit(rled_gpio, rled_pin)

typedef struct LED_GROUP {
	unsigned char led0;
	unsigned char led1;
}LED_t;

typedef enum __LED_PIN {
	LED0=0,
	LED1,
	ALLLED_STATUS,
}LED_PIN;

typedef enum __LED_CTL_STATUS {
	IOCTL_LED0_ON=0,
	IOCTL_LED0_OFF,
	
	IOCTL_LED1_ON,
	IOCTL_LED1_OFF,
	
	IOCTL_LED_ALLON,
	IOCTL_LED_ALLOFF,
}LED_CTL_STATUS;

#endif

