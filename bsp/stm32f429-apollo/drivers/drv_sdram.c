/*
 * Copyright (c) 2006-2018, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2016-08-20     xuzhuoyi     The first version for STM32F42x
 * 2017-04-07     lizhen9880   Use SDRAM BANK1
 */

/* Includes ------------------------------------------------------------------*/
#include <rtdevice.h>

#include "stm32f4xx.h"
#include "drv_sdram.h"
#include "board.h"
#include "bsp_delay.h"

/**
  * @brief  Perform the SDRAM exernal memory inialization sequence
  * @param  hsdram: SDRAM handle
  * @param  Command: Pointer to SDRAM command structure
  * @retval None
  */
static void SDRAM_Initialization_Sequence(void)
{
	FMC_SDRAMCommandTypeDef FMC_SDRAMCommandStructure;
	uint32_t tmpr = 0;
	uint32_t timeout = SDRAM_TIMEOUT;
	
	/* Step 3 --------------------------------------------------------------------*/
	/* Configure a clock configuration enable command */
	FMC_SDRAMCommandStructure.FMC_CommandMode = FMC_Command_Mode_CLK_Enabled;
	FMC_SDRAMCommandStructure.FMC_CommandTarget = FMC_Command_Target_bank1;
	FMC_SDRAMCommandStructure.FMC_AutoRefreshNumber = 1;
	FMC_SDRAMCommandStructure.FMC_ModeRegisterDefinition = 0;
	/* Wait until the SDRAM controller is ready */
	while((FMC_GetFlagStatus(FMC_Bank1_SDRAM, FMC_FLAG_Busy) != RESET) && (timeout > 0))
	{
		timeout--;
	}
	/* Send the command */
	FMC_SDRAMCmdConfig(&FMC_SDRAMCommandStructure);
	
	
	/* Step 4 --------------------------------------------------------------------*/
	/* Insert 100 ms delay */
	bsp_DelayMS(100);

	
	/* Step 5 --------------------------------------------------------------------*/
	/* Configure a PALL (precharge all) command */
	FMC_SDRAMCommandStructure.FMC_CommandMode = FMC_Command_Mode_PALL;
	FMC_SDRAMCommandStructure.FMC_CommandTarget = FMC_Command_Target_bank1;
	FMC_SDRAMCommandStructure.FMC_AutoRefreshNumber = 1;
	FMC_SDRAMCommandStructure.FMC_ModeRegisterDefinition = 0;

	/* Wait until the SDRAM controller is ready */
	timeout = SDRAM_TIMEOUT;
	while((FMC_GetFlagStatus(FMC_Bank1_SDRAM, FMC_FLAG_Busy) != RESET) && (timeout > 0))
	{
		timeout--;
	}	
	/* Send the command */
	FMC_SDRAMCmdConfig(&FMC_SDRAMCommandStructure);
	
	
	/* Step 6 --------------------------------------------------------------------*/
	/* Configure a Auto-Refresh command */
	FMC_SDRAMCommandStructure.FMC_CommandMode = FMC_Command_Mode_AutoRefresh;
	FMC_SDRAMCommandStructure.FMC_CommandTarget = FMC_Command_Target_bank1;
	FMC_SDRAMCommandStructure.FMC_AutoRefreshNumber = 8;
	FMC_SDRAMCommandStructure.FMC_ModeRegisterDefinition = 0;

	/* Wait until the SDRAM controller is ready */
	timeout = SDRAM_TIMEOUT;
	while((FMC_GetFlagStatus(FMC_Bank1_SDRAM, FMC_FLAG_Busy) != RESET) && (timeout > 0))
	{
		timeout--;
	}
	/* Send the command */
	FMC_SDRAMCmdConfig(&FMC_SDRAMCommandStructure);

	/* Step 7 --------------------------------------------------------------------*/
	/* Program the external memory mode register */
	tmpr = (uint32_t)SDRAM_MODEREG_BURST_LENGTH_1	 |
			   SDRAM_MODEREG_BURST_TYPE_SEQUENTIAL	 |
			   SDRAM_MODEREG_CAS_LATENCY_3			 |
			   SDRAM_MODEREG_OPERATING_MODE_STANDARD |
			   SDRAM_MODEREG_WRITEBURST_MODE_SINGLE;

	/* Configure a load Mode register command*/
	FMC_SDRAMCommandStructure.FMC_CommandMode = FMC_Command_Mode_LoadMode;
	FMC_SDRAMCommandStructure.FMC_CommandTarget = FMC_Command_Target_bank1;
	FMC_SDRAMCommandStructure.FMC_AutoRefreshNumber = 1;
	FMC_SDRAMCommandStructure.FMC_ModeRegisterDefinition = tmpr;

	/* Wait until the SDRAM controller is ready */
	timeout = SDRAM_TIMEOUT;
	while((FMC_GetFlagStatus(FMC_Bank1_SDRAM, FMC_FLAG_Busy) != RESET) && (timeout > 0))
	{
		timeout--;
	}
	/* Send the command */
	FMC_SDRAMCmdConfig(&FMC_SDRAMCommandStructure);
	
	
	
	/* Step 8 --------------------------------------------------------------------*/

	/* Set the refresh rate counter */
	/* (15.62 us x Freq) - 20 */
	/* Set the device refresh counter */
	//刷新频率计数器(以SDCLK频率计数),计算方法:
	//COUNT=SDRAM刷新周期/行数-20=SDRAM刷新周期(us)*SDCLK频率(Mhz)/行数
	//我们使用的SDRAM刷新周期为64ms,SDCLK=180/2=90Mhz,行数为8192(2^13).
	//所以,COUNT=64*1000*90/8192-20=683
	FMC_SetRefreshCount(683);

	/* Wait until the SDRAM controller is ready */
	timeout = SDRAM_TIMEOUT;
	while((FMC_GetFlagStatus(FMC_Bank1_SDRAM, FMC_FLAG_Busy) != RESET) && (timeout > 0))
	{
		timeout--;
	}	
}

static void SDRAM_gpioConfig(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC | RCC_AHB1Periph_GPIOD | RCC_AHB1Periph_GPIOE |
                     RCC_AHB1Periph_GPIOF | RCC_AHB1Periph_GPIOG , ENABLE);
/*
	FMC_DQ0<-->DQ0<-->PD14		FMC_A0<-->A0<-->PF0     	FMC_SDNWE<-->/WE<-->PC0
	FMC_DQ1<-->DQ1<-->PD15		FMC_A1<-->A1<-->PF1			FMC_SDNCAS<-->/CAS<-->PG15
	FMC_DQ2<-->DQ2<-->PD0		FMC_A2<-->A2<-->PF2			FMC_SDNRAS<-->/RAS<-->PF11
	FMC_DQ3<-->DQ3<-->PD1		FMC_A3<-->A3<-->PF3			FMC_SDNE0<-->/CS<-->PC2
	FMC_DQ4<-->DQ4<-->PE7		FMC_A4<-->A4<-->PF4
	FMC_DQ5<-->DQ5<-->PE8		FMC_A5<-->A5<-->PF5			FMC_SDCKE0<-->CKE<-->PC3
	FMC_DQ6<-->DQ6<-->PE9		FMC_A6<-->A6<-->PF12		FMC_SCLK<-->CLK<-->PG8
	FMC_DQ7<-->DQ7<-->PE10		FMC_A7<-->A7<-->PF13								
	FMC_DQ8<-->DQ8<-->PE11		FMC_A8<-->A8<-->PF14
	FMC_DQ9<-->DQ9<-->PE12		FMC_A9<-->A9<-->PF15		FMC_NBL0<-->LDQM<-->PE0
	FMC_DQ10<-->DQ10<-->PE13	FMC_A10<-->A10<-->PG0		FMC_NBL1<-->UDQM<-->PE1
	FMC_DQ11<-->DQ11<-->PE14	FMC_A11<-->A11<-->PG1
	FMC_DQ12<-->DQ12<-->PE15	FMC_A14_BA0<-->BA0<-->PG4
	FMC_DQ13<-->DQ13<-->PD8		FMC_A15_BA1<-->BA1<-->PG5
	FMC_DQ14<-->DQ14<-->PD9								
	FMC_DQ15<-->DQ15<-->PD10								
*/

	//GPIOC  0-2-3
	GPIO_PinAFConfig(GPIOC,GPIO_PinSource0,GPIO_AF_FMC);
	GPIO_PinAFConfig(GPIOC,GPIO_PinSource2,GPIO_AF_FMC);
	GPIO_PinAFConfig(GPIOC,GPIO_PinSource3,GPIO_AF_FMC);

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0|GPIO_Pin_2|GPIO_Pin_3;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOC,&GPIO_InitStructure);	

	//GPIOD 0-1-8-9-10-14-15
	GPIO_PinAFConfig(GPIOD,GPIO_PinSource0,GPIO_AF_FMC);
	GPIO_PinAFConfig(GPIOD,GPIO_PinSource1,GPIO_AF_FMC);
	GPIO_PinAFConfig(GPIOD,GPIO_PinSource8,GPIO_AF_FMC);
	GPIO_PinAFConfig(GPIOD,GPIO_PinSource9,GPIO_AF_FMC);
	GPIO_PinAFConfig(GPIOD,GPIO_PinSource10,GPIO_AF_FMC);
	GPIO_PinAFConfig(GPIOD,GPIO_PinSource14,GPIO_AF_FMC);
	GPIO_PinAFConfig(GPIOD,GPIO_PinSource15,GPIO_AF_FMC);	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_8 | GPIO_Pin_9 |\
															GPIO_Pin_10 | GPIO_Pin_14 | GPIO_Pin_15;
	GPIO_Init(GPIOD,&GPIO_InitStructure);

	//GPIOE 0-1-7-8-9-10-11-12-13-14-15
	GPIO_PinAFConfig(GPIOE,GPIO_PinSource0,GPIO_AF_FMC);
	GPIO_PinAFConfig(GPIOE,GPIO_PinSource1,GPIO_AF_FMC);
	GPIO_PinAFConfig(GPIOE,GPIO_PinSource7,GPIO_AF_FMC);
	GPIO_PinAFConfig(GPIOE,GPIO_PinSource8,GPIO_AF_FMC);
	GPIO_PinAFConfig(GPIOE,GPIO_PinSource9,GPIO_AF_FMC);
	GPIO_PinAFConfig(GPIOE,GPIO_PinSource10,GPIO_AF_FMC);
	GPIO_PinAFConfig(GPIOE,GPIO_PinSource11,GPIO_AF_FMC);
	GPIO_PinAFConfig(GPIOE,GPIO_PinSource12,GPIO_AF_FMC);
	GPIO_PinAFConfig(GPIOE,GPIO_PinSource13,GPIO_AF_FMC);
	GPIO_PinAFConfig(GPIOE,GPIO_PinSource14,GPIO_AF_FMC);
	GPIO_PinAFConfig(GPIOE,GPIO_PinSource15,GPIO_AF_FMC);
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_7 | GPIO_Pin_8| GPIO_Pin_9 |\
															GPIO_Pin_10 | GPIO_Pin_11| GPIO_Pin_12| GPIO_Pin_13| GPIO_Pin_14 | GPIO_Pin_15;
	GPIO_Init(GPIOE,&GPIO_InitStructure);

	//GPIOF 0-1-2-3-4-5-11-12-13-14-15
	GPIO_PinAFConfig(GPIOF,GPIO_PinSource0,GPIO_AF_FMC);
	GPIO_PinAFConfig(GPIOF,GPIO_PinSource1,GPIO_AF_FMC);
	GPIO_PinAFConfig(GPIOF,GPIO_PinSource2,GPIO_AF_FMC);
	GPIO_PinAFConfig(GPIOF,GPIO_PinSource3,GPIO_AF_FMC);
	GPIO_PinAFConfig(GPIOF,GPIO_PinSource4,GPIO_AF_FMC);
	GPIO_PinAFConfig(GPIOF,GPIO_PinSource5,GPIO_AF_FMC);
	GPIO_PinAFConfig(GPIOF,GPIO_PinSource11,GPIO_AF_FMC);
	GPIO_PinAFConfig(GPIOF,GPIO_PinSource12,GPIO_AF_FMC);
	GPIO_PinAFConfig(GPIOF,GPIO_PinSource13,GPIO_AF_FMC);
	GPIO_PinAFConfig(GPIOF,GPIO_PinSource14,GPIO_AF_FMC);
	GPIO_PinAFConfig(GPIOF,GPIO_PinSource15,GPIO_AF_FMC);
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3| GPIO_Pin_4 |\
															GPIO_Pin_5 | GPIO_Pin_11| GPIO_Pin_12| GPIO_Pin_13| GPIO_Pin_14 | GPIO_Pin_15;
	GPIO_Init(GPIOF,&GPIO_InitStructure);


	//GPIOG 0-1-4-5-8-15
	GPIO_PinAFConfig(GPIOG,GPIO_PinSource0,GPIO_AF_FMC);
	GPIO_PinAFConfig(GPIOG,GPIO_PinSource1,GPIO_AF_FMC);
	GPIO_PinAFConfig(GPIOG,GPIO_PinSource4,GPIO_AF_FMC);
	GPIO_PinAFConfig(GPIOG,GPIO_PinSource5,GPIO_AF_FMC);
	GPIO_PinAFConfig(GPIOG,GPIO_PinSource8,GPIO_AF_FMC);
	GPIO_PinAFConfig(GPIOG,GPIO_PinSource15,GPIO_AF_FMC);
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_4| GPIO_Pin_5 | \
										GPIO_Pin_8 | GPIO_Pin_15;
	GPIO_Init(GPIOG,&GPIO_InitStructure);
}


/**
  * @brief  Configures the FMC and GPIOs to interface with the SDRAM memory.
  *         This function must be called before any read/write operation
  *         on the SDRAM.
  * @param  None
  * @retval None
  */
void SDRAM_Init(void)
{
	FMC_SDRAMInitTypeDef  FMC_SDRAMInitStructure;
	FMC_SDRAMTimingInitTypeDef	FMC_SDRAMTimingInitStructure;
	
	/* Enable FMC clock */
	RCC_AHB3PeriphClockCmd(RCC_AHB3Periph_FMC, ENABLE); 
	SDRAM_gpioConfig();

	/* FMC Configuration ---------------------------------------------------------*/
	/* FMC SDRAM Bank configuration */
	/* Timing configuration for 90 Mhz of SD clock frequency (180Mhz/2)  11.11ns
	   Timing configuration for 84 Mhz of SD clock frequency (168Mhz/2)  11.90ns
	*/
	/* TMRD: 2 Clock cycles */
	FMC_SDRAMTimingInitStructure.FMC_LoadToActiveDelay	  = 2;  //加载模式寄存器到激活时间的延迟为2个时钟周期
	/* TXSR: min=70ns (7x11.11ns or 11.90) */
	FMC_SDRAMTimingInitStructure.FMC_ExitSelfRefreshDelay = 8;  //退出自刷新延迟为8个时钟周期
	/* TRAS: min=42ns (4x11.11ns or 11.90) max=120k (ns) */
	FMC_SDRAMTimingInitStructure.FMC_SelfRefreshTime	  = 6;  //自刷新时间为6个时钟周期
	/* TRC:  min=70 (7x11.11ns or 11.90) */
	FMC_SDRAMTimingInitStructure.FMC_RowCycleDelay		  = 6;  //行循环延迟为6个时钟周期
	/* TWR:  min=1+ 7ns (1+1x11.11ns or 11.90) 2修改为1 ---------*/
	FMC_SDRAMTimingInitStructure.FMC_WriteRecoveryTime	  = 2;  //恢复延迟为2个时钟周期
	/* TRP:  min=20ns  2x11.11ns or 11.90 */
	FMC_SDRAMTimingInitStructure.FMC_RPDelay			  = 2;  //行预充电延迟为2个时钟周期
	/* TRCD: min=20ns  2x11.11ns or 11.90 */
	FMC_SDRAMTimingInitStructure.FMC_RCDDelay			  = 2;  //行到列延迟为2个时钟周期


	/* FMC SDRAM control configuration */
	FMC_SDRAMInitStructure.FMC_Bank 			  = FMC_Bank1_SDRAM;    //SDRAM接在BANK5上
	/* Row addressing: [7:0] */
	FMC_SDRAMInitStructure.FMC_ColumnBitsNumber   = FMC_ColumnBits_Number_9b;   //列数量
	/* Column addressing: [10:0] --> [11:0], _12b = 16M,  _11b = 8M */
	FMC_SDRAMInitStructure.FMC_RowBitsNumber	  = FMC_RowBits_Number_13b;     //行数量	
	FMC_SDRAMInitStructure.FMC_SDMemoryDataWidth  = FMC_SDMemory_Width_16b;     //数据宽度为16位
	FMC_SDRAMInitStructure.FMC_InternalBankNumber = FMC_InternalBank_Number_4;  //一共4个BANK
	/* CL: Cas Latency = 3 clock cycles 3修改为2 --------------*/
	FMC_SDRAMInitStructure.FMC_CASLatency		  = FMC_CAS_Latency_3;			//CAS为3
	FMC_SDRAMInitStructure.FMC_WriteProtection	  = FMC_Write_Protection_Disable;   //失能写保护
	FMC_SDRAMInitStructure.FMC_SDClockPeriod	  = FMC_SDClock_Period_2;   //SDRAM时钟为HCLK/2=180M/2=90M=11.1ns
	FMC_SDRAMInitStructure.FMC_ReadBurst		  = FMC_Read_Burst_Enable;  //使能突发
	/* CL: Cas Latency = 3 clock cycles 1修改为0----------------- */
	FMC_SDRAMInitStructure.FMC_ReadPipeDelay	  = FMC_ReadPipe_Delay_1;   //读通道延时
	FMC_SDRAMInitStructure.FMC_SDRAMTimingStruct  = &FMC_SDRAMTimingInitStructure;

	/* FMC SDRAM bank initialization */
	FMC_SDRAMInit(&FMC_SDRAMInitStructure);

	SDRAM_Initialization_Sequence();
}

rt_err_t sdram_hw_init(void)
{
  SDRAM_Init();
  return RT_EOK;
}

static int rt_sdram_hw_init(void)
{
    return (int)sdram_hw_init();
    
}
INIT_BOARD_EXPORT(rt_sdram_hw_init);
