/*
 * Copyright (c) 2006-2018, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2009-01-05     Bernard      the first version
 * 2010-03-29     Bernard      remove interrupt Tx and DMA Rx mode
 * 2012-02-08     aozima       update for F4.
 * 2012-07-28     aozima       update for ART board.
 * 2016-05-28     armink       add DMA Rx mode
 */

#include "stm32f4xx.h"
#include "usart.h"
#include "board.h"
#include <stdio.h>

#include <rtdevice.h>

#ifdef RT_USING_UART1
#define USART1_RX_GPIO_CLK_ENABLE()      RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE)
#define USART1_TX_GPIO_CLK_ENABLE()      RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE)

/* Definition for USART1 Pins */
#define USART1_TX_PIN                    GPIO_Pin_9
#define USART1_TX_PinSource				 GPIO_PinSource9
#define USART1_TX_GPIO_PORT              GPIOA
#define USART1_TX_AF                     GPIO_AF_USART1

#define USART1_RX_PIN                    GPIO_Pin_10
#define USART1_RX_PinSource				 GPIO_PinSource10
#define USART1_RX_GPIO_PORT              GPIOA
#define USART1_RX_AF                     GPIO_AF_USART1
#endif

#ifdef RT_USING_UART2
#define USART2_RX_GPIO_CLK_ENABLE()      RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE)
#define USART2_TX_GPIO_CLK_ENABLE()      RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE)

/* Definition for USART2 Pins */
#define USART2_TX_PIN                    GPIO_Pin_2
#define USART2_TX_PinSource				 GPIO_PinSource2
#define USART2_TX_GPIO_PORT              GPIOA
#define USART2_TX_AF                     GPIO_AF_USART2

#define USART2_RX_PIN                    GPIO_Pin_3
#define USART2_RX_PinSource				 GPIO_PinSource3
#define USART2_RX_GPIO_PORT              GPIOA
#define USART2_RX_AF                     GPIO_AF_USART2
#endif

#ifdef RT_USING_UART3
#define USART3_RX_GPIO_CLK_ENABLE()      RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE)
#define USART3_TX_GPIO_CLK_ENABLE()      RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE)

/* Definition for USART3 Pins */
#define USART3_TX_PIN                    GPIO_Pin_10
#define USART3_TX_PinSource				 GPIO_PinSource10
#define USART3_TX_GPIO_PORT              GPIOB
#define USART3_TX_AF                     GPIO_AF_USART3

#define USART3_RX_PIN                    GPIO_Pin_11
#define USART3_RX_PinSource				 GPIO_PinSource11
#define USART3_RX_GPIO_PORT              GPIOB
#define USART3_RX_AF                     GPIO_AF_USART3
#endif

#ifdef RT_USING_UART4
#define UART4_RX_GPIO_CLK_ENABLE()      RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE)
#define UART4_TX_GPIO_CLK_ENABLE()      RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE)

/* Definition for USART4 Pins */
#define UART4_TX_PIN                    GPIO_Pin_0
#define UART4_TX_PinSource				GPIO_PinSource0
#define UART4_TX_GPIO_PORT              GPIOA
#define UART4_TX_AF                     GPIO_AF_UART4

#define UART4_RX_PIN                    GPIO_Pin_1
#define UART4_RX_PinSource				GPIO_PinSource1
#define UART4_RX_GPIO_PORT              GPIOA
#define UART4_RX_AF                     GPIO_AF_UART4
#endif

#ifdef RT_USING_UART5
#define UART5_RX_GPIO_CLK_ENABLE()      RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE)
#define UART5_TX_GPIO_CLK_ENABLE()      RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE)

/* Definition for USART5 Pins */
#define UART5_TX_PIN                    GPIO_Pin_12
#define UART5_TX_PinSource				GPIO_PinSource12
#define UART5_TX_GPIO_PORT              GPIOC
#define UART5_TX_AF                     GPIO_AF_UART5

#define UART5_RX_PIN                    GPIO_Pin_2
#define UART5_RX_PinSource				GPIO_PinSource2
#define UART5_RX_GPIO_PORT              GPIOD
#define UART5_RX_AF                     GPIO_AF_UART5
#endif

#ifdef RT_USING_UART6
#define USART6_RX_GPIO_CLK_ENABLE()      RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOG, ENABLE)
#define USART6_TX_GPIO_CLK_ENABLE()      RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOG, ENABLE)

/* Definition for USART6 Pins */
#define USART6_TX_PIN                    GPIO_Pin_14
#define USART6_TX_PinSource				 GPIO_PinSource14
#define USART6_TX_GPIO_PORT              GPIOG
#define USART6_TX_AF                     GPIO_AF_USART6

#define USART6_RX_PIN                    GPIO_Pin_9
#define USART6_RX_PinSource				 GPIO_PinSource9
#define USART6_RX_GPIO_PORT              GPIOG
#define USART6_RX_AF                     GPIO_AF_USART6
#endif

#ifdef RT_USING_UART7
#define UART7_RX_GPIO_CLK_ENABLE()      RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOF, ENABLE)
#define UART7_TX_GPIO_CLK_ENABLE()      RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOF, ENABLE)

/* Definition for USART7 Pins */
#define UART7_TX_PIN                    GPIO_Pin_7
#define UART7_TX_PinSource				GPIO_PinSource7
#define UART7_TX_GPIO_PORT              GPIOF
#define UART7_TX_AF                     GPIO_AF_UART7

#define UART7_RX_PIN                    GPIO_Pin_6
#define UART7_RX_PinSource				GPIO_PinSource6
#define UART7_RX_GPIO_PORT              GPIOF
#define UART7_RX_AF                     GPIO_AF_UART7
#endif

/* STM32 uart driver */
struct stm32_uart
{
	USART_TypeDef * uart_device;
    IRQn_Type irq;
};

static rt_err_t stm32_configure(struct rt_serial_device *serial, struct serial_configure *cfg)
{
	struct stm32_uart *uart;
	USART_InitTypeDef USART_InitStructure;


	RT_ASSERT(serial != RT_NULL);
	RT_ASSERT(cfg != RT_NULL);

	uart = (struct stm32_uart *)serial->parent.user_data;

	USART_InitStructure.USART_BaudRate = cfg->baud_rate;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

  	//uart->UartHandle.Init.OverSampling = UART_OVERSAMPLING_16;

	switch (cfg->data_bits)
	{
	case DATA_BITS_8:
		USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	  break;
	case DATA_BITS_9:
		USART_InitStructure.USART_WordLength = USART_WordLength_9b;
	  break;
	default:
		USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	  break;
	}
	switch (cfg->stop_bits)
	{
	case STOP_BITS_1:
		USART_InitStructure.USART_StopBits = USART_StopBits_1;
	  break;
	case STOP_BITS_2:
		USART_InitStructure.USART_StopBits   = USART_StopBits_2;
	  break;
	default:
		USART_InitStructure.USART_StopBits   = USART_StopBits_1;
	  break;
	}
	switch (cfg->parity)
	{
	case PARITY_NONE:
		USART_InitStructure.USART_Parity	 = USART_Parity_No;
	  break;
	case PARITY_ODD:
		USART_InitStructure.USART_Parity     = USART_Parity_Odd;
	  break;
	case PARITY_EVEN:
	  	USART_InitStructure.USART_Parity     = USART_Parity_Even;
	  break;
	default:
		USART_InitStructure.USART_Parity     = USART_Parity_No;
	  break;
	}
	
    USART_Init(uart->uart_device, &USART_InitStructure);

    /* Enable USART */
    USART_Cmd(uart->uart_device, ENABLE);
    /* enable interrupt */
    USART_ITConfig(uart->uart_device, USART_IT_RXNE, ENABLE);
    USART_ClearFlag(uart->uart_device, USART_FLAG_TC);

	return RT_EOK;
}

struct intt_uart
{
    rt_uint16_t open_flag;
};

static rt_err_t stm32_control(struct rt_serial_device *serial, int cmd, void *arg)
{
	struct stm32_uart *uart;

	RT_ASSERT(serial != RT_NULL);
	uart = (struct stm32_uart *)serial->parent.user_data;
	
    /* get open flags */
    struct intt_uart *int_uart;
    int_uart = (struct intt_uart *)&arg;

	switch (cmd)
	{
	case RT_DEVICE_CTRL_CLR_INT:
		/* disable rx irq */
		NVIC_DisableIRQ(uart->irq);
		/* disable interrupt */
	    if (int_uart->open_flag & RT_DEVICE_FLAG_DMA_RX){
	        
	    }else if (int_uart->open_flag & RT_DEVICE_FLAG_INT_RX){
	        USART_ITConfig(uart->uart_device, USART_IT_RXNE, DISABLE);
	    }else if (int_uart->open_flag & RT_DEVICE_FLAG_DMA_TX){
	        
	    } else {
	    
	    }
		break;
	case RT_DEVICE_CTRL_SET_INT:
		/* enable rx irq */
		NVIC_EnableIRQ(uart->irq);
		/* enable interrupt */
        if (int_uart->open_flag & RT_DEVICE_FLAG_DMA_RX){
            
        }else if (int_uart->open_flag & RT_DEVICE_FLAG_INT_RX){
            USART_ITConfig(uart->uart_device, USART_IT_RXNE, ENABLE);
        }else if (int_uart->open_flag & RT_DEVICE_FLAG_DMA_TX){
            
        } else {
        
        }
		break;
	}

  return RT_EOK;
}

static int stm32_putc(struct rt_serial_device *serial, char c)
{
    struct stm32_uart *uart;

    RT_ASSERT(serial != RT_NULL);
    uart = (struct stm32_uart *)serial->parent.user_data;

    uart->uart_device->DR = c;
    while (!(uart->uart_device->SR & USART_FLAG_TC));

    return 1;
}

static int stm32_getc(struct rt_serial_device *serial)
{
	int ch;
	struct stm32_uart *uart;

	RT_ASSERT(serial != RT_NULL);
	uart = (struct stm32_uart *)serial->parent.user_data;

	ch = -1;
	if (uart->uart_device->SR & USART_FLAG_RXNE)
	{
		ch = uart->uart_device->DR & 0xff;
	}

	return ch;
}

/**
 * Uart common interrupt process. This need add to uart ISR.
 *
 * @param serial serial device
 */
static void uart_isr(struct rt_serial_device *serial) {
    struct stm32_uart *uart = (struct stm32_uart *) serial->parent.user_data;

    RT_ASSERT(uart != RT_NULL);

    /* UART in mode Receiver -------------------------------------------------*/
    if(USART_GetITStatus(uart->uart_device, USART_IT_RXNE) != RESET)
    {
        rt_hw_serial_isr(serial, RT_SERIAL_EVENT_RX_IND);
		
        /* clear interrupt */
        USART_ClearITPendingBit(uart->uart_device, USART_IT_RXNE);
    }
    if ( USART_GetITStatus(uart->uart_device, USART_IT_TXE) != RESET )
    {
        rt_hw_serial_isr(serial, RT_SERIAL_EVENT_TX_DONE);
		
		/* clear interrupt */
        USART_ClearITPendingBit(uart->uart_device, USART_IT_TXE);
    }
    if(USART_GetITStatus(uart->uart_device, USART_IT_IDLE) != RESET)
    {
        rt_hw_serial_isr(serial,RT_SERIAL_EVENT_RX_DMADONE);
        USART_ReceiveData(uart->uart_device);

		/* clear interrupt */
        USART_ClearITPendingBit(uart->uart_device, USART_IT_IDLE);
    }
    if (USART_GetITStatus(uart->uart_device, USART_IT_TC) != RESET)
    {
        /* clear interrupt */
        USART_ClearITPendingBit(uart->uart_device, USART_IT_TC);
    }
    if (USART_GetFlagStatus(uart->uart_device, USART_FLAG_ORE) == SET)
    {
        stm32_getc(serial);
    }
}

static const struct rt_uart_ops stm32_uart_ops =
{
    stm32_configure,
    stm32_control,
    stm32_putc,
    stm32_getc,
};

#if defined(RT_USING_UART1)
/* UART1 device driver structure */
struct stm32_uart uart1 =
{
	USART1,
    USART1_IRQn,
};
struct rt_serial_device serial1;

void USART1_IRQHandler(void)
{
    /* enter interrupt */
    rt_interrupt_enter();

    uart_isr(&serial1);

    /* leave interrupt */
    rt_interrupt_leave();
}

#endif /* RT_USING_UART1 */

#if defined(RT_USING_UART2)
/* UART2 device driver structure */
struct stm32_uart uart2 =
{
    USART2,
    USART2_IRQn,
};
struct rt_serial_device serial2;

void USART2_IRQHandler(void)
{
    /* enter interrupt */
    rt_interrupt_enter();

    uart_isr(&serial2);

    /* leave interrupt */
    rt_interrupt_leave();
}

#endif /* RT_USING_UART2 */

#if defined(RT_USING_UART3)
/* UART3 device driver structure */
struct stm32_uart uart3 =
{
    USART3,
    USART3_IRQn,
};
struct rt_serial_device serial3;

void USART3_IRQHandler(void)
{
    /* enter interrupt */
    rt_interrupt_enter();

    uart_isr(&serial3);

    /* leave interrupt */
    rt_interrupt_leave();
}

#endif /* RT_USING_UART3 */

#if defined(RT_USING_UART4)
/* UART4 device driver structure */
struct stm32_uart uart4 =
{
    UART4,
    UART4_IRQn,
};
struct rt_serial_device serial4;

void UART4_IRQHandler(void)
{
    /* enter interrupt */
    rt_interrupt_enter();

    uart_isr(&serial4);

    /* leave interrupt */
    rt_interrupt_leave();
}
#endif /* RT_USING_UART4 */

#if defined(RT_USING_UART5)
/* UART5 device driver structure */
struct stm32_uart uart5 =
{
    UART5,
    UART5_IRQn,
};
struct rt_serial_device serial5;

void UART5_IRQHandler(void)
{
    /* enter interrupt */
    rt_interrupt_enter();

    uart_isr(&serial5);

    /* leave interrupt */
    rt_interrupt_leave();
}

#endif /* RT_USING_UART5 */

#if defined(RT_USING_UART6)
/* UART6 device driver structure */
struct stm32_uart uart6 =
{
    USART6,
    USART6_IRQn,
};
struct rt_serial_device serial6;

void USART6_IRQHandler(void)
{
    /* enter interrupt */
    rt_interrupt_enter();

    uart_isr(&serial6);

    /* leave interrupt */
    rt_interrupt_leave();
}

#endif /* RT_USING_UART6 */

#if defined(RT_USING_UART7)
/* UART7 device driver structure */
struct stm32_uart uart7 =
{
    UART7,
    UART7_IRQn,
};
struct rt_serial_device serial7;

void UART7_IRQHandler(void)
{
    /* enter interrupt */
    rt_interrupt_enter();

    uart_isr(&serial7);

    /* leave interrupt */
    rt_interrupt_leave();
}

#endif /* RT_USING_UART7 */

void uart_bsp_init()
{
	GPIO_InitTypeDef  GPIO_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

#ifdef RT_USING_UART1
	/* Enable GPIO clocks */
	USART1_RX_GPIO_CLK_ENABLE();
	USART1_TX_GPIO_CLK_ENABLE();
	
	/* Enable USART1 clock */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1,ENABLE);
	
	GPIO_PinAFConfig(USART1_RX_GPIO_PORT,USART1_RX_PinSource,USART1_RX_AF);
	GPIO_PinAFConfig(USART1_TX_GPIO_PORT,USART1_TX_PinSource,USART1_TX_AF);
	
	/* Configure USART1 Rx (PA.10)*/
	GPIO_InitStructure.GPIO_Pin = USART1_RX_PIN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(USART1_RX_GPIO_PORT,&GPIO_InitStructure);
	
	/* Configure USART1 Tx (PA.09) as alternate function push-pull */
	GPIO_InitStructure.GPIO_Pin = USART1_TX_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_Init(USART1_TX_GPIO_PORT,&GPIO_InitStructure);

	/* Enable the USART1 Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
#endif

#ifdef RT_USING_UART2
	/* Enable GPIO clocks */
	USART2_RX_GPIO_CLK_ENABLE();
	USART2_TX_GPIO_CLK_ENABLE();
	
	/* Enable USART2 clock */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);

	GPIO_PinAFConfig(USART2_RX_GPIO_PORT,USART2_RX_PinSource,USART2_RX_AF);
	GPIO_PinAFConfig(USART2_TX_GPIO_PORT,USART2_TX_PinSource,USART2_TX_AF);	

	/* Configure USART2 Rx*/
	GPIO_InitStructure.GPIO_Pin = USART2_RX_PIN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(USART2_RX_GPIO_PORT,&GPIO_InitStructure);

	/* Configure USART2 Tx as alternate function push-pull */
	GPIO_InitStructure.GPIO_Pin = USART2_TX_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_Init(USART2_TX_GPIO_PORT,&GPIO_InitStructure);

	/* Enable the USART2 Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

#ifdef RT_USING_UART2_DMA  
	/* Enable the DMA1 Channel7 Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = DMA1_Stream7_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
#endif
#endif

#ifdef RT_USING_UART3
	/* Enable GPIO clocks */
	USART3_RX_GPIO_CLK_ENABLE();
	USART3_TX_GPIO_CLK_ENABLE();
	
	/* Enable USART3 clock */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);

	GPIO_PinAFConfig(USART3_RX_GPIO_PORT,USART3_RX_PinSource,USART3_RX_AF);
	GPIO_PinAFConfig(USART3_TX_GPIO_PORT,USART3_TX_PinSource,USART3_TX_AF);	

	/* Configure USART3 Rx as input floating */
	GPIO_InitStructure.GPIO_Pin = USART3_RX_PIN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(USART3_RX_GPIO_PORT,&GPIO_InitStructure);

	/* Configure USART3 Tx as alternate function push-pull */
	GPIO_InitStructure.GPIO_Pin = USART3_TX_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_Init(USART3_TX_GPIO_PORT,&GPIO_InitStructure);

	/* Enable the USART3 Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = USART3_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

#ifdef RT_USING_UART3_DMA
	/* Enable the DMA1 Channel2 Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = DMA1_Stream2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
#endif
#endif /* RT_USING_UART3 */

#ifdef RT_USING_UART4
    /* Enable UART GPIO clocks */   
	UART4_RX_GPIO_CLK_ENABLE();
	UART4_TX_GPIO_CLK_ENABLE();
	
    /* Enable UART4 clock */
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART4, ENABLE);

	GPIO_PinAFConfig(UART4_RX_GPIO_PORT,UART4_RX_PinSource,UART4_RX_AF);
	GPIO_PinAFConfig(UART4_TX_GPIO_PORT,UART4_TX_PinSource,UART4_TX_AF);

    /* Configure USART Rx/tx PIN */
	GPIO_InitStructure.GPIO_Pin = UART4_RX_PIN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(UART4_RX_GPIO_PORT,&GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = UART4_TX_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_Init(UART4_TX_GPIO_PORT, &GPIO_InitStructure);

	/* Enable the UART4 Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = UART4_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 4;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
    
#ifdef RT_USING_UART4_DMA
	/* Enable the DMA2 Channel5 Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = DMA2_Stream5_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 4;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
#endif
#endif /* RT_USING_UART4 */

#ifdef RT_USING_UART5
    /* Enable UART GPIO clocks */
    UART5_RX_GPIO_CLK_ENABLE();
	UART5_TX_GPIO_CLK_ENABLE();
	
    /* Enable UART5 clock */
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART5, ENABLE);

	GPIO_PinAFConfig(UART5_RX_GPIO_PORT,UART5_RX_PinSource,UART5_RX_AF);
	GPIO_PinAFConfig(UART5_TX_GPIO_PORT,UART5_TX_PinSource,UART5_TX_AF);

    /* Configure USART Rx/tx PIN */
	GPIO_InitStructure.GPIO_Pin = UART5_RX_PIN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(UART5_RX_GPIO_PORT,&GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = UART5_TX_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_Init(UART5_TX_GPIO_PORT, &GPIO_InitStructure);

	/* Enable the UART5 Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = UART5_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 5;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
#endif /* RT_USING_UART5 */

#ifdef RT_USING_UART6
	/* Enable UART GPIO clocks */
	USART6_RX_GPIO_CLK_ENABLE();
	USART6_TX_GPIO_CLK_ENABLE();
	
	/* Enable USART6 clock */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART6, ENABLE);

	GPIO_PinAFConfig(USART6_RX_GPIO_PORT,USART6_RX_PinSource,USART6_RX_AF);
	GPIO_PinAFConfig(USART6_TX_GPIO_PORT,USART6_TX_PinSource,USART6_TX_AF);

	/* Configure USART6 Rx as input floating */
	GPIO_InitStructure.GPIO_Pin = USART6_RX_PIN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(USART6_RX_GPIO_PORT,&GPIO_InitStructure);

	/* Configure USART6 Tx as alternate function push-pull */
	GPIO_InitStructure.GPIO_Pin = USART6_TX_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_Init(USART6_TX_GPIO_PORT,&GPIO_InitStructure);

	/* Enable the USART6 Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = USART6_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 6;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
#endif /* RT_USING_UART6 */

#ifdef RT_USING_UART7
	/* Enable UART GPIO clocks */
	UART7_RX_GPIO_CLK_ENABLE();
	UART7_TX_GPIO_CLK_ENABLE();
	
	/* Enable UART7 clock */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART7, ENABLE);

	GPIO_PinAFConfig(UART7_RX_GPIO_PORT,UART7_RX_PinSource,UART7_RX_AF);
	GPIO_PinAFConfig(UART7_TX_GPIO_PORT,UART7_TX_PinSource,UART7_TX_AF);

    /* Configure USART Rx/tx PIN */
	GPIO_InitStructure.GPIO_Pin = UART7_RX_PIN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(UART7_RX_GPIO_PORT,&GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = UART7_TX_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_Init(UART7_TX_GPIO_PORT, &GPIO_InitStructure);

	/* Enable the UART7 Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = UART7_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 7;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
#endif /* RT_USING_UART7 */
}


int stm32_hw_usart_init(void)
{
    struct stm32_uart *uart;
    struct serial_configure config = RT_SERIAL_CONFIG_DEFAULT;

	uart_bsp_init();
	
#ifdef RT_USING_UART1
    uart = &uart1;

    serial1.ops    = &stm32_uart_ops;
    serial1.config = config;

    /* register UART1 device */
    rt_hw_serial_register(&serial1,
                          "uart1",
                          RT_DEVICE_FLAG_RDWR | RT_DEVICE_FLAG_INT_RX,
                          uart);
#endif /* RT_USING_UART1 */

#ifdef RT_USING_UART2
    uart = &uart2;

    serial2.ops    = &stm32_uart_ops;
    serial2.config = config;
	serial2.config.bufsz = 256;

    /* register UART2 device */
    rt_hw_serial_register(&serial2,
                          "uart2",
                          RT_DEVICE_FLAG_RDWR | RT_DEVICE_FLAG_INT_RX,
                          uart);
#endif /* RT_USING_UART2 */

#ifdef RT_USING_UART3
    uart = &uart3;

    serial3.ops    = &stm32_uart_ops;
    serial3.config = config;

    /* register UART3 device */
    rt_hw_serial_register(&serial3,
                          "uart3",
                          RT_DEVICE_FLAG_RDWR | RT_DEVICE_FLAG_INT_RX,
                          uart);
#endif /* RT_USING_UART3 */

#ifdef RT_USING_UART4
    uart = &uart4;

    serial4.ops    = &stm32_uart_ops;
    serial4.config = config;

    /* register UART4 device */
    rt_hw_serial_register(&serial4,
                          "uart4",
                          RT_DEVICE_FLAG_RDWR | RT_DEVICE_FLAG_INT_RX,
                          uart);
#endif /* RT_USING_UART4 */

#ifdef RT_USING_UART5
    uart = &uart5;

    serial5.ops    = &stm32_uart_ops;
    serial5.config = config;

    /* register UART5 device */
    rt_hw_serial_register(&serial5,
                          "uart5",
                          RT_DEVICE_FLAG_RDWR | RT_DEVICE_FLAG_INT_RX,
                          uart);
#endif /* RT_USING_UART5 */

#ifdef RT_USING_UART6
	uart = &uart6;

	serial6.ops    = &stm32_uart_ops;
	serial6.config = config;
	serial6.config.baud_rate = BAUD_RATE_9600;
	serial6.config.bufsz = 1024;

	/* register UART6 device */
	rt_hw_serial_register(&serial6,
						  "uart6",
						  RT_DEVICE_FLAG_RDWR | RT_DEVICE_FLAG_INT_RX,
						  uart);
#endif /* RT_USING_USART6 */

#ifdef RT_USING_UART7
	uart = &uart7;

	serial7.ops    = &stm32_uart_ops;
	serial7.config = config;

	/* register UART7 device */
	rt_hw_serial_register(&serial7,
						  "uart7",
						  RT_DEVICE_FLAG_RDWR | RT_DEVICE_FLAG_INT_RX,
						  uart);
#endif /* RT_USING_UART7 */

    return 0;
}
INIT_BOARD_EXPORT(stm32_hw_usart_init);

//标准库需要的支持函数
struct __FILE
{
    int handle;
};
 
//定义_sys_exit()以避免使用半主机模式
void _sys_exit(int x)
{
    x = x;
}
 
int fputc(int ch, FILE *f)
{
	USART_SendData(USART1,(u8) ch );
	while(USART_GetFlagStatus(USART1,USART_FLAG_TXE)==RESET);
	return ch;
}


