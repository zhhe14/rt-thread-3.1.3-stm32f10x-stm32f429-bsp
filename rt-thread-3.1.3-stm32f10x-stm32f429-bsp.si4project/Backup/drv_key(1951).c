#include <rtthread.h>
#include <rtdevice.h>

#include <stm32f4xx.h>

#include "drv_led.h"
#include "drv_key.h"

void key0_isr(void *args)
{
    rt_kprintf("key0_isr!\n");

	/* 查找系统中的leds设备*/
    rt_device_t led_device = rt_device_find("leds");
    if (led_device != RT_NULL)
    {
        rt_device_open(led_device, RT_DEVICE_OFLAG_RDWR);
	}

	LED_t leds;
	rt_device_read(led_device, ALLLED_STATUS, &leds, sizeof(leds));

	if (leds.led0 == LEDON)
		rt_device_control(led_device, IOCTL_LED0_OFF, NULL);
	else if (leds.led0 == LEDOFF)
		rt_device_control(led_device, IOCTL_LED0_ON, NULL);
	
	if (leds.led1 == LEDON)
		rt_device_control(led_device, IOCTL_LED1_OFF, NULL);
	else if (leds.led1 == LEDOFF)
		rt_device_control(led_device, IOCTL_LED1_ON, NULL);
	
}

void key1_isr(void *args)
{
    rt_kprintf("key1_isr!\n");

	/* 查找系统中的leds设备*/
    rt_device_t led_device = rt_device_find("leds");
    if (led_device != RT_NULL)
    {
        rt_device_open(led_device, RT_DEVICE_OFLAG_RDWR);
	}

	rt_device_control(led_device, IOCTL_LED_ALLON, NULL);
}

void key2_isr(void *args)
{
    rt_kprintf("key2_isr!\n");

	/* 查找系统中的leds设备*/
    rt_device_t led_device = rt_device_find("leds");
    if (led_device != RT_NULL)
    {
        rt_device_open(led_device, RT_DEVICE_OFLAG_RDWR);
	}

	rt_device_control(led_device, IOCTL_LED0_ON, NULL);
	rt_device_control(led_device, IOCTL_LED1_OFF, NULL);
}

void keyup_isr(void *args)
{
    rt_kprintf("keyup_isr!\n");
	
	/* 查找系统中的leds设备*/
    rt_device_t led_device = rt_device_find("leds");
    if (led_device != RT_NULL)
    {
        rt_device_open(led_device, RT_DEVICE_OFLAG_RDWR);
	}

	rt_device_control(led_device, IOCTL_LED_ALLOFF, NULL);
}

static rt_err_t hw_keys_init(rt_device_t dev)
{
    return RT_EOK;
}

static rt_err_t hw_keys_open(rt_device_t dev, rt_uint16_t oflag)
{	
	/* 按键引脚为输入模式 */
	rt_pin_mode(KEY0_PIN_NUM, PIN_MODE_INPUT_PULLUP);
	/* 绑定中断，下降沿模式，回调函数名为key0_isr */
	rt_pin_attach_irq(KEY0_PIN_NUM, PIN_IRQ_MODE_FALLING, key0_isr, RT_NULL);
	/* 使能中断 */
	rt_pin_irq_enable(KEY0_PIN_NUM, PIN_IRQ_ENABLE);

	/* 按键引脚为输入模式 */
	rt_pin_mode(KEY1_PIN_NUM, PIN_MODE_INPUT_PULLUP);
	/* 绑定中断，下降沿模式，回调函数名为keysub_isr */
	rt_pin_attach_irq(KEY1_PIN_NUM, PIN_IRQ_MODE_FALLING, key1_isr, RT_NULL);
	/* 使能中断 */
	rt_pin_irq_enable(KEY1_PIN_NUM, PIN_IRQ_ENABLE);

	/* 按键引脚为输入模式 */
	rt_pin_mode(KEY2_PIN_NUM, PIN_MODE_INPUT_PULLUP);
	/* 绑定中断，下降沿模式，回调函数名为key2_isr */
	rt_pin_attach_irq(KEY2_PIN_NUM, PIN_IRQ_MODE_FALLING, key2_isr, RT_NULL);
	/* 使能中断 */
	rt_pin_irq_enable(KEY2_PIN_NUM, PIN_IRQ_ENABLE);

	/* 按键引脚为输入模式 */
	rt_pin_mode(KEYUP_PIN_NUM, PIN_MODE_INPUT_PULLUP);
	/* 绑定中断，下降沿模式，回调函数名为keysub_isr */
	rt_pin_attach_irq(KEYUP_PIN_NUM, PIN_IRQ_MODE_FALLING, keyup_isr, RT_NULL);
	/* 使能中断 */
	rt_pin_irq_enable(KEYUP_PIN_NUM, PIN_IRQ_ENABLE);

    return RT_EOK;
}

rt_err_t  hw_keys_close(rt_device_t dev)
{
	return RT_EOK;
}

rt_size_t rt_hw_keys_read(rt_device_t dev, rt_off_t pos, void *buffer, rt_size_t size)
{
	return RT_EOK;
}

static rt_err_t  rt_hw_keys_control(rt_device_t dev, int cmd, void *args)
{
    RT_ASSERT(dev != RT_NULL);

    switch(cmd) {
        case IOCTL_READ_KEYADD:

            break;
            
        case IOCTL_READ_KEYSUB:
            break;

		case IOCTL_READ_KEYOK:
            break;
            
        case IOCTL_READ_KEYESC:
            break;

        default:break;
    }

    return RT_EOK;
}

#ifdef RT_USING_DEVICE_OPS
const static struct rt_device_ops keys_ops = 
{
    hw_keys_init,
    hw_keys_open,
    hw_keys_close,
    rt_hw_keys_read,
    RT_NULL,
    rt_hw_keys_control
};
#endif


static struct rt_device keys_device;
int rt_hw_keys_init(void)
{
    /* register device */
#ifdef RT_USING_DEVICE_OPS
	keys_device.ops = &keys_ops;
#else
    keys_device.type      = RT_Device_Class_Char;
    keys_device.init      = hw_keys_init;
    keys_device.open      = hw_keys_open;
    keys_device.close     = hw_keys_close;
    keys_device.read      = rt_hw_keys_read;
    keys_device.write     = RT_NULL;
    keys_device.control   = rt_hw_keys_control;
    keys_device.user_data = RT_NULL;
#endif

    return rt_device_register(&keys_device, "keys", RT_DEVICE_FLAG_RDWR);
}
INIT_DEVICE_EXPORT(rt_hw_keys_init);

static int isopen = 0;
static void keys_enable(void)
{
		/* 查找系统中的pin设备*/
    rt_device_t key_device = rt_device_find("keys");
    if (key_device!= RT_NULL)
    {
        rt_device_open(key_device, RT_DEVICE_OFLAG_RDWR);
		isopen = 1;
	}
}
/* 导出到 msh 命令列表中 */
MSH_CMD_EXPORT(keys_enable, enable keys function);

static void keys_disable(void)
{
	/* 查找系统中的pin设备*/
    rt_device_t key_device = rt_device_find("keys");
    if (key_device != RT_NULL)
    {
    	if (isopen)
    	{
    		isopen = 0;
        	rt_device_close(key_device);
    	}
	}
}
/* 导出到 msh 命令列表中 */
MSH_CMD_EXPORT(keys_disable, disable keys function);


